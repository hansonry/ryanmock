/**
 * @file ryanmockutil.h
 *
 * @brief The utility header file for Ryan Mock.
 *
 * This file is not nessary for writting tests.
 *
 * This file has the definitions nessary to create your own types and checks.
 * This file also contains convenience functions.
 */
#ifndef __RYANMOCKUTIL_H__
#define __RYANMOCKUTIL_H__
#include <stdarg.h>
#include "ryanmock.h"

enum ryanmock_result
{
   eRMR_passed = 0,
   eRMR_failed,
   eRMR_error
};

// Test Stack
struct ryanmock_stack_frame;

struct ryanmock_stack
{
   struct ryanmock_stack_frame * base;
   size_t size;
   size_t count;
};

struct ryanmock_value
{
   const void * data;
   const char * valueText;
   size_t size;
   bool isStored;
};

struct ryanmock_value_stored
{
   struct ryanmock_value parent;
   struct ryanmock_stack stack;
   unsigned int refCount;
   unsigned char data[];
};


void rmStreamWrite(struct ryanmock_stream * stream, const char * format, ...);
void rmStreamVWrite(struct ryanmock_stream * stream, const char * format, va_list args);

struct ryanmock_type_vt
{
   void (*fpWriteName)(const struct ryanmock_type * type, struct ryanmock_stream * stream);
   void (*fpDataCopy)(const struct ryanmock_type * type, void * dest, size_t destSize, const void * src);
   void (*fpDataFree)(const struct ryanmock_type * type, void * data);
   int  (*fpCompare)(const struct ryanmock_type * type,  const struct ryanmock_value * a, 
                                                         const struct ryanmock_value * b);
   bool (*fpIsWithin)(const struct ryanmock_type * type, const struct ryanmock_value * a, 
                                                         const struct ryanmock_value * b, 
                                                         const struct ryanmock_value * epsilon);
   void (*fpWriteValue)(const struct ryanmock_type * type, const void * value, size_t size, struct ryanmock_stream * stream);
   void (*fpWriteFailureText2)(const struct ryanmock_type * type, enum ryanmock_comparison_operator2 op, const struct ryanmock_value * actual, const struct ryanmock_value * expected, struct ryanmock_stream * rmStreamMsg);
   void (*fpWriteFailureText3)(const struct ryanmock_type * type, enum ryanmock_comparison_operator3 op, const struct ryanmock_value * actual, const struct ryanmock_value * expected1, const struct ryanmock_value * expected2, struct ryanmock_stream * rmStreamMsg);
   void (*fpDestroy)(const struct ryanmock_type * type);
   bool dereferenceParameter;

};




struct ryanmock_check_vt
{
   enum ryanmock_result (*fpCheck)(struct ryanmock_check * check,
                                   const struct ryanmock_value * value,
                                   struct ryanmock_stream * rmStreamMsg,
                                   struct ryanmock_stream * rmStreamInfo);
   void (*fpWriteInfo)(struct ryanmock_check * check,
                       struct ryanmock_stream * rmStreamInfo);
   void (*fpDestroy)(struct ryanmock_check * check);
};

struct ryanmock_check
{
   const struct ryanmock_check_vt * vt;
   struct ryanmock_type * type;
   struct ryanmock_check * parent;
   unsigned int refCount;
   unsigned char data[];
};

void rmWriteFailureText2Default(const struct ryanmock_type * type,
                                enum ryanmock_comparison_operator2 op,
                                const struct ryanmock_value * actual,
                                const struct ryanmock_value * expected,
                                struct ryanmock_stream * rmStreamMsg);

void rmWriteFailureText3Default(const struct ryanmock_type * type,
                                enum ryanmock_comparison_operator3 op,
                                const struct ryanmock_value * actual,
                                const struct ryanmock_value * expected1,
                                const struct ryanmock_value * expected2,
                                struct ryanmock_stream * rmStreamMsg);




static inline
void rmValueStoredTake(struct ryanmock_value_stored * value)
{
   value->refCount ++;
}

void rmValueStoredRelease(struct ryanmock_value_stored * value, const struct ryanmock_type * type);

static inline
void rmValueRelease(struct ryanmock_value * value, const struct ryanmock_type * type)
{
   if(value->isStored)
   {
      struct ryanmock_value_stored * valueStored = (struct ryanmock_value_stored *)value;
      rmValueStoredRelease(valueStored, type);
   }
}

static inline
void rmCheckTake(struct ryanmock_check * check)
{
   check->refCount ++;
}

void rmCheckRelease(struct ryanmock_check * check);


void rmTypeDataCopy(const struct ryanmock_type * type, void * dest, size_t destSize, const void * src);

static inline
int rmTypeCompare(const struct ryanmock_type * type, 
                  const struct ryanmock_value * a, 
                  const struct ryanmock_value * b)
{
   if(type->vt->fpCompare != NULL)
   {
      return type->vt->fpCompare(type, a, b);
   }
   else
   {
      return memcmp(a->data, b->data, type->size);
   }
}

static inline
void rmTypeWriteFailureText2(const struct ryanmock_type * type,
                             enum ryanmock_comparison_operator2 op,
                             const struct ryanmock_value * actual,
                             const struct ryanmock_value * expected,
                             struct ryanmock_stream * rmStreamMsg)
{
   if(type->vt->fpWriteFailureText2 != NULL)
   {
      type->vt->fpWriteFailureText2(type, op, actual, expected, rmStreamMsg);
   }
   else
   {
      rmWriteFailureText2Default(type, op, actual, expected, rmStreamMsg);
   }
}

static inline
void rmTypeWriteFailureText3(const struct ryanmock_type * type,
                             enum ryanmock_comparison_operator3 op,
                             const struct ryanmock_value * actual,
                             const struct ryanmock_value * expected1,
                             const struct ryanmock_value * expected2,
                             struct ryanmock_stream * rmStreamMsg)
{
   if(type->vt->fpWriteFailureText3 != NULL)
   {
      type->vt->fpWriteFailureText3(type, op, actual, expected1, expected2, rmStreamMsg);
   }
   else
   {
      rmWriteFailureText3Default(type, op, actual, expected1, expected2, rmStreamMsg);
   }
}


static inline
size_t rmTypeSize(const struct ryanmock_type * type)
{
   return type->size;
}

static inline
void rmTypeWriteValue(const struct ryanmock_type * type, const void * value, size_t size, struct ryanmock_stream * stream)
{
   type->vt->fpWriteValue(type, value, size, stream);
}

static inline
bool rmTypeIsWithin(const struct ryanmock_type * type,
                    const struct ryanmock_value * a,
                    const struct ryanmock_value * b,
                    const struct ryanmock_value * epsilon)
{
   if(type->vt->fpIsWithin == NULL)
   {
      // This is wrong, but it will be close.
      return rmTypeCompare(type, a, b) == 0;
   }
   return type->vt->fpIsWithin(type, a, b, epsilon);
}

static inline
void rmTypeWriteName(const struct ryanmock_type * type, struct ryanmock_stream * stream)
{
   if(type->vt->fpWriteName == NULL)
   {
      rmStreamWrite(stream, "Unnamed");
   }
   type->vt->fpWriteName(type, stream);
}



static inline
void rmValueWriteValueText(const struct ryanmock_value * value, const char * prefix, const char * postfix, struct ryanmock_stream * stream)
{
   if(value->valueText != NULL)
   {
      rmStreamWrite(stream, "%s%s%s", prefix, value->valueText, postfix);
   }
}


static inline
void rmValueWriteSummaryWithText(const struct ryanmock_value * value, const struct ryanmock_type * type, struct ryanmock_stream * stream)
{
   rmTypeWriteValue(type, value->data, value->size, stream);
   rmValueWriteValueText(value, " {", "}", stream);
}


struct ryanmock_value_stored * rmTypeCreateStoredValue(const struct ryanmock_type * type, 
                                                       const void * value, 
                                                       const char * valueText,
                                                       size_t size);


static inline
void rmValueWriteFullSummary(const struct ryanmock_value * value, const struct ryanmock_type * type, struct ryanmock_stream * stream)
{
   rmValueWriteSummaryWithText(value, type, stream);
   rmStreamWrite(stream, " <");
   rmTypeWriteName(type, stream);
   rmStreamWrite(stream, ">");
}

struct ryanmock_check * rmCreateCheckBoolean(bool expected);
struct ryanmock_check * rmCreateCheckNULL(bool shouldBeNULL);
struct ryanmock_check * rmCreateCheck3(enum ryanmock_comparison_operator3 op,
                                       struct ryanmock_type * type,
                                       struct ryanmock_value_stored * expected1,
                                       struct ryanmock_value_stored * expected2);
struct ryanmock_check * rmCreateCheck2(enum ryanmock_comparison_operator2 op,
                                       struct ryanmock_type * type,
                                       struct ryanmock_value_stored * expected);
struct ryanmock_check * rmCreateCheckAlwaysPass(void);
struct ryanmock_check * rmCreateCheckCallback(rmcheck_function callback, 
                                              const char * callbackName, 
                                              void * userData);

struct ryanmock_list_appearance
{
   const char * seperator;
   const char * start;
   const char * end;
   const char * hidden;
};


void rmCheck2WriteArrayFirstDiffrence(struct ryanmock_stream * stream,
                                      const void * actual,
                                      const void * expected,
                                      const struct ryanmock_type * elementType,
                                      size_t elementStride,
                                      size_t expectedCount,
                                      size_t actualCount,
                                      size_t indexOfFirstDiffrence,
                                      const struct ryanmock_list_appearance * appearance,
                                      const size_t indexToLookBack,
                                      const size_t indexToLookForward);
#endif // __RYANMOCKUTIL_H__
