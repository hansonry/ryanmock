/**
 * @file ryanmock.h
 *
 * @brief The main header file for Ryan Mock.
 *
 * This file includes all test setup, assert, expect, and mocking functions.
 *
 * If an assert or an expect results in a test failure the execution will stop
 * for that particular tests. The execution will not leave the assert functions.
 */

#ifndef __RYANMOCK_H__
#define __RYANMOCK_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

// We need to get the structure definitions from symboltable
#ifndef RMSYMBOLTABLE
#define RMSYMBOLTABLE
#endif 
#include <rmsymboltable.h>

// Lifted from: https://stackoverflow.com/questions/7008485/func-or-function-or-manual-const-char-id
#if __STDC_VERSION__ < 199901L
#if __GNUC__ >= 2 || _MSC_VER >= 1300
#define __func__ __FUNCTION__
#else
#define __func__ "<unknown>"
#endif
#endif

// Will Count forever, but expects at least one occurance
#define rmmCOUNT_ALLWAYS  -1
// Will Count forever, and has no expectation
#define rmmCOUNT_MAYBE    -2

// Exit Codes
#define RYANMOCK_EXITCODE_PASS                  0
#define RYANMOCK_EXITCODE_FAIL                  1
#define RYANMOCK_EXITCODE_ERROR                 2
#define RYANMOCK_EXITCODE_CONFIG_BAD            3
#define RYANMOCK_EXITCODE_CONFIG_NOT_SUPPORTED  4
#define RYANMOCK_EXITCODE_INFORMATION_SUPPLIED  5

struct ryanmock_test;
struct ryanmock_value;
struct ryanmock_value_stored;
struct ryanmock_check;
struct ryanmock_stream;


enum ryanmock_state
{
   eRMS_SuiteSetup,
   eRMS_TestSetup,
   eRMS_TestTeardown,
   eRMS_SuiteTeardown,
};

// Function typedefs
typedef void (* rmtest_function)(void);
typedef void (* rmstate_function)(enum ryanmock_state state, 
                                  const struct ryanmock_test * test);
typedef void (* rmcheck_function)(const void * parameterData, void * userData);

// Max Integer Type
typedef intmax_t  rmintmax_t;
typedef uintmax_t rmuintmax_t;

struct ryanmock_test
{
   const char * name;
   rmtest_function testFunction;
   rmstate_function stateFunction;
};

enum ryanmock_config_color_output
{
   eRMCCO_EnableIfPossible,
   eRMCCO_Disabled,
   eRMCCO_Enabled
};

enum ryanmock_config_run_order
{
   eRMCRO_InOrder,
   eRMCRO_Solo,
   eRMCRO_Random,
   eRMCRO_Combination,
};

struct ryanmock_config
{
   // Required
   const char * name;
   const struct ryanmock_test * tests;
   size_t numberOfTests;
   
   // Optional test state function (Set to NULL if you don't want it)
   rmstate_function suiteStateFunction;
   
   // Test Order
   enum ryanmock_config_run_order order;
   unsigned int randomTestOrderSeed;
   size_t soloTestNameCount;
   const char ** soloTestNames;
   
   // Output Options
   bool         writeResultsToStdOut;
   const char * xmlOutputFilename; // Write file if not NULL
   bool         justListTests; // If True, no tests will run, just the test 
                               // names printed to stdout
   
   // Other Options
   enum ryanmock_config_color_output colorOutput;
   bool breakOnBadResult;
   size_t stackFillCount;
   unsigned char stackFillValue;
   
   
};

enum ryanmock_comparison_operator2
{
   eRMCO2_NotApplicable,
   eRMCO2_Equal,
   eRMCO2_NotEqual,
   eRMCO2_GreaterThan,
   eRMCO2_GreaterThanOrEqualTo,
   eRMCO2_LessThan,
   eRMCO2_LessThanOrEqualTo
};

enum ryanmock_comparison_operator3
{
   eRMCO3_NotApplicable,
   eRMCO3_Between,
   eRMCO3_NotBetween,
   eRMCO3_Within,
   eRMCO3_NotWithin
};

/**
 * @defgroup Create Test Creation and Running Functions
 * @{
 */

/**
 * @brief Creates a test from a function.
 * 
 * @see rmmRunTestsCmdLine
 * 
 * @param function  The function that contains the test.
 */
#define rmmMakeTest(function) { #function, function, NULL }

/**
 * @brief Creates a test from a function with setup and teardown functions.
 * 
 * @see rmmRunTestsCmdLine
 * 
 * @param function             The function that contains the test.
 * @param testStateFunction    A state function used for test setup and 
 *                             teardown.
 */
#define rmmMakeTest2(function, testStateFunction) \
{ #function, function, testStateFunction }


bool rmConfigCommandLine(struct ryanmock_config * config,
                         int argc, char * args[]);

#define rmmConfigTestsSimple(config, tests, name) \
rmConfigTestsSimple((config), (tests), \
                    sizeof(tests) / sizeof(struct ryanmock_test), \
                    (name), NULL)
void rmConfigTestsSimple(struct ryanmock_config * config,
                         const struct ryanmock_test * tests, 
                         size_t numberOfTests,
                         const char * name,
                         rmstate_function suiteStateFunction);

/**
 * @brief Runs the specified tests
 *
 * @param tests  An array of test functions (declared in calling function).
 * @param name   The name of this set of this unit test
 * @return The test result. 0 - all passed, 1 - at least one test failed and
 *         no test errors, 2 -  at least one test error.
 *          
 */
#define rmmRunTests(tests, name) \
rmRunTestsSimple((tests), sizeof(tests) / sizeof(struct ryanmock_test), (name))
int rmRunTestsSimple(const struct ryanmock_test * tests, size_t numberOfTests,
                     const char * name);

/**
 * @brief Runs the specified tests with settings from the command line.
 *
 * @param tests                 An array of test functions 
 *                              (declared in calling function).
 * @param name                  The name of this set of this unit test. 
 *                              Can be NULL to get the executable name from the 
 *                              command line arguments. 
 * @param testStateFunction     Test State function. This can be NULL. This 
 *                              can be used to do test setup/teardown and 
 *                              suite setup/teardown.
 * @param argc                  Argument count from command line.
 * @param args                  Array of argument from the command line.
 * 
 * @return The test result. 0 - all passed, 1 - at least one test failed and
 *         no test errors, 2 -  at least one test error, 3 - failure during
 *         command line parsing.
 *          
 */
#define rmmRunTestsCmdLine2(tests, name, \
                            testStateFunction, argc, args) \
rmRunTestsCmdLine((tests), sizeof(tests) / sizeof(struct ryanmock_test), \
                  (name), (testStateFunction), (argc), (args))
/**
 * @brief Runs the specified tests with settings from the command line.
 *
 * @param tests                 An array of test functions 
 *                              (declared in calling function).
 * @param name                  The name of this set of this unit test. 
 *                              Can be NULL to get the executable name from the 
 *                              command line arguments. 
 * @param argc                  Argument count from command line.
 * @param args                  Array of argument from the command line.
 * 
 * @return The test result. 0 - all passed, 1 - at least one test failed and
 *         no test errors, 2 -  at least one test error, 3 - failure during
 *         command line parsing.
 *          
 */
#define rmmRunTestsCmdLine(tests, name, argc, args) \
rmRunTestsCmdLine((tests), sizeof(tests) / sizeof(struct ryanmock_test), \
                  name, NULL, (argc), (args))
int rmRunTestsCmdLine(const struct ryanmock_test * tests, 
                      size_t numberOfTests,
                      const char * name,
                      rmstate_function    testStateFunction,
                      int argc, char * args[]);

int rmRunTests(const struct ryanmock_config * config);

/**@}*/ // End of Create

/**
 * @defgroup Control Test Control Functions
 * @{
 */

/**
 * @brief Checks and clear the test mock buffer
 *
 * You can use this to split up your test into parts. If there are unused 
 * mocks or expects this will cause the test to fail. You will need to 
 * re-ignore any mocked functions you have ignored.
 */
#define rmmFUTCalled() \
rmFunctionUnderTestCalled(__LINE__, __FILE__, "rmmFUTCalled")
void rmFunctionUnderTestCalled(int line, const char * filename, 
                               const char * function);


/**
 * @brief Skips the current test.
 *
 * If the test should fail before this is called the test will not be
 * skipped.
 *
 * This will not prevent the test a whole from passing.
 */
#define rmmSkip() rmSkip(__LINE__, __FILE__, "rmmSkip")
void rmSkip(int line, const char * filename, const char * function);

/**@}*/ // End of Control

/**
 * @defgroup Stack Test Stack manipulation functions
 *
 * This library has a really basic stack system for giving 
 * good feedback when tests don't pass.
 *
 * Most of the time you won't need to mess with this.
 *
 * @{
 */

/**
 * @brief Adds stack tracking to a specific function
 *
 * Use this in your test to call utility functions to track stack transitions.
 * You can add in return values too.
 * 
 * EX: rmmCallFunction(ret = add2(1, 2));
 *
 * @param function The function or statement to add to the test stack
 */
#define rmmCallFunction(function) rmEnterFunction(#function, __LINE__, __FILE__); function; rmExitFunction(#function, __LINE__, __FILE__)

/**
 * @brief Pushes a stack record for the current function
 *
 * Don't forget to exit the function when you are done.
 *
 * @see rmmExitFunction
 *
 * @param function The function you are entering on the stack
 */
#define rmmEnterFunction(function) rmEnterFunction(#function, __LINE__, __FILE__)
void rmEnterFunction(const char * functionCallText, int line, const char * filename);

/**
 * @brief Pops a stack record for the current function
 *
 * You need to push a function recrod before you pop it.
 *
 * @see rmmEnterFunction
 *
 * @param function The function you are exiting on the stack
 */
#define rmmExitFunction(function) rmExitFunction(#function, __LINE__, __FILE__)
void rmExitFunction(const char * functionCallText, int line, const char * filename);


/**
 * @brief Pushes a stack record with whatever text you like
 *
 * This is good for adding test utility functions to the call stack.
 *
 * You need to Pop every record you push
 *
 * @see rmmStackPop
 *
 * @param filename  The filename you want to appear in the stack trace
 * @param line      The line number you want to appear in the stack trace
 * @param token     A pointer used to uniquly identify this record.
 * @param format    A format string like printf
 */
#define rmmStackPush(filename, line, token, ...)                       \
rmStackPush(__LINE__, __FILE__, "rmmStackPush", line, filename, token, \
            __VA_ARGS__)

/**
 * @brief Pushes a stack record with whatever text you like.
 *
 * This is a lot like rmmStackPush but the line and file are taken 
 * from the current called location.
 *
 * This is good for adding loop iterations to the call stack.
 *
 * You need to Pop every record you push
 *
 * @see rmmStackPop
 *
 * @param token     A pointer used to uniquly identify this record.
 * @param format    A format string like printf
 */
#define rmmStackPushHere(token, ...) \
rmStackPush(__LINE__, __FILE__, "rmmStackPushHere", __LINE__, __FILE__, token, \
            __VA_ARGS__)

void rmStackPush(int rmLine, const char * rmFilename, 
                 const char * rmFunctionName, 
                 int line, const char * filename,
                 const void * token, const char * format, ...);


/**
 * @brief Pops a stack record.
 *
 * You need to have Pushed a record into the stack with the @p token
 * befor you can Pop it.
 *
 * @see rmmStackPush
 * @see rmmStackPushHere
 *
 * @param token  A pointer used to uniquly identify the recrod to remove.
 */
#define rmmStackPop(token) \
rmStackPop(token, __LINE__, __FILE__, "rmmStackPop")
void rmStackPop(const void * token, int rmLine, const char * rmFilename, 
                const char * rmFunctionName);

/**@}*/ // End of Stack


/**
 * @defgroup Types Type Creation functions for use in custom types
 *
 * For most basic usages of this library, you won't need to deal with these.
 *
 * @{
 */

struct ryanmock_type
{
   const struct ryanmock_type_vt * vt;
   size_t size;
   unsigned int refCount;
   unsigned char data[];
};

static inline
void rmTypeTake(struct ryanmock_type * type)
{
   type->refCount ++;
}

void rmTypeRelease(struct ryanmock_type * type);

struct ryanmock_type * rmCreateTypeInteger(size_t size,
                                           bool isSigned,
                                           const char * typeName,
                                           bool isPickyAboutSize);

struct ryanmock_type * rmCreateTypeArray(struct ryanmock_type * elementType,
                                         size_t count);
static inline
struct ryanmock_type * rmCreateTypeIntegerInt(void)
{
   return rmCreateTypeInteger(sizeof(int), true, "Int", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerUInt(void)
{
   return rmCreateTypeInteger(sizeof(unsigned int), false, "Unsigned Int", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerULong(void)
{
   return rmCreateTypeInteger(sizeof(unsigned long), false, "Unsigned Long", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerLong(void)
{
   return rmCreateTypeInteger(sizeof(long), true, "Long", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerUShort(void)
{
   return rmCreateTypeInteger(sizeof(unsigned short), false, "Unsigned Short", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerShort(void)
{
   return rmCreateTypeInteger(sizeof(short), true, "Short", true);
}


static inline
struct ryanmock_type * rmCreateTypeIntegerU8(void)
{
   return rmCreateTypeInteger(sizeof(uint8_t), false, "Unsigned Int8", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerU16(void)
{
   return rmCreateTypeInteger(sizeof(uint16_t), false, "Unsigned Int16", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerU32(void)
{
   return rmCreateTypeInteger(sizeof(uint32_t), false, "Unsigned Int32", true);
}

#ifdef UINT64_MAX
static inline
struct ryanmock_type * rmCreateTypeIntegerU64(void)
{
   return rmCreateTypeInteger(sizeof(uint64_t), false, "Unsigned Int64", true);
}
#endif // UINT64_MAX

static inline
struct ryanmock_type * rmCreateTypeIntegerUIntPtr(void)
{
   return rmCreateTypeInteger(sizeof(uintptr_t), false, "Unsigned IntPtr", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerS8(void)
{
   return rmCreateTypeInteger(sizeof(int8_t), true, "Int8", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerS16(void)
{
   return rmCreateTypeInteger(sizeof(int16_t), true, "Int16", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerS32(void)
{
   return rmCreateTypeInteger(sizeof(int32_t), true, "Int32", true);
}

#ifdef UINT64_MAX
static inline
struct ryanmock_type * rmCreateTypeIntegerS64(void)
{
   return rmCreateTypeInteger(sizeof(int64_t), true, "Int64", true);
}
#endif // UINT64_MAX

static inline
struct ryanmock_type * rmCreateTypeIntegerIntPtr(void)
{
   return rmCreateTypeInteger(sizeof(intptr_t), true, "IntPtr", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerSizeT(void)
{
   return rmCreateTypeInteger(sizeof(size_t), false, "Size_T", true);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerSignedMax(void)
{
   return rmCreateTypeInteger(sizeof(rmintmax_t), true, "Max Signed Int", false);
}

static inline
struct ryanmock_type * rmCreateTypeIntegerUnsignedMax(void)
{
   return rmCreateTypeInteger(sizeof(rmuintmax_t), false, "Max Unsigned Int", false);
}


struct ryanmock_type * rmCreateTypeFloatingPoint(const char * typeName,
                                                 size_t size,
                                                 const char * format);

static inline
struct ryanmock_type * rmCreateTypeFloat(void)
{
   return rmCreateTypeFloatingPoint("Float", sizeof(float), "%0.2f");
}

static inline
struct ryanmock_type * rmCreateTypeDouble(void)
{
   return rmCreateTypeFloatingPoint("Double", sizeof(double), "%0.2f");
}

static inline
struct ryanmock_type * rmCreateTypeLongDouble(void)
{
   return rmCreateTypeFloatingPoint("LongDouble", sizeof(long double),
                                    "%0.2Lf");
}

struct ryanmock_type * rmCreateTypePointer(void);
/**@}*/ // End of Types



/**
 * @defgroup Mocks Mocking functions
 * @{
 */

/**
 * @brief Mark a mocked function as called
 *
 * This function is meant to be used inside mocked functions. You don't need to 
 * add this to your mocked function, but it is needed if you want to 
 * enforce call ordering.
 *
 * @see rmmExpectOrdered
 * @see rmmIgnoreOrdered
 */
#define rmmFunctionCalled() \
rmFunctionCalled(__func__, __LINE__, __FILE__, "rmmFunctionCalled")
void rmFunctionCalled(const char * functionName,
                      int line, const char * filename,
                      const char * thisFunctionName);

/**
 * @brief Check the value of the parameter named @p parameter.
 *
 * This function is meant to be used inside mocked functions. This is
 * a way of getting values out to the expect functions.
 *
 * @param parameter The parameter name to check.
 */
#define rmmParamCheck(parameter)                            \
rmParamCheck(__func__, #parameter, &parameter,  #parameter, \
             sizeof(parameter), 1, false,                   \
             __LINE__, __FILE__, "rmmParamCheck")
             
/**
 * @brief Check the value of the void parameter array named @p parameter.
 *
 * This function is meant to be used inside mocked functions. This is
 * a way of getting values out to the expect functions.
 *
 * This is used to check the contents of void pointers of various sizes. 
 * The @p byteCount can be determined at runtime.
 *
 * @param parameter  The parameter name to check.
 * @param byteCount  The number of bytes in the array
 */
#define rmmParamCheckVoidArray(parameter, byteCount)            \
rmParamCheck(__func__, #parameter, &parameter,  #parameter,     \
             1, byteCount, true,                                \
             __LINE__, __FILE__,                                \
             "rmmParamCheckVoidArray")
             
/**
 * @brief Check the value of the parameter array named @p parameter.
 *
 * This function is meant to be used inside mocked functions. This is
 * a way of getting values out to the expect functions.
 *
 * This is used to check arrays of various sizes. The @p elementSize can
 * be determined at runtime.
 *
 * @param parameter    The parameter name to check.
 * @param elementCount The number of elements in the array
 */
#define rmmParamCheckArray(parameter, elementCount)                \
rmParamCheck(__func__, #parameter, &parameter,  #parameter,        \
             sizeof(*parameter), elementCount, true,               \
             __LINE__, __FILE__,                                   \
             "rmmParamCheckArray")
void rmParamCheck(const char * functionName, const char * parameterName,
                  const void * data, const char * valueText, 
                  size_t elementSize, size_t elementCount, bool isArray,
                  int line, const char * filename,
                  const char * thisFunctionName);

/**
 * @brief Enable the mocked function to return a value
 *
 * This function is meant to be used inside mocked functions. This macro
 * has a return statement inside it, so it needs to be the last statment in
 * the function.
 *
 * See the WillReturn functions
 *
 * @param type The datatype of the value being returned. ex int for functions
 *             returning ints.
 */
#define rmmMockReturn(type)                                      \
do {                                                             \
   type t;                                                       \
   rmMock(__func__, NULL, &t, sizeof(type),                      \
          false, __LINE__, __FILE__, "rmmMockReturn");           \
   return t;                                                     \
} while(0)

/**
 * @brief Enable the mocked function to return an array value
 *
 * An element count needs to be provided so that the correct size can be
 * computed. 
 *
 * This function is meant to be used inside mocked functions. This macro
 * has a return statement inside it, so it needs to be the last statment in
 * the function.
 *
 * See the WillReturn functions
 *
 * @param type         The datatype of the value being returned. ex int for 
 *                     functions returning ints.
 * @param elementCount The number of elements in the array, can be defined 
 *                     from a parameter passed into the mocked function.
 */
#define rmmMockReturnArray(type, elementCount)             \
do {                                                       \
   type * t;                                               \
   rmMock(__func__, NULL, &t, sizeof(type) * elementCount, \
          true, __LINE__, __FILE__, "rmmMockReturnArray"); \
   return t;                                               \
} while(0)
   
/**
 * @brief Enable the mocked function to return a void array value
 *
 * An element count needs to be provided so that the correct size can be
 * computed. 
 *
 * This function is meant to be used inside mocked functions. This macro
 * has a return statement inside it, so it needs to be the last statment in
 * the function.
 *
 * See the WillReturn functions
 *
 * @param type         The datatype of the value being returned. ex int for 
 *                     functions returning ints.
 * @param elementCount The number of elements in the array, can be defined 
 *                     from a parameter passed into the mocked function.
 */
#define rmmMockReturnVoidArray(elementCount)                     \
do {                                                             \
   void * t;                                                     \
   rmMock(__func__, NULL, &t, elementCount,                      \
          true, __LINE__, __FILE__, "rmmMockReturnVoidArray");   \
   return t;                                                     \
} while(0)

/**
 * @brief Enable the mocked function to set an output value for void pointers.
 *
 * This function is meant to be used inside mocked functions. This assumes
 * that the parameter passed in is at least a pointer.
 *
 * See the WillOutput functions
 *
 * @param parameter   The function parameter you will to change.
 * @param byteSize    The length of the output in bytes.
 */
#define rmmMockParamVoid(parameter, byteSize)       \
rmMock(__func__, #parameter, (parameter), byteSize, \
       false, __LINE__, __FILE__, "rmmMockParamVoid")

/**
 * @brief Enable the mocked function to set an output value
 *
 * This function is meant to be used inside mocked functions. This assumes
 * that the parameter passed in is at least a pointer.
 *
 * See the WillOutput functions
 *
 * @param parameter   The function parameter you will to change.
 * @param elementSize The length of the output in elements. For non arrays this
 *                    value should be 1.
 */
#define rmmMockParam(parameter, elementSize) \
rmMock(__func__, #parameter, (parameter), sizeof(*parameter) * elementSize, \
       false, __LINE__, __FILE__, "rmmMockParam")
void rmMock(const char * functionName, const char * parameter,
             void * value, size_t size, bool isArray,
             int line, const char * filename, 
             const char * thisFunctionName);

/**
 * @brief Used to begin the definition of a mocked function in a test.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBegin(function) \
rmMockBegin(#function, 1, __LINE__, __FILE__, "rmmMockBegin")

/**
 * @brief Used to begin the definition of a mocked function in a test.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 * @param count    The number of times this function is expected to be called
 */
#define rmmMockBeginCount(function, count) \
rmMockBegin(#function, count, __LINE__, __FILE__, "rmmMockBeginCount")

/**
 * @brief Used to begin the definition of all mocked function in a test.
 *
 * This expects @p function to be called at least once.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginAll(function) \
rmMockBegin(#function, rmmCOUNT_ALLWAYS, __LINE__, __FILE__, "rmmMockBeginAll")

/**
 * @brief Used to begin the definition of all mocked function in a test.
 *
 * @p function can be called zero times.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginAny(function) \
rmMockBegin(#function, rmmCOUNT_MAYBE, __LINE__, __FILE__, "rmmMockBeginAny")

/**
 * @brief Used to begin the definition of an ordered mocked function in a test.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginOrdered(function)                                 \
rmMockBegin(#function, 1, __LINE__, __FILE__, "rmmMockBeginOrdered"); \
rmExpectOrdered(false, __LINE__, __FILE__, "rmmMockBeginOrdered")

/**
 * @brief Used to begin the definition of a mocked function in a test.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 * @param count    The number of times this function is expected to be called
 */
#define rmmMockBeginOrderedCount(function, count)                              \
rmMockBegin(#function, count, __LINE__, __FILE__, "rmmMockBeginOrderedCount"); \
rmExpectOrdered(false, __LINE__, __FILE__, "rmmMockBeginOrderedCount")

/**
 * @brief Used to begin the definition of all ordered mocked function in a test.
 *
 * This expects @p function to be called at least once.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginOrderedAll(function)                                                \
rmMockBegin(#function, rmmCOUNT_ALLWAYS, __LINE__, __FILE__, "rmmMockBeginOrderedAll"); \
rmExpectOrdered(false, __LINE__, __FILE__, "rmmMockBeginOrderedAll")

/**
 * @brief Used to begin the definition of all ordered mocked function in a test.
 *
 * @p function can be called zero times.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginOrderedAny(function)                                              \
rmMockBegin(#function, rmmCOUNT_MAYBE, __LINE__, __FILE__, "rmmMockBeginOrderedAny"); \
rmExpectOrdered(false, __LINE__, __FILE__, "rmmMockBeginOrderedAny")

/**
 * @brief Used to begin the definition of an ignored mocked function in a test.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginIgnored(function)                                 \
rmMockBegin(#function, 1, __LINE__, __FILE__, "rmmMockBeginIgnored"); \
rmExpectOrdered(true, __LINE__, __FILE__, "rmmMockBeginIgnored")

/**
 * @brief Used to begin the definition of an ignored mocked function in a test.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 * @param count    The number of times this function is expected to be called
 */
#define rmmMockBeginIgnoredCount(function, count)                              \
rmMockBegin(#function, count, __LINE__, __FILE__, "rmmMockBeginIgnoredCount"); \
rmExpectOrdered(true, __LINE__, __FILE__, "rmmMockBeginIgnoredCount")

/**
 * @brief Used to begin the definition of all ignored mocked function in a test.
 *
 * This expects @p function to be called at least once.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginIgnoredAll(function)                                                \
rmMockBegin(#function, rmmCOUNT_ALLWAYS, __LINE__, __FILE__, "rmmMockBeginIgnoredAll"); \
rmExpectOrdered(true, __LINE__, __FILE__, "rmmMockBeginIgnoredAll")

/**
 * @brief Used to begin the definition of all ignored mocked function in a test.
 *
 * @p function can be called zero times.
 *
 * This is used to "control" the mocked function named @p function. It needs an
 * associated end function call after. You can use the
 * @ref Expects "expects functions" to check parameters.
 *
 * @see rmmMockEnd
 *
 * @param function The parameter name to check.
 */
#define rmmMockBeginIgnoredAny(function)                                              \
rmMockBegin(#function, rmmCOUNT_MAYBE, __LINE__, __FILE__, "rmmMockBeginIgnoredAny"); \
rmExpectOrdered(true, __LINE__, __FILE__, "rmmMockBeginIgnoredAny")

void rmMockBegin(const char * functionName, int expectedCount,
                 int line, const char * filename,
                 const char * thisFunctionName);

/**
 * @brief Used to end the definition of a mocked function in a test.
 *
 * This needs an associated begin function call before.
 *
 * @see rmmMockBegin
 * @see rmmMockBeginCount
 * @see rmmMockBeginAll
 *
 */
#define rmmMockEnd() rmMockEnd(__LINE__, __FILE__, "rmmMockEnd")
void rmMockEnd(int line, const char * filename,
               const char * thisFunctionName);


/**
 * @brief Used to enforce the order of function calls
 *
 * This needs to be called only once between begin and end mock functions
 *
 * @see rmmMockBegin
 * @see rmmMockEnd
 */
#define rmmExpectOrdered() \
rmExpectOrdered(false, __LINE__, __FILE__, "rmmExpectOrdered")

/**
 * @brief Used to ignore the order of function calls
 *
 * This needs to be called only once between begin and end mock functions
 *
 * @see rmmMockBegin
 * @see rmmMockEnd
 */
#define rmmIgnoreOrdered() \
rmExpectOrdered(true, __LINE__, __FILE__, "rmmIgnoreOrdered")
void rmExpectOrdered(bool ignore,
                     int line, const char * filename,
                     const char * functionName);

/**@}*/ // End of Mocks

/**
 * @defgroup SymbolTable Static Local Symbol Export
 * @{
 */

/**
 * @brief Loads a symbol table of a specified module
 * 
 * This need to be done before calling rmmSymbolTableLink.
 * The module name needs to match one of the RMM_SYMBOLTABLE_BEGIN 
 * module names.
 *
 * This is used outside of functions. 
 * Do not add a semicolon at the end of the line.
 * 
 * @see rmmSymbolTableLink
 * @see RMM_SYMBOLTABLE_BEGIN
 * 
 * @param module  The name of the module you are attemping to bring in.
 */
#define RMM_SYMBOLTABLE_LOAD(module) \
extern const struct ryanmock_symbol symboltable_##module[];

/**
 * @brief Linking a symbol 
 * 
 * This needs to be done after loading the module symbol table using
 * RMM_SYMBOLTABLE_LOAD.
 *
 * A test error will be triggered if the symbol is not found.
 * 
 * You need to declare a pointer variable whos address will be set
 * to the address of the symbol in a diffrent module. For example:
 *
 * @code{.c}
 * static int * abc; // The symbol we are trying to link to is also named abc
 * RMM_SYMBOLTABLE_LOAD(module)
 * void test_setup_function()
 * {
 *    rmmSymbolTableLink(module, abc);
 * }
 * @endcode
 *
 * @see RMM_SYMBOLTABLE_LOAD
 * 
 * @param module  The name of the module that the symbol is in.
 * @param symbol  The name of the symbol you are linking to and the pointer
 *                that you are linking it to.
 */
#define rmmSymbolTableLink(module, symbol) \
rmSymbolTableLink((void**)&symbol, #symbol, symboltable_##module, #module, \
                  __LINE__, __FILE__, "rmmSymbolTableLink")
void rmSymbolTableLink(void ** dest, 
                       const char * symbolName,
                       const struct ryanmock_symbol * table, 
                       const char * tableName,
                       int line, const char * filename,
                       const char * functionName);


/**@}*/ // End of SymbolTable

// See devtools/headergen.py
#include <ryanmock_gen.h>


#endif // __RYANMOCK_H__

