/**
 * @file rmsymboltable.h
 *
 * @brief Static Local Symbol Export.
 *
 * Using this you can export your local static symbols for your test.
 *
 * Exporting only generates code if you have defined RMSYMBOLTABLE. It is 
 * sugjested to do this in your build system.
 *
 * The table needs to be defined after you have eithier prototyped or 
 * declared your symbols.
 *
 * Example:
 * @code{.c}
 * // Inside magic.c
 * RMM_SYMBOLTABLE_BEGIN(magic)
 *    RMM_SYMBOLTABLE_SYMBOL(a)
 *    RMM_SYMBOLTABLE_SYMBOL(doubler)
 *    RMM_SYMBOLTABLE_ARRAY(array)
 * RMM_SYMBOLTABLE_END()
 * @endcode
 */
#ifndef __RMSYMBOLTABLE_H__
#define __RMSYMBOLTABLE_H__

#ifdef RMSYMBOLTABLE
#include <stddef.h> // For NULL
#include <stdbool.h>

struct ryanmock_symbol
{
   union {
      void * ptr;
      void (*func)(void);
   } sym;
   bool isFunc;
   const char * name;
};
/**
 * @defgroup SymbolTable Static Local Symbol Export
 * @{
 */

/**
 * @brief Starts the creation of a symbol table.
 * 
 * Don't forget to end the symbol table
 * 
 * This is used outside of functions. 
 * Do not add a semicolon at the end of the line.
 * 
 * @see RMM_SYMBOLTABLE_END
 * 
 * @param module  The name of the module you are build a symbol table for.
 */
#define RMM_SYMBOLTABLE_BEGIN(module) const struct ryanmock_symbol symboltable_##module[] = {

/**
 * @brief Ends the definition of a symbol table
 * 
 * Don't forget to begin the symbol table
 * 
 * This is used outside of functions. 
 * Do not add a semicolon at the end of the line.
 * 
 * @see RMM_SYMBOLTABLE_BEGIN
 */
#define RMM_SYMBOLTABLE_END() \
{ .sym.ptr = NULL, .isFunc = false, .name = NULL } };

/**
 * @brief Shares a symbol
 * 
 * This needs to be between RMM_SYMBOLTABLE_BEGIN and RMM_SYMBOLTABLE_END.
 *
 * You can use this to share any symbol including functions with the exception
 * of arrays declared with [].
 * 
 * This is used outside of functions. 
 * Do not add a semicolon at the end of the line.
 * 
 * @see RMM_SYMBOLTABLE_BEGIN
 * @see RMM_SYMBOLTABLE_END
 * 
 * @param symbol  The name of the symbol you are sharing.
 */
#define RMM_SYMBOLTABLE_SYMBOL(symbol) \
{ .sym.ptr = (void*)&symbol, .isFunc = false, .name = #symbol },

/**
 * @brief Shares an array symbol
 * 
 * This needs to be between RMM_SYMBOLTABLE_BEGIN and RMM_SYMBOLTABLE_END.
 *
 * This is only used for sharing array symboles. You know the 
 * symbols with [] after their names.
 * 
 * This is used outside of functions. 
 * Do not add a semicolon at the end of the line.
 * 
 * @see RMM_SYMBOLTABLE_BEGIN
 * @see RMM_SYMBOLTABLE_END
 * 
 * @param symbol  The name of the array symbol you are sharing.
 */
#define RMM_SYMBOLTABLE_ARRAY(array) \
{ .sym.ptr = (void*)array, .isFunc = false, .name = #array  },

/**
 * @brief Shares a function symbol
 * 
 * This needs to be between RMM_SYMBOLTABLE_BEGIN and RMM_SYMBOLTABLE_END.
 *
 * This is only used for sharing function symboles.
 * 
 * This is used outside of functions. 
 * Do not add a semicolon at the end of the line.
 * 
 * @see RMM_SYMBOLTABLE_BEGIN
 * @see RMM_SYMBOLTABLE_END
 * 
 * @param function  The name of the function symbol you are sharing.
 */
#define RMM_SYMBOLTABLE_FUNCTION(function) \
{ .sym.func = (void (*)(void))function, .isFunc = true, .name = #function  },


/**@}*/ // End of SymbolTable
#else // RMSYMBOLTABLE

// Empty #defines to compile out any code
#define RMM_SYMBOLTABLE_BEGIN(module)
#define RMM_SYMBOLTABLE_END()

#define RMM_SYMBOLTABLE_SYMBOL(symbol)
#define RMM_SYMBOLTABLE_ARRAY(array)
#define RMM_SYMBOLTABLE_FUNCTION(function)

#endif // RMSYMBOLTABLE
 
#endif // __RMSYMBOLTABLE_H__
