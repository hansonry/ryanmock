# Functions Cheat Sheet

ryanmock has a lot of functions but most are different forms of each other

[[_TOC_]]

## Capabilities

ryanmock currently has support for the following datatypes and 
comparisons.

### Datatypes

The following datatypes are supported. If you have a `typedef`ed type,
you will want to use the closest equivalent. The compiler should be able
to figure it out. Structs need to be handled via `Memory` comparison
or you can use a [callback](#callback) to pull apart specific struct members.

* `Int`    - System integer (int)
* `UInt`   - Unsigned system integer (unsigned int)
* `Long`   - System long (long)
* `ULong`  - Unsigned system long (unsigned long)
* `Short`  - System short (short)
* `UShort` - Unsigned system short (unsigned short)
* `Int8`   - 8 bit integer (int8_t)
* `Int16`  - 16 bit integer (int16_t)
* `Int32`  - 32 bit integer (int32_t)
* `Int64`  - 64 bit integer (int64_t) (on supported systems only) 
* `IntU8`  - Unsigned 8 bit integer (uint8_t)
* `IntU16` - Unsigned 16 bit integer (uint16_t)
* `IntU32` - Unsigned 32 bit integer (uint32_t)
* `IntU64` - Unsigned 64 bit integer (uint64_t) (on supported systems only) 
* `SizeT`  - Memory Size Type (size_t) 
* `Enum`   - Used for Enumerations and unknown size types (rmunitmax_t)
* `Ptr`    - Pointers (const void *)
* `Memory` - Chunks of memory (const void *)
* `Double` - System doubles (double)
* `Float`  - System floats (float)
* `String` - Strings Char arrays and null terminated (const char *)
* `Bool`   - Boolean value (bool)

### Comparisons

* `Equal` - actual needs to equal expected
* `NotEqual` - actual needs to not equal expected
* `GreaterThan` - actual needs to be greater than expected
* `GreaterThanOrEqualTo` - actual needs to be greater than or equal to expected
* `LessThan`             - actual needs to be less than expected
* `LessThanOrEqualTo`    - actual needs to be less than or equal to expected
* `Between`              - actual needs to be between min and max (inclusive)
* `NotBetween`           - actual needs to be not between min and max
* `Within`               - formula: `abs(actual - expected) > epsilon` (good for float comparison)
* `NotWithin`            - Inverse of `Within`

## Test

Here is an example main function:

```c
int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_test),
      rmmMakeTest(failing_test),
   };
   return rmmRunTestsCmdLine(tests, NULL, argc, args);
}
```

Here are some of the functions you can use when setting up the test suite:

* `rmmMakeTest(testFunction)` - Creates a test from a test function
* `rmmMakeTest2(testFunction, setupFunction, teardownFunction)` - 
   Creates a test from a test function. It will run the setup Function before
   the test is run and it will run the teardownFunction after the test is
   run.
* `rmmRunTestsCmdLine(tests, name, argc, args)` - Runs all the tests. If name
   is NULL then it will use the exe file name. argc and args are main 
   arguments.
* `rmmRunTestsCmdLine2(tests, name, setupFunction, teardownFunction, argc, args)` -
   Runs all the tests. If name is NULL then it will use the exe file name. 
   setupFunction is run before any tests are run. teardownFunction is run
   only after all the tests have run. argc and args are main arguments.


Functions you can run during a test that aren't directly related to
verification.

* `rmmFail(format, ...)` - Fail the test and specify a reason like printf.
* `rmmSkip()`            - Skips this test.
* `rmmFUTCalled()`       - Checks to see if all the mocks have been satisfied.
                           This is optional and is done at the end of the 
                           test automatically. 

## Stack

ryanmock stack system is a manual one. You can Push and pop functions or
other things onto the call stack using these functions:

* `rmmEnterFunction(function)` - Add this function to the stack.
                                 The stack picks the line and file this 
                                 was called at.
* `rmmExitFunction(function)`  - Removes this function from the stack.
* `rmmStackPush(filename, line, token, format, ...)` - Pushes whatever
                                 text you want onto the stack. The
                                 address of the token is used as the
                                 identifier.
* `rmmStackPushHere(token, format, ...)` - A lot like `rmmStackPush` but
                                 it takes the line and file from where it is 
                                 called.
* `rmmStackPop(token)`         - Pops the pushed string from the stack using
                                 the address of token to match the record.

## Asserts

The general assert form is as follows:

`rmmAssert<Datatype><Comparison>(actual, ...)`

Where `<Datatype>` can be any of of the [Datatypes](#datatypes) and 
`<Comparison>` can be any of the [Comparisons](#comparisons). The
number of parameters after actual depends on the comparison and datatype.


There are some special ones too:

* `rmmAssertTrue(actual)`       - This will fail if actual is not true
* `rmmAssertFalse(actual)`      - This will fail if actual is true
* `rmmAssertPtrNULL(actual)`    - This will fail if actual is not NULL
* `rmmAssertPtrNotNULL(actual)` - This will fail if actual is NULL


## Mocking

### Setting up a Function to be Mocked

To mock a function, you need to create an instrumented version of it.
You can use the following functions:

* `rmmFunctionCalled()`      - Use to enforce the order of the mocked function
* `rmmParamCheck(parameter)` - Check the parameter
* `rmmMockReturn(type)`      - Return a mocked value. The type should be the
                               same as the return type of the function.
                               This isn't needed if the return type of the
                               function is void.
* `rmmParamCheckArray(parameter, count)` - Check parameter as an array. The
                                           count parameter is the number of
                                           elements in the array (not bytes).
* `rmmMockParam(parameter, count)` - Used to set an output parameter value.
                                     The count parameter is the number of 
                                     elements in the array (not bytes).


### Expecting a Mocked Function to be Called

For each function call you want to mock, you need to specify the function
name, if you used `rmmFunctionCalled()` it's ordering, and if the function
returns a value, a return value.

Parameters can be checked an output between a begin and end function.

Here is an example:

```c
   rmmMockBegin(AddTwoNumbers);
      rmmExpectOrdered(); // Only needed if rmmFunctionCalled is used in the mocked function
      rmmExpectIntEqual(b, 2);
      rmmExpectIntEqual(a, 1);
   rmmMockEndReturnInt(5);
```

Here are some functions you can use to begin the mock:

* `rmmMockBegin(functionName)` - Begins a single Mock to functionName
* `rmmMockBeginCount(functionName, count)` - Begins a mock that will be used
                                             for the next count calls to 
                                             functionName
* `rmmMockBeginAll(functionName)` - Begins a mock that will be used for all 
                                    calls to functionName. Function needs to 
                                    be called at least once.
* `rmmMockBeginAny(functionName)` - Begins a mock that will be used for all 
                                    calls to functionName 
* `rmmMockBeginOrdered(functionName)` - Ordered version of rmmMockBegin
* `rmmMockBeginOrderedCount(functionName, count)` - Ordered version of 
                                                    rmmMockBeginCount
* `rmmMockBeginOrderedAll(functionName)` - Ordered version of rmmMockBeginAll
* `rmmMockBeginOrderedAny(functionName)` - Ordered version of rmmMockBeginAny
* `rmmMockBeginIgnored(functionName)` - Order Ignored version of rmmMockBegin
* `rmmMockBeginIgnoredCount(functionName, count)` - Order Ignored version of
                                                    rmmMockBeginCount
* `rmmMockBeginIgnoredAll(functionName)` - Order Ignored version of 
                                           rmmMockBeginAll
* `rmmMockBeginIgnoredAny(functionName)` - Order Ignored version of 
                                           rmmMockBeginAny

You can call `rmmExpectOrdered()` or `rmmIgnoreOrdered()` between the begin
and end functions if you don't want to use the Ignored or Ordered begin 
functions.

If your mocked function doesn't return a value you can use the following
to end the mock definition:

* `rmmMockEnd()` - End the mock.

If your mocked function does return a value, you can use the following to
end the mock definition and specify a return value:

`rmmMockEndReturn<Datatype>(value)`


Where `<Datatype>` can be any of of the [Datatypes](#datatypes).

#### Parameter Matching

For each function call you are trying to mock, you can specify expectations
for each parameter. These checks are not done here, they are done when
the FUT calls the mocked out functions.

The general expect form is as follows:

`rmmExpect<Datatype><Comparison>(parameterName, ...)`

Where `<Datatype>` can be any of of the [Datatypes](#datatypes) and 
`<Comparison>` can be any of the [Comparisons](#comparisons). The
number of parameters after actual depends on the comparison and datatype.


There are some special ones too:

* `rmmExpectAny(parameterName)`        - This will never fail. Aka, I don't 
                                         care about the value of this parameter
* `rmmExpectTrue(parameterName)`       - This will fail if the parameter is not true
* `rmmExpectFalse(parameterName)`      - This will fail if the parameter is true
* `rmmExpectPtrNULL(parameterName)`    - This will fail if the parameter is not NULL
* `rmmExpectPtrNotNULL(parameterName)` - This will fail if the parameter is NULL



#### Parameter Outputs

Mocking functions with output parameters use the following functions:

`rmmWillOutput<Datatype>(parameterName, value, ...)`

Where `<Datatype>` can be any of of the [Datatypes](#datatypes). The 
number of parameters after actual depends on the datatype. The most commonly
used function is `rmmWillOutputMemory(parameterName, value, size)`

There are some special ones too:
* `rmmWillOutputNothing(parameterName)` - This will not change the value of 
                                          the output parameter parameterName

#### Callback

If you need more detailed parameter control. You can use callbacks:

* `rmmExpectCallback(paramterName, callbackFunction, userData)` - 
   When checking parameterName, call the callbackFunction with the
   parameter value and the userData pointer.

The callback prototype looks like this:

```c
void CallbackFunction(const void * parameterData, void * userData);
```


## Symbol Table

The Symbol Table functions allow you to "export" private module symbols
(variables or functions) for use in the tests. The Symbol Table exports
can be turned on or off at compile time.

TODO: List functions

