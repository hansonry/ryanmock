Release v1.6.0 merged the discrete setup & teardown functions to a single state function. You will need to combine your discrete setup & tear down functions into a single state function.

## Combining suite setup and teardown with rmmRunTestsCmdLine2()
If you are using suite-level setup and teardown functions with `rmmRunTestsCmdLine2()` you will need to combine them into a single state function after upgrading to release v1.6.0.

#### v1.5.0 and prior releases suite-level setup & teardown
```c
static 
void suiteSetup(void)
{
   // This runs before any test has run.
}

static
void suiteTeardown(void)
{
   // This runs after all tests have run.
}

void test_something_important(void)
{
   // Check important things
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_something_important),
   };
   return rmmRunTestsCmdLine2(tests, "mytestsuite", 
                              suiteSetup, suiteTeardown,
                              argc, args);
}
```

#### v1.6.0 and later releases suite-level state function
Using the [same prior example from v1.5.0](#v150-and-prior-releases-suite-level-setup--teardown), you can update your test suite as follows.

```c
static
void testStateFunction(enum ryanmock_state state,
                       const struct ryanmock_test * test)
{
   switch(state)
   {
   case eRMS_SuiteSetup:
      // This runs before any test has run.
      break;
   case eRMS_SuiteTeardown:
      // This runs after all tests have run.
      break;
   default:
      break;
   }
}

void test_something_important(void)
{
   // Check important things
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_something_important),
   };
   return rmmRunTestsCmdLine2(tests, "mytestsuite", 
                              testStateFunction,
                              argc, args);
}
```

## Combine discrete test case setup and teardown functions
If you are using test case level setup and teardown functions with `rmmMakeTest2()` you will need to combine them into a single state function after upgrading to release v1.6.0.

#### v1.5.0 and prior releases test case setup & teardown
```c
static
void testCaseSetup()
{
    // This runs before each test case
}

static
void testCaseTeardown()
{
    // This runs after each test case
}

void test_something_important(void)
{
   // Check important things
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest2(test_something_important, testCaseSetup, testCaseTeardown),
   };
   return rmmRunTestsCmdLine(tests, "mytestsuite", 
                             argc, args);
}
```

#### v1.6.0 and later releases test case state function
The [prior example from v1.5.0](#v150-and-prior-releases-test-case-setup--teardown) updated to a single state function.

```c
static
void testStateFunction(enum ryanmock_state state,
                       const struct ryanmock_test * test)
{
   switch(state)
   {
   case eRMS_TestSetup:
      // This runs before each test case
      break;
   case eRMS_TestTeardown:
      // This runs after each test case
      break;
   default:
      break;
   }
}

void test_something_important(void)
{
   // Check important things
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest2(test_something_important, testStateFunction),
   };
   return rmmRunTestsCmdLine(tests, "mytestsuite", 
                             argc, args);
}
```

## Only run test case setup once
v1.5.0 & prior releases executed the setup function provided in `rmmMakeTest2()` before each test case. If you are using the setup function without a teardown function and only update the signature of your setup function from `void setup(void)` to the state function signature `void setup(enum ryanmock_state, const struct ryanmock_test *)`, then your setup function will be executed both before and after each test case after you upgrade to release v1.6.0. If it is imporant that you only perform setup before each test case, then you will need to check the value of `enum ryanmock_state`.

#### v1.5.0 and prior releases setup only
```c
static
void testCaseSetup()
{
    // This runs before each test case
}

void test_something_important(void)
{
   // Check important things
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest2(test_something_important, testCaseSetup, NULL),
   };
   return rmmRunTestsCmdLine(tests, "mytestsuite", 
                             argc, args);
}
```

#### v1.6.0 and later releases state function for setup only
The [prior example from v1.5.0](#v150-and-prior-releases-setup-only) updated to state function.

```c
static
void testCaseSetup(enum ryanmock_state state,
                   const struct ryanmock_test * test)
{
   switch(state)
   {
   case eRMS_TestSetup:
      // This runs before each test case
      break;
   default:
      // We only care about test case setup for this example.
      // We're ignoring suite-level setup/teardown and
      // test case level teardown.
      break;
   }
}

void test_something_important(void)
{
   // Check important things
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest2(test_something_important, testCaseSetup),
   };
   return rmmRunTestsCmdLine(tests, "mytestsuite", 
                             argc, args);
}
```

## Using a single state function for both suite & case level setup & teardown
If desired, you are also allowed to reuse the same state function for both suite & case level setup & teardown.

Example:
```c
static
void testStateFunction(enum ryanmock_state state,
                       const struct ryanmock_test * test)
{
   switch(state)
   {
   case eRMS_SuiteSetup:
      // This runs before ALL tests have run.
      break;
   case eRMS_TestSetup:
      // This runs before EACH test case
      break;
   case eRMS_TestTeardown:
      // This runs after EACH test case
      break;
   case eRMS_SuiteTeardown:
      // This runs after ALL tests have run.
   default:
      break;
   }
}

void test_something_important(void)
{
   // Check important things
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest2(test_something_important, testStateFunction),
   };
   return rmmRunTestsCmdLine2(tests, "mytestsuite",
                              testStateFunction,
                              argc, args);
}
```