# Release Checklist

This file contains the release process for ryanmock

* [ ] Set PROJECT_NUMBER in the Doxyfile to the version (ex: v1.0.0)
* [ ] Update ReleaseNotes.md

**And Finaly:**

* [ ] Create git tag (ex: v1.0.0)

