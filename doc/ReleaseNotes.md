# Release Notes

## To Be Released

* Fixed compiling issue on mingw due to missing zu printf formatting for 
  size_t types.

## v1.6.2

* Used specific python compiler executable instead of letting the build
  system pick one (Internal Fix).
* Added SetupTeardownMigrationToStateFunction.md. Thanks Mike Bullis!

## v1.6.1

* Fixed asm directive to work without warnings on clang (Internal Fix).

## v1.6.0

* Slightly better information when missing a return mock.
* Changed setup and teardown functions to be one state function. The
  state function for the suite will be called before and after the suite
  is run and before and after each test. Asserts and mocking will work
  in these callbacks. [Migration guide](SetupTeardownMigrationToStateFunction.md)
* Added command line option to just list the test names.
* Added command line option to order tests in a "combination" way. This
  will allow test writers to run every test after every other test and
  make it easy to spot bad initial conditions.
* Updated solo command so it can now take multiple test names including
  taking `*` as a wild card.
* Added a command line argument -b/--break that will trigger a breakpoint
  when a test has a bad result. Useful for easy debugging of tests.
* Added a command line argument -f/--fill to attempt to fill the stack 
  memory with default value to make it easier to check for uninitialized 
  memory in tests.
* DataTypes Added:
    * IntPtrU
    * IntPtr

## v1.5.0

* Arrays and Strings cleaned up. Better output.
* Fixed wording to be more clear when a function was called without a mock.
* Added `Any` prefixed mock begin function to allow for no 
  expected function calls.
* Selecting color output will now force color output on *nix systems instead
  of checking to see if the terminal supports it (ctest color output).

## v1.4.0

* Removed extra newlines that are put into function mocking failure output.
* Added Color console output text when available.
* Added a way to create dynamic stack records for for loops and function calls.
* Strings are now considered Null Terminated by default
* Null Terminated strings are now checked for length and the code used
  to show the difference between to strings has been updated to handle 
  differing lengths.

## v1.3.0

* Added Function CheatSheet document.
* Removed duplicate Int datatype definition
* Fixed Calculator example
* Added 64 bit integer support (uint64_t and int64_t)
* Fixed a very bad bug that caused sized signed integer functions to not be
  named correctly and not found at link time.

## v1.2.0

* Fixed up issues returning arrays.
* Added Array and VoidArray to lists.
* Test functions now work with higher warning levels without producing warnings.
* Added rmmWillOutputNothing for doing nothing with output parameters.
* Renamed `rmmMockWillOutputXXXX` to `rmmWillOutputXXXX`
* Renamed return commands and made them also act as Mock End statements
* Added variants of `rmmMockBegin` that include ignore or ordered statements.
* DataTypes Added:
    * Float

## v1.1.0

* Better feedback on test errors
* Added documentation Mocking Guide 
* DataTypes Added:
    * ULong
    * Enum
    * IntU8
    * IntU16
    * IntU32
    * Bool
* Data size is now considered when mocking and checking data.
* Test Skipping using rmmSkip()
* Interface libraries for rmsymboltable

Bug Fixes:

* Issue with sign bit extension on Int Types
* Actual and expected values getting swapped on first difference report.
* Memory Leaks found and cleaned up using Valgrind. 

## v1.0.1

* CMakeLists.txt now has options to not build examples and not build and run
  tests. This is so the project can be easily integrated into other CMake
  projects.
* Cleaned up compile errors and warnings on clang. It's more sensitive.
* Updated Documentation.


## v1.0.0

* Too much to note

