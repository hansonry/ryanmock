# Mocking Guide

This document will go over the details on how to write mocks in ryanmock.

[[_TOC_]]

## General Mocking Overview

This section will cover basic unit test terminology. Feel free to skip this
section if you are experienced with unit testing.

*Definitions:*

 * FUT (Function Under Test) - The function that you are unit testing.
 * Test - Code that calls the FUT to exercise it and verify that it is working.
 * Stub - A replacement of an existing function that the FUT will call. Stubs
          do nothing and usually return successful status.
 * Fake - A replacement of an existing function that the FUT will call. Fakes
          are specialized hand coded functions.
 * Mock - A replacement of an existing function that the FUT will call. Mocks
          are generic functions who's inputs and outputs can be checked and
          controlled by the test. 99% of the time you can use Mocks in place 
          of Stubs or Fakes.

Tests will always call FUT (Function Under Test) but never Stubs, Fakes, 
or Mocks. Stubs, Fakes, and/or Mocks will be called by the FUT.

An example tests call graph will look something like this:

 * Test
     * FUT A
         * STUB A
         * MOCK B
         * STUB A
         * FAKE C
         * STUB A
     * FUT B
         * MOCK D
         * MOCK E
         * STUB A
         * MOCK F
        

### Integration

In order to test you need to find a way to replace the function calls that
your FUT (Function Under Test) makes with Mocks, Fakes, and/or Stubs.

You can:

 * Build your FUT as a stand alone module.
 * Use Link Seaming 
 
## Mocking in Ryanmock

Mocking is done by creating Mocked functions. 


### Basic Example

Let's say you have a function prototype like this:

```c
// In ExampleMath.h
int AddTwoNumbers(int a, int b);
```

To mock this function you would do:

```c
#include <ExampleMath>

int AddTwoNumbers(int a, int b)
{
   rmmFunctionCalled(); // Optional
   rmmParamCheck(a);
   rmmParamCheck(b);
   rmmMockReturn(int);
}
```

To Control this mocked function in your test you would write the following:

```c
void TestFunction(void)
{
   rmmMockBegin(AddTwoNumbers);
      rmmExpectOrdered(); // Only needed if rmmFunctionCalled is used in the mocked function
      rmmExpectIntEqual(b, 2);
      rmmExpectIntEqual(a, 1);
   rmmMockEndReturnInt(5);
   
   VeryCoolFunctionUnderTest();
}
```

In this example we are only expecting the mocked function
`AddTwoNumbers` to be called once by `VeryCoolFunctionUnderTest`. 
We are expecting the parameters `a` to be 1 and `b` to be 2. We will
be returning 5 back to `VeryCoolFunctionUnderTest`. Also notice that
5 != 1 + 2. You don't need your mocks to output values that make sense.


In the mocked function `rmmFunctionCalled` is an optional call ordering 
statement. If this is called, then the test needs to either enforce ordering
with `rmmExpectOrdered` as seen above, or ignore ordering with 
`rmmIgnoreOrdered`. 

Notice in the mocked function the calls to `rmmParamCheck`. Each take a 
parameter as their input. Notice the same parameter names are used
in the test functions `rmmExpectIntEqual`. Here `rmmExpectIntEqual(a, 1)`
check that `VeryCoolFunctionUnderTest` has passed in 1 for `a`.

Notice in the mocked function the call to `rmmMockReturn` the parameter
specified (`int`) should be the same as the return type of the function.
`rmmMockReturn` contains a `return` statement in it. So you don't need a 
return statement for this function and code after `rmmMockReturn` will not
be executed.

Notice that the order of `rmmParamCheck` and `rmmExpectIntEqual` for parameters
`a` and `b` don't match. This is allowed.

When Mocking, all check values, return values, and output values are stored. 
So if running the test changes values, that will not be reflected. If you need
to do stuff like that, look into `rmmExpectCallback`.

If you look though the documentation. You will notice that there are many forms
of `rmmExpectInt`:

 * `rmmExpectIntEqual`
 * `rmmExpectIntNotEqual`
 * `rmmExpectIntGreaterThan`
 * `rmmExpectIntGreaterThan`
 * `rmmExpectIntGreaterThan`
 * `rmmExpectIntGreaterThanOrEqualTo`
 * `rmmExpectIntLessThan`
 * `rmmExpectIntLessThanOrEqualTo`
 * `rmmExpectIntLessThanOrEqualTo`
 * `rmmExpectIntBetween`
 * `rmmExpectIntNotBetween`
 * `rmmExpectIntWithin`
 * `rmmExpectIntNotWithin`

For each data type you will find similar options. There are also some special
options for some data types. For example with booleans, you can use 
`rmmExpectTrue(param);` instead of `rmmExpectBoolEqual(param, true);`.

### Ignoring

Sometimes you don't care if a function under test was called or what was 
passed into parameters.

If you take the FUT and Mock from above. If you didn't care about what the
FUT passed into `a` or `b`. Then you could do the following:

```c
void TestFunction(void)
{
   rmmMockBegin(AddTwoNumbers);
      rmmExpectOrdered(); 
      rmmExpectAny(a);
      rmmExpectAny(b);
   rmmMockEndReturnInt(-5);
   
   VeryCoolFunctionUnderTest();
}
```

Notice the calls to `rmmExpectAny`.

Sometimes you have a function that is called often and you don't care about.
You could do something like this:

```c
   rmmMockBeginAll(AddTwoNumbers);
      rmmIgnoreOrdered(); 
      rmmExpectAny(a);
      rmmExpectAny(b);
   rmmMockEndReturnInt(0);
```

Notice that we are beginning the mock with `rmmMockBeginAll` and we are using
`rmmIgnoreOrdered`. This expects at least one call to the function. Use
the `rmmMockBeginAny` version if there could be 0 calls to the function.

Note that you will always need to return or output something for these mocks.
You will also want to do this before any of your ordered mocks in the test.



### Arrays

Some times functions take an array of values. For example:

```c
int Sum(const int * nums, int count);
```

You would mock this as follows:

```c
int Sum(const int * nums, int count)
{
   rmmFunctionCalled(); // Optional
   rmmParamCheck(count);
   rmmParamCheckArray(nums, count);
   rmmMockReturn();
}
```

You would check the array as follows:

```c
void TestFunction(void)
{
   int expectedInput[] = {1, 2, 3, 4, 5};
   rmmMockBegin(Sum);
      rmmExpectOrdered(); 
      rmmExpectIntEqual(count, 5);
      rmmExpectMemoryEqual(nums, expectedInput, 5);
   rmmMockEndReturnInt(15);
   
   VeryCoolFunctionUnderTest();
}
```

Notice that the last parameter of `rmmExpectMemoryEqual` is the size. Notice,
the size is in elements not bytes. The element size is taken from the expected
value (`expectedInput` in this example).

Sometimes arrays can be defined with `[]` instead of pointers. For example:

```c
int Sum(const int nums[], int count);
```

Just convert that back to the pointer form in the mock. The compiler doesn't
seem to mind.

```c
int Sum(const int * nums, int count)
{
   // Same as above
}
```

### Outputs

Sometimes you will have an output parameter in your function. For example:

```c
struct data
{
   int a;
   int b;
   int c;
};
void ReadData(struct data * data);
```

You can mock that like so:

```c
void ReadData(struct data * data)
{
   rmmFunctionCalled();
   rmmParamCheckArray(data, 1);
   rmmMockParam(data, 1);
}
```

You would control the mock as follows in test like this:

```c
void TestFunction(void)
{
   struct outputData = {
      .a = 1,
      .b = 2,
      .c = 3
   };
   rmmMockBegin(ReadData);
      rmmExpectOrdered(); 
      rmmExpectAny(data);
      rmmWillOutputMemory(data, &outputData, 1);
   rmmMockEndReturnInt(15);
   
   VeryCoolFunctionUnderTest();
}
```

Notice how mocking and output and checking an output are two different steps.
Also notice that `rmmWillOutputMemory` takes a size. This size is also
in elements not bytes.


You will also come across reading and array of data.

```c
int ReadNumbers(int * buffer, int bufferSize);
```

You can mock that like so:

```c
int ReadNumbers(int * buffer, int bufferSize)
{
   rmmFunctionCalled();
   rmmParamCheckArray(buffer, bufferSize);
   rmmMockParam(buffer, bufferSize);
   rmmMockReturn(int);
}
```

You would control the mock as follows in test like this:

```c
void TestFunction(void)
{
   int outputNumbers = {1, 2, 3, 4};
   rmmMockBegin(ReadNumbers);
      rmmExpectOrdered(); 
      rmmExpectIntEqual(bufferSize, 5);
      rmmExpectAny(buffer);      
      rmmWillOutputMemory(data, &outputNumbers, 4);
   rmmMockEndReturnInt(4);
   
   VeryCoolFunctionUnderTest();
}
```

If you don't want to change the memory passed in, you can just write nothing:

```c
void TestFunction(void)
{
   rmmMockBegin(ReadNumbers);
      rmmExpectOrdered(); 
      rmmExpectIntEqual(bufferSize, 5);
      rmmExpectAny(buffer);      
      rmmWillOutputMemory(data, NULL, 0);
   rmmMockEndReturnInt(0);
   
   VeryCoolFunctionUnderTest();
}
```

### Custom Callbacks

Sometimes you need to do something special or you care about a specific part
of a structure. You will want to use `rmmExpectCallback`.

You will need to create a callback function as follows:

```c
// function type is based on rmcheck_function typedef.
void DataCheck(const void * parameterData, void * userData)
{
   const struct data * actual   = *(const struct data * const *)parameterData;
   const struct data * expected =  userData;
   
   rmmAssertIntEqual(actual->a, expected->a);
   rmmAssertIntEqual(actual->c, expected->c);
}

void TestFunction(void)
{
   struct data expectedData = {
      .a = 1,
      .b = 0, // Don't care
      .c = 10
   };
   rmmMockBegin(WriteData);
      rmmExpectOrdered(); 
      rmmExpectCallback(data, DataCheck, &expectedData);
   rmmMockEnd();
   
   VeryCoolFunctionUnderTest();
}
```

Notice that you can use asserts in your callback function. You can also
use this to change or check values that change while the 
FUT (Function Under Test) is running.

