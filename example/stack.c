/**
 * @file
 *
 * Simple Example test used in the Readme
 */
#include <ryanmock.h>

#define util_function() _util_function(__FILE__, __LINE__)
static inline
void _util_function(const char * filename, int line)
{
   int token;
   rmmStackPush(filename, line, &token, "util_function");
   rmmAssertIntEqual(4, 5); // Or any other checks or mocks you want to do
   rmmStackPop(&token);
}

static 
void failing_test_util(void)
{
   rmmAssertIntEqual(5, 5);
   util_function();
}

static
void failing_test_for_loop(void)
{
   int i;
   for(i = 0; i < 10; i++)
   {
      int token;
      rmmStackPushHere(&token, "For Loop Index: %d", i);
      rmmAssertIntNotEqual(i, 5); // Or any other checks or mocks you want to do
      rmmStackPop(&token);
   }
}

static 
void failing_dontdothis1(void)
{
   int token;
   rmmEnterFunction(util_function);
   rmmStackPop(&token);
}

static 
void failing_dontdothis2(void)
{
   int token;
   rmmStackPushHere(&token, "Nope");
   rmmExitFunction(Nope);
   
   rmmStackPushHere(&token, "Nope");
   rmmExitFunction(util_function);
}

static 
void failing_dontdothis3(void)
{
   int token;
   rmmStackPushHere(&token, "Layer 1");
   rmmStackPushHere(&token, "Layer 2");
   rmmStackPop(&token);
   rmmStackPop(&token);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(failing_test_util),
      rmmMakeTest(failing_test_for_loop),
      rmmMakeTest(failing_dontdothis1),
      rmmMakeTest(failing_dontdothis2),
      rmmMakeTest(failing_dontdothis3),
   };
   return rmmRunTestsCmdLine(tests, NULL, argc, args);
}

