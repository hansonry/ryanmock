/**
 * @file
 *
 * Asserts example test
 */
#include <stdio.h>
#include <ryanmock.h>

static void test_fail(void)
{
   rmmAssertTrue(3 == 9);
}

static void subfunction(int a, int b)
{
   rmmAssertTrue(a == b);
}

static void test_fail_in_subfunction(void)
{
   rmmCallFunction(
      subfunction(1, 1)
      );
   rmmCallFunction(
      subfunction(1, 2)
      );
}

static void test_sint_equal_fail(void)
{
   rmmAssertIntEqual(0 - 1, 0 - 3);
}

static void test_uint_equal_fail(void)
{
   rmmAssertUIntEqual(0 - 1, 0 - 3);
}

static void test_sint_notequal_fail(void)
{
   rmmAssertIntNotEqual(-3, 0 - 3);
}

static void test_uint_notequal_fail(void)
{
   rmmAssertUIntNotEqual(3, 2 + 1);
}

static void test_segfault(void)
{
   int * v = NULL;
   *v = 5;
   printf("We shouldn't get here\n");
}

static void test_pass(void)
{
   rmmAssertTrue(1);
   rmmAssertIntEqual(10, 10);
   rmmAssertUIntNotEqual(1, 2);
}

static void test_empty(void)
{
}


static void test_custom_type_check(void)
{
   int actual[]   = { -1, -2, -3, -4, -5, 10 };
   int expected[] = { -1, -2, -3,  4, -5, 10 };

   // Building a custom array of signed integers type
   struct ryanmock_type * signedIntType = rmCreateTypeIntegerInt();
   struct ryanmock_type * signedIntArray = rmCreateTypeArray(signedIntType, 6);
  
   // We no longer need access to the signedIntType so we are releasing it here
   // if you don't do this it will become a leak. 
   rmTypeRelease(signedIntType);
   
   rmmAssertType2(eRMCO2_Equal, signedIntArray, actual, expected);
   

}




int main(int argc, char * args[])
{
   struct ryanmock_config config;
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_fail),
      rmmMakeTest(test_sint_equal_fail),
      rmmMakeTest(test_uint_equal_fail),
      rmmMakeTest(test_sint_notequal_fail),
      rmmMakeTest(test_uint_notequal_fail),
      rmmMakeTest(test_fail_in_subfunction),
      rmmMakeTest(test_segfault),      
      rmmMakeTest(test_pass),
      rmmMakeTest(test_empty),
      rmmMakeTest(test_custom_type_check),
   };
   rmmConfigTestsSimple(&config, tests, "asserts");
   config.xmlOutputFilename = "asserts.xml"; // Optional
   return rmRunTests(&config);
}
