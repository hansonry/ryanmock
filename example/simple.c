/**
 * @file
 *
 * Simple Example test used in the Readme
 */
#include <ryanmock.h>

static void passing_test(void)
{
   rmmAssertIntEqual(5, 5);
}

static void failing_test(void)
{
   rmmAssertTrue(3 == 9);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_test),
      rmmMakeTest(failing_test),
   };
   return rmmRunTestsCmdLine(tests, NULL, argc, args);
}
