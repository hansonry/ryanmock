/**
 * @file
 *
 * Calculator source code to test.
 */
#include "calculator.h"
#include "math_functions.h"


int calculate(enum operation operation, int a, int b)
{
   int result;
   switch(operation)
   {
   default:
   case eO_add:
      result = mf_add(a, b);
      break;
   case eO_subtract:
      result = mf_subtract(a, b);
      break;
   case eO_divide:
      result = mf_divide(a, b);
      break;
   case eO_multiply:
      result = mf_multiply(a, b);
      break;
   }
   return result;
}
