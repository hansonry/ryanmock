/**
 * @file
 *
 * Function to test for the calculator example.
 */

#ifndef __CALCULATOR_H__
#define __CALCULATOR_H__

enum operation
{
   eO_add,
   eO_subtract,
   eO_multiply,
   eO_divide
};

int calculate(enum operation operation, int a, int b);

#endif // __CALCULATOR_H__
