/**
 * @file
 *
 * Mocked functions used by calculator example
 */
#include <ryanmock.h>

int mf_add(int a, int b)
{
   rmmParamCheck(a);
   rmmParamCheck(b);
   rmmMockReturn(int);
}

int mf_subtract(int a, int b)
{
   rmmParamCheck(a);
   rmmParamCheck(b);
   rmmMockReturn(int);
}

int mf_multiply(int a, int b)
{
   rmmParamCheck(a);
   rmmParamCheck(b);
   rmmMockReturn(int);
}

int mf_divide(int a, int b)
{
   rmmParamCheck(a);
   rmmParamCheck(b);
   rmmMockReturn(int);
}
