/**
 * @file
 *
 * Calculator example test.
 */
#include <ryanmock.h>
#include "calculator.h"

static void test_add(void)
{
   rmmMockBegin(mf_add);
      rmmExpectIntEqual(a, 2);
      rmmExpectIntEqual(b, 3);
   rmmMockEndReturnInt(2);
   
   // FUT
   rmmAssertIntEqual(2, calculate(eO_add, 2, 3));
   rmmFUTCalled();
   
   rmmMockBegin(mf_add);
      rmmExpectIntEqual(a, 5);
      rmmExpectIntEqual(b, 6);
   rmmMockEndReturnInt(5);
   
   // FUT
   rmmAssertIntEqual(5, calculate(eO_add, 5, 6));
   rmmFUTCalled();
}

// This test will fail as part of the example.
// There is a test error in this test.
static void test_add_with_mistake(void)
{
   rmmMockBegin(mf_add);
      // Whoops a should be -3
      rmmExpectIntEqual(a, -5);
      rmmExpectIntEqual(b, 3);
   rmmMockEndReturnInt(2);
   
   // FUT
   rmmAssertIntEqual(2, calculate(eO_add, -3, 3));
   rmmFUTCalled();   
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_add),
      rmmMakeTest(test_add_with_mistake),
   };
   return rmmRunTestsCmdLine(tests, NULL, argc, args);
}
