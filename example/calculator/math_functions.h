/**
 * @file
 *
 * Header File for calculator example 
 */
#ifndef __MATH_FUNCTIONS_H__
#define __MATH_FUNCTIONS_H__

int mf_add(int a, int b);
int mf_subtract(int a, int b);
int mf_multiply(int a, int b);
int mf_divide(int a, int b);

#endif // __MATH_FUNCTIONS_H__
