# TODO

## Date: 2022.03.23

**New Items:**

* Create SymbolTable

**Moved Items:**

* Update Readme file

** Completed Items: **

* Add function callback capablities on expects
* Test Order Randomization
* Complete Doxygen on all user side functions

** Abondoned Items: **

None

## Date: 2022.03.21

**New Items:**

* Update Readme file

**Moved Items:**

* Add function callback capablities on expects
* Test Order Randomization
* Complete Doxygen on all user side functions

** Completed Items: **

* Get Repeated Function calls working

** Abondoned Items: **

None


## Date: 2022.03.16

**New Items:**

None

**Moved Items:**

* Get Repeated Function calls working
* Add function callback capablities on expects
* Test Order Randomization
* Complete Doxygen on all user side functions

** Completed Items: **

* Get Function Call ordering working
* Get Ignore Funcion calls working

** Abondoned Items: **

None


## Date: 2022.03.05

**New Items:**

* Get Function Call ordering working
* Get Repeated Function calls working
* Get Ignore Funcion calls working
* Add function callback capablities on expects
* Test Order Randomization
* Complete Doxygen on all user side functions

**Moved Items:**

None

** Completed Items: **

* Get Arrays Diffrence working because that is hard to do.
* Adjust first diffrence and print out code (Arrays).
* Header File Code generation Script
* Strings length computatoin is done in the string function to prevent
  Double calls due to macros.

** Abondoned Items: **

None

## Date: 2021.12.21

**New Items:**

* Get Arrays Diffrence working because that is hard to do.

**Moved Items:**

* Adjust first diffrence and print out code (Arrays).

** Completed Items: **

None

** Abondoned Items: **

* Figure out how to create array types (think about strings)
    * This is too intense right now. I want to focus on getting a release
      out the door.
      
**Probably ok to Ignore for Now:**

None

## Date: 2021.12.06

**New Items:**

* Figure out how to create array types (think about strings)
* Adjust first diffrence and print out code (Arrays).

**Moved Items:**

* None

**Probably ok to Ignore for Now:**
* Doubles
* Pointer Greater than/less than/etc...
* 

## Date: 2021.11.18

**New Items:**

* Break headder file into two parts so it isn't as cluttered.
* Add function name parameter to capture the macro name.
* Test the rest of the integer expects.

**Moved Items:**

None

**Probably ok to Ignore for Now:**

* Ignore Mocked Function Calls
* Documentation
* Website
* Support for Types that are built from other types (ex: Arrays of Floats or
  Structures).


