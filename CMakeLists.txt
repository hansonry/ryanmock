cmake_minimum_required(VERSION 3.10)


find_package(Python3 COMPONENTS Interpreter)

# set the project name
project(ryanmock)

option(RYANMOCK_BUILD_LIBRARY "Enable Building the ryanmock library"       ON)
option(RYANMOCK_BUILD_EXAMPLE "Enable Building ryanmock Examples"          ON)
option(RYANMOCK_RUN_TESTS     "Enable Building and Running ryanmock Tests" ON)

set(RYANMOCK_GENERATED_FILE_PATH ${CMAKE_CURRENT_BINARY_DIR})

set(RYANMOCK_INCLUDE_DIRS include ${RYANMOCK_GENERATED_FILE_PATH})

if(RYANMOCK_BUILD_LIBRARY)
   # Generate Header Macro File
   set(RYANMOCK_GENERATED_HEADER ${RYANMOCK_GENERATED_FILE_PATH}/ryanmock_gen.h)
   set(RYANMOCK_GENERATOR_SCRIPT ${CMAKE_CURRENT_SOURCE_DIR}/devtools/headergen.py)
   add_custom_command(
      OUTPUT ${RYANMOCK_GENERATED_HEADER}
      COMMAND ${PYTHON3_EXECUTABLE} ${RYANMOCK_GENERATOR_SCRIPT} ${RYANMOCK_GENERATED_HEADER}
      DEPENDS ${RYANMOCK_GENERATOR_SCRIPT})
   

   # Main ryanmock library
   add_library(ryanmock STATIC src/ryanmock.c ${RYANMOCK_GENERATED_HEADER})
   target_include_directories(ryanmock PUBLIC 
      ${RYANMOCK_INCLUDE_DIRS})
   
   # Max warnings
   if(MSVC)
     target_compile_options(ryanmock PRIVATE /W4 /WX)
   else()
     target_compile_options(ryanmock PRIVATE -Wall -Wextra -pedantic -Werror)
   endif()

   # Build the symbol table 
   add_library(rmsymboltable_on INTERFACE)
   target_include_directories(rmsymboltable_on  INTERFACE ${RYANMOCK_INCLUDE_DIRS})
   target_compile_definitions(rmsymboltable_on  INTERFACE RMSYMBOLTABLE)
   
   if(RYANMOCK_BUILD_EXAMPLE)
      add_subdirectory(example)
   endif()
   if(RYANMOCK_RUN_TESTS)
      add_subdirectory(test)
   endif()
endif()

# Always make the off version of the symbol table avialable
add_library(rmsymboltable_off INTERFACE)
target_include_directories(rmsymboltable_off INTERFACE ${RYANMOCK_INCLUDE_DIRS})




