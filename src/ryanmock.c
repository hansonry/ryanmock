#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <signal.h>
#include <limits.h>
#include <time.h>
#include <errno.h>
#include <ryanmockutil.h>

#if defined(__MINGW32__)
#define RYANMOCK_SYS_MINGW 1
#elif defined(_WIN32) || defined(_WIN64)
#define RYANMOCK_SYS_WINDOWS 1
#else
#define RYANMOCK_SYS_NIX 1
#endif // __WIN32

#if RYANMOCK_SYS_WINDOWS || RYANMOCK_SYS_MINGW
#include <Windows.h>
#include <intrin.h>
typedef void (*sighandler_t)(int);
#endif // RYANMOCK_SYS_WINDOWS

#if RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW
#include <unistd.h>
#endif // RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW

#if RYANMOCK_SYS_NIX
#define sighandler_t __sighandler_t
#endif // RYANMOCK_SYS_NIX

// size_t printing workaround
#include <inttypes.h>
#ifdef _WIN32
#ifdef _WIN64
#define PRI_SIZET PRIu64
#else // WIN64
#define PRI_SIZET PRIu32
#endif // _WIN64
#else // _WIN32
#define PRI_SIZET "zu"
#endif // _WIN32


#define DECLARE_CAST(type, dest, src) type * dest = (type *)src

#define JUMP_VALUE_NOJUMP   0
#define JUMP_VALUE_FAILURE  1
#define JUMP_VALUE_ERROR    2
#define JUMP_VALUE_SKIPPED  3

#define RM_TEXT_SKIP    "[     SKIP ] "
#define RM_TEXT_SKIPPED "[  SKIPPED ] "
#define RM_TEXT_FAIL    "[     FAIL ] "
#define RM_TEXT_FAILURE "[  FAILURE ] "
#define RM_TEXT_FAILED  "[  FAILED  ] "
#define RM_TEXT_ERROR   "[  ERROR   ] "
#define RM_TEXT_INFO    "[     INFO ] "
#define RM_TEXT_DIV     "[==========] "
#define RM_TEXT_PASSED  "[  PASSED  ] "
#define RM_TEXT_RUN     "[ RUN      ] "
#define RM_TEXT_OK      "[       OK ] "





enum ryanmock_term_color
{
   eRMTC_Default,
   eRMTC_Red,
   eRMTC_Green,
   eRMTC_Yellow
};
enum ryanmock_color_system
{
   eRMCS_None,
   eRMCS_Nix,
   eRMCS_Windows
};

static enum ryanmock_color_system rmColorPrintSystem = eRMCS_None;

static inline
void TriggerBreakpoint(void)
{
#if RYANMOCK_SYS_NIX
   // GDB/Linux only
   (void)raise(SIGTRAP);
#elif RYANMOCK_SYS_WINDOWS || RYANMOCK_SYS_MINGW
   __debugbreak(); 
#endif
}


static inline
bool CharIsLetter(char c)
{
   return (c >= 'A' && c <= 'Z') ||
          (c >= 'a' && c <= 'z');
}

static inline
bool CharEqualsCaseInsensitive(char src, char cmp)
{
   if(src == cmp)
   {
      return true;
   }
   if(!CharIsLetter(src) || !CharIsLetter(cmp))
   {
      return false;
   }
   if(src > cmp)
   {
      cmp += 'a' - 'A';
   }
   else
   {
      src += 'a' - 'A';
   }
   return src == cmp;
   
}

static inline
bool StringEndsWithCaseInsensitive(const char * str, const char * cmp)
{
   const char * cmpWorking = cmp;
   while(*str != '\0')
   {
      if(CharEqualsCaseInsensitive(*str, *cmpWorking))
      {
         cmpWorking ++;
      }
      else
      {
         cmpWorking = cmp;
      }
      str ++;
   }
   return *cmpWorking == '\0';
}

#if RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW
static inline
bool rmColorPrintIsValidTerminal(void)
{
   const char * termStr = getenv("TERM");
   return termStr != NULL && (
          strcmp(termStr, "xterm")        == 0 ||
          strcmp(termStr, "xterm-color")  == 0 ||
          strcmp(termStr, "screen")       == 0 ||
          strcmp(termStr, "tmux")         == 0 ||
          strcmp(termStr, "rxvt-unicode") == 0 ||
          strcmp(termStr, "linux")        == 0 ||
          strcmp(termStr, "cygwin")       == 0 ||
          StringEndsWithCaseInsensitive(termStr, "-256color"));
}
#endif // RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW

static inline
void rmColorPrintInit(enum ryanmock_config_color_output colorOutput)
{
   
#if RYANMOCK_SYS_NIX
   switch(colorOutput)
   {
   case eRMCCO_Enabled:
      rmColorPrintSystem = eRMCS_Nix;
      break;
   case eRMCCO_EnableIfPossible:      
      if(isatty(fileno(stdout)) > 0 && 
         rmColorPrintIsValidTerminal())
      {
         rmColorPrintSystem = eRMCS_Nix;
      }
      break;
   case eRMCCO_Disabled:
      rmColorPrintSystem = eRMCS_None;
      break;
   }
#endif // RYANMOCK_SYS_NIX
#if RYANMOCK_SYS_MINGW
   // MSYSTEM will be something when in an MSYS terminal
   // on a Windows DOS shell it will be nothing.
   enum ryanmock_color_system colorEnabled;
   const char * mSystemStr = getenv("MSYSTEM");
   if(mSystemStr == NULL)
   {
      colorEnabled = eRMCS_Windows;
   }
   else if(rmColorPrintIsValidTerminal())
   {
      colorEnabled = eRMCS_Nix;
   }
   
   switch(colorOutput)
   {
   case eRMCCO_Enabled:
      rmColorPrintSystem = colorEnabled;
      break;
   case eRMCCO_EnableIfPossible:      
      rmColorPrintSystem = colorEnabled;
      break;
   case eRMCCO_Disabled:
      rmColorPrintSystem = eRMCS_None;
      break;
   }   
#endif // RYANMOCK_SYS_MINGW

#if RYANMOCK_SYS_WINDOWS
   switch(colorOutput)
   {
   case eRMCCO_Enabled:
      rmColorPrintSystem = eRMCS_Windows;
      break;
   case eRMCCO_EnableIfPossible:      
      rmColorPrintSystem = eRMCS_Windows;
      break;
   case eRMCCO_Disabled:
      rmColorPrintSystem = eRMCS_None;
      break;
   }   
#endif // RYANMOCK_SYS_WINDOWS
}

#if RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW
static inline
const char * rmColorPrintGetAnsiColorCode(enum ryanmock_term_color color)
{
   const char * colorStr;
   switch(color)
   {
   case eRMTC_Green:
      colorStr = "2";
      break;
   case eRMTC_Red:
      colorStr = "1";
      break;
   case eRMTC_Yellow:
      colorStr = "3";
      break;
   default:      
      colorStr = "7";
      break;
   }
   return colorStr;
}
#endif // RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW

#if RYANMOCK_SYS_WINDOWS || RYANMOCK_SYS_MINGW

static inline
WORD GetForgroundColor(enum ryanmock_term_color color)
{
   WORD out;
   switch(color)
   {
   default:
      out = FOREGROUND_INTENSITY;
      break;
   case eRMTC_Red:
      out = FOREGROUND_INTENSITY | FOREGROUND_RED;
      break;
   case eRMTC_Green:
      out = FOREGROUND_INTENSITY | FOREGROUND_GREEN;
      break;
   case eRMTC_Yellow:
      out = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN;
      break;
   
   }
   return out;
}

static inline
WORD GetNewColor(enum ryanmock_term_color color, WORD oldColorAttrs)
{
#define FS(field, flag) (((field) & (flag)) == (flag))
   // Let's reuse the BG
   static const WORD backgroundMask = BACKGROUND_BLUE | BACKGROUND_GREEN |
                                      BACKGROUND_RED  | BACKGROUND_INTENSITY;
   const WORD existingBackground = oldColorAttrs & backgroundMask;
   WORD newColor = GetForgroundColor(color) | existingBackground;
   
   // Drop intensity if the background matches the foreground exactly
   if(FS(newColor, BACKGROUND_BLUE)      == FS(newColor, FOREGROUND_BLUE)  &&
      FS(newColor, BACKGROUND_GREEN)     == FS(newColor, FOREGROUND_GREEN) &&
      FS(newColor, BACKGROUND_RED)       == FS(newColor, FOREGROUND_RED)   &&
      FS(newColor, BACKGROUND_INTENSITY) == FS(newColor, FOREGROUND_INTENSITY))
   {
      newColor ^= FOREGROUND_INTENSITY;
   }
   return newColor;
#undef FS
}

#endif // RYANMOCK_SYS_WINDOWS || RYANMOCK_SYS_MINGW

static inline
void rmColorVPrintf(enum ryanmock_term_color color, const char * format, 
                    va_list arg)
{
   if(rmColorPrintSystem != eRMCS_None && color != eRMTC_Default)
   {
#if RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW
      if(rmColorPrintSystem == eRMCS_Nix)
      {
         printf("\033[0;3%sm", rmColorPrintGetAnsiColorCode(color));
         vprintf(format, arg);
         printf("\033[m");
      }
#endif // RYANMOCK_SYS_NIX || RYANMOCK_SYS_MINGW

#if RYANMOCK_SYS_WINDOWS || RYANMOCK_SYS_MINGW
      if(rmColorPrintSystem == eRMCS_Windows)
      {
         const HANDLE stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
         CONSOLE_SCREEN_BUFFER_INFO bufferInfo;
         GetConsoleScreenBufferInfo(stdoutHandle, &bufferInfo);
         const WORD oldColorAttrs = bufferInfo.wAttributes;
         const WORD newColor = GetNewColor(color, oldColorAttrs);
      
         fflush(stdout);
         SetConsoleTextAttribute(stdoutHandle, newColor);
      
         vprintf(format, arg);
         fflush(stdout);
      
         SetConsoleTextAttribute(stdoutHandle, oldColorAttrs);
      }
      
#endif //RYANMOCK_SYS_WINDOWS || RYANMOCK_SYS_MINGW
   }
   else
   {
      vprintf(format, arg);
   }
}

static inline
void rmColorPrintf(enum ryanmock_term_color color, const char * format, ...)
{
   va_list arg;
   va_start(arg, format);
   rmColorVPrintf(color, format, arg);
   va_end(arg);
}

#ifdef GNUC
#define __asm__ asm
#define __volatile__ volatile
#endif // GNUC

#if defined(__x86_64__) || defined(_M_X64) || defined(i386) || defined(__i386__) || defined(__i386) || defined(_M_IX86)
#define RM_ARCH_X86
#endif
static
void rmFillStackData(size_t bytes, unsigned char value)
{
   if(bytes > 0)
   {
      // If anyone knows a better way to do this, please let me know
#ifdef RM_ARCH_X86
      // This works by assuming that the last parameter "value" is at the bottom
      // of the stack. So writing below it is fine. This also assumes the 
      // address of the stack pointer get smaller as the stack gets fuller.
      __asm__ __volatile__ (
         "loop:\n\t"
         "mov %1,(%2)\n\t"
         "dec %2\n\t"
         "dec %0\n\t"
         "jne loop\n"
         : /* No Outputs */
         : "r"(bytes),"r"(value),"r"(&value)
         : "memory");
#endif
   }
}

static inline
bool rmFillStackIsSupported(void)
{
#ifdef RM_ARCH_X86
   return true;
#else
   return false;
#endif
}



struct ryanmock_stack_frame
{
   const char * filename;
   int line;
   const void * token;
   char * text;
};

static inline
void rmStackFrameInitFunction(struct ryanmock_stack_frame * frame, 
                              const char * fileName,
                              int line,
                              const char * functionName)
{
   frame->text     = strdup(functionName);
   frame->filename = fileName;
   frame->line     = line;
   frame->token    = NULL;
}

static inline
void rmStackFrameInitVTextf(struct ryanmock_stack_frame * frame, 
                            const char * fileName,
                            int line,
                            const void * token,
                            const char * format, va_list args)
{
   va_list args2;
   int sizeOfFinalString;
   
   frame->filename = fileName;
   frame->line     = line;
   frame->token    = token;
   va_copy(args2, args);
   sizeOfFinalString = vsnprintf(NULL, 0, format, args);
   if(sizeOfFinalString >= 0)
   {
      frame->text = malloc(sizeOfFinalString + 1);
      (void)vsnprintf(frame->text, sizeOfFinalString + 1, format, args2);
   }
   else
   {
      frame->text = NULL;
   }
   va_end(args2);
}

// Might be useful but unused so I need to hide it from picky compilers.
/*
static inline
void rmStackFrameInitTextf(struct ryanmock_stack_frame * frame, 
                           const char * fileName,
                           int line,
                           const void * token,
                           const char * format, ...)
{
   va_list args;
   va_start(args, format);
   rmStackFrameInitVTextf(frame, fileName, line, token, format, args);
   va_end(args);
}
*/

static inline
void rmStackFrameClone(struct ryanmock_stack_frame * dest,
                       const struct ryanmock_stack_frame * src)
{
   dest->text     = strdup(src->text);
   dest->filename = src->filename;
   dest->line     = src->line;
   dest->token    = src->token;
}

static inline
void rmStackFrameCleanup(struct ryanmock_stack_frame * frame)
{
   if(frame->text != NULL)
   {
      free(frame->text);
      frame->text = NULL;
   }
   frame->line     = 0;
   frame->token    = NULL;
   frame->filename = NULL;
}


static struct ryanmock_stack rmStack;

static inline
void rmStackTeardown(struct ryanmock_stack * stack);

static inline
void rmStackWriteTraceToStream(const struct ryanmock_stack * stack,
                               struct ryanmock_stream * stream,
                               const char * startLineWith);

static inline
void rmValueInit(struct ryanmock_value * value,
                 const void * data,
                 const char * valueText,
                 size_t size);

// Stream
struct ryanmock_stream
{
   char * buffer;
   size_t size;
   size_t count;
   size_t markCount;
};

enum ryanmock_endianness
{
   eRME_Unknown,
   eRME_Little,
   eRME_Big
};


static jmp_buf                 JumpBuffer;
static bool                    BreakOnBadResult          = false;
static const char *            MockBeginFunctionName     = NULL;
static struct ryanmock_check * RootCheck                 = NULL;
static int                     MockFunctionExpectedCount = 0;
static struct ryanmock_stream  rmStreamMsg;
static struct ryanmock_stream  rmStreamInfo;

static inline
void rmWriteValueStackTraceWithNameToStream(struct ryanmock_stream * stream,
                                            const char * name,
                                            const struct ryanmock_stack * stack);


static inline
void rmCheckStackWriteInfo(struct ryanmock_stream * rmStreamInfo);

static inline
void rmCheckStackRelease(void);

static inline
void rmWriteCheckAndTestStack(struct ryanmock_stream * rmStreamInfo)
{
   rmCheckStackWriteInfo(rmStreamInfo);
   rmWriteValueStackTraceWithNameToStream(rmStreamInfo, "Test", &rmStack);
}

static inline
void rmTestBadResult(void)
{
   rmWriteCheckAndTestStack(&rmStreamInfo);
   if(BreakOnBadResult)
   {
      TriggerBreakpoint();
   }
}

static inline
void rmTestError(void)
{
   rmTestBadResult();
   longjmp(JumpBuffer, JUMP_VALUE_ERROR);
}

static inline
void rmTestFailure(void)
{
   rmTestBadResult();
   longjmp(JumpBuffer, JUMP_VALUE_FAILURE);
}

static inline
void rmTestSkipped(void)
{
   rmWriteValueStackTraceWithNameToStream(&rmStreamInfo, "Test", &rmStack);
   longjmp(JumpBuffer, JUMP_VALUE_SKIPPED);
}


static inline
void rmStreamInit(struct ryanmock_stream * stream)
{
   stream->buffer    = NULL;
   stream->size      = 0;
   stream->count     = 0;
   stream->markCount = 0;
}

static inline
void rmStreamTeardown(struct ryanmock_stream * stream)
{
   if(stream->buffer != NULL)
   {
      free(stream->buffer);
      stream->buffer = NULL;
   }
   stream->size      = 0;
   stream->count     = 0;
   stream->markCount = 0;
}

static inline
void rmStreamClear(struct ryanmock_stream * stream)
{
   stream->count     = 0;
   stream->markCount = 0;
   if(stream->buffer != NULL)
   {
      stream->buffer[0] = '\0';
   }
}

static inline
void rmStreamEnsureNewLine(struct ryanmock_stream * stream)
{
   if(stream->count > 0)
   {
      if(stream->buffer[stream->count - 1] != '\n')
      {
         rmStreamWrite(stream, "\n");
      }
   }
}

static inline
size_t rmStreamClearMarkCount(struct ryanmock_stream * stream)
{
   const size_t markCount = stream->markCount;
   stream->markCount = 0;
   return markCount;
}

static inline
char * rmStreamGetCopy(struct ryanmock_stream * stream)
{
   if(stream->buffer == NULL)
   {
      return strdup("");
   }
   return strdup(stream->buffer);
}


static inline
void rmStreamMakeRoom(struct ryanmock_stream * stream, int moreChars)
{
   if(stream->count + moreChars >= stream->size)
   {
      size_t newSize = stream->count + moreChars + 32;
      stream->buffer = realloc(stream->buffer, newSize);
      stream->size = newSize;
   }
}

void rmStreamWrite(struct ryanmock_stream * stream, const char * format, ...)
{
   va_list args;
   va_start(args, format);
   rmStreamVWrite(stream, format, args);
   va_end(args);
}

void rmStreamVWrite(struct ryanmock_stream * stream, const char * format, va_list args)
{
   va_list args2;
   int sizeOfFinalString;
   va_copy(args2, args);
   sizeOfFinalString = vsnprintf(NULL, 0, format, args);
   if(sizeOfFinalString >= 0)
   {
      rmStreamMakeRoom(stream, sizeOfFinalString + 1);
      (void)vsnprintf(&stream->buffer[stream->count], sizeOfFinalString + 1, format, args2);
      stream->count     += sizeOfFinalString;
      stream->markCount += sizeOfFinalString;
   }
   va_end(args2);
}

void rmStreamWriteChars(struct ryanmock_stream * stream, char character, size_t times)
{
   if(times > 0)
   {
      rmStreamMakeRoom(stream, times + 1);
      memset(&stream->buffer[stream->count], character, times);
      stream->count     += times;
      stream->markCount += times;
      stream->buffer[stream->count] = '\0';
   }
}

static inline
void rmStreamAppend(struct ryanmock_stream * dest,
                    const char * before,
                    const struct ryanmock_stream * src,
                    const char * after)
{
   rmStreamWrite(dest, "%s%s%s", before, src->buffer, after);
}

static inline
void rmStreamMatchLength(struct ryanmock_stream * toChange,
                         struct ryanmock_stream * toMatch,
                         char withCharacter)
{
   const size_t match = rmStreamClearMarkCount(toMatch);
   rmStreamWriteChars(toChange, withCharacter, match);
}

// Inline functions for VTs
void rmTypeRelease(struct ryanmock_type * type)
{
   if(type->refCount > 0)
   {
      type->refCount --;
   }

   if(type->refCount == 0)
   {
      if(type->vt->fpDestroy != NULL)
      {
         type->vt->fpDestroy(type);
      }
      free(type);
   }
}

static inline
void rmSafeCopyDefault(void * dest, size_t dest_size, const void * src, size_t src_size, unsigned char defaultValue)
{
   size_t defaultSize, copySize;

   if(dest_size <= src_size)
   {
      copySize = dest_size;
      defaultSize = 0;
   }
   else
   {
      copySize = src_size;
      defaultSize = dest_size - copySize;
   }
   
   if(defaultSize > 0)
   {
      DECLARE_CAST(unsigned char, byteDest, dest);
      memset(byteDest + copySize, defaultValue, defaultSize);
   }
   memcpy(dest, src, copySize);
}

static inline
void rmSafeCopy(void * dest, size_t destSize, const void * src, size_t srcSize)
{
   size_t copySize;

   if(destSize <= srcSize)
   {
      copySize = destSize;
   }
   else
   {
      copySize = srcSize;
   }
   
   memcpy(dest, src, copySize);
}

static inline
void rmTypeSafeCopy(const struct ryanmock_type * type, void * dest, size_t destSize, const void * src, unsigned char defaultValue)
{
   rmSafeCopyDefault(dest, destSize, src, rmTypeSize(type), defaultValue);
}


static inline
bool rmSizeOperatorCheck(enum ryanmock_comparison_operator2 op, 
                         size_t a, size_t b)
{
   bool pass;
   switch(op)
   {
   case eRMCO2_NotApplicable:
      pass = true;
      break;
   case eRMCO2_Equal:
      pass = (a == b);
      break;
   case eRMCO2_NotEqual:
      pass = (a != b);
      break;
   case eRMCO2_GreaterThan:
      pass = (a >  b);
      break;
   case eRMCO2_GreaterThanOrEqualTo:
      pass = (a >= b);
      break;
   case eRMCO2_LessThan:
      pass = (a <  b);
      break;
   case eRMCO2_LessThanOrEqualTo:
      pass = (a <= b);
      break;
   default:
      pass = false;
      break;
   }
   return pass;
}

static inline
const char * rmGetOperator2String(enum ryanmock_comparison_operator2 op);

static inline
void rmWriteValueStoredStackTypeToStream(struct ryanmock_stream * stream,
                                         const char * name,
                                         const struct ryanmock_value_stored * value,
                                         const struct ryanmock_type * type);


void rmTypeMockSizeCheck(enum ryanmock_comparison_operator2 op, 
                         const struct ryanmock_type * type,
                         size_t destSize, 
                         const struct ryanmock_value_stored * src)
{
   if(!rmSizeOperatorCheck(op, destSize, src->parent.size))
   {
      const char * opText = rmGetOperator2String(op);
      rmStreamWrite(&rmStreamMsg, 
                    "Destination Size (%d) %s Source Size (%d). Did you use the right type?",
                    (int)destSize, opText, (int)src->parent.size);
      rmWriteValueStoredStackTypeToStream(&rmStreamInfo, "Source", src, type);
      rmTestError();
      // Execution never reaches this point
   }
}

void rmTypeMockSafe(enum ryanmock_comparison_operator2 op, 
                    const struct ryanmock_type * type,
                    void * dest, size_t destSize, 
                    const struct ryanmock_value_stored * src)
{
   if(!rmSizeOperatorCheck(op, destSize, src->parent.size))
   {
      const char * opText = rmGetOperator2String(op);
      rmStreamWrite(&rmStreamMsg, 
                    "Destination Size (%d) %s Source Size (%d). Did you use the right type?",
                    (int)destSize, opText, (int)src->parent.size);
      rmWriteValueStoredStackTypeToStream(&rmStreamInfo, "Source", src, type);
      rmTestError();
      // Execution never reaches this point
   }

   rmTypeDataCopy(type, dest, destSize, src->data);
}

void rmTypeDataCopy(const struct ryanmock_type * type, void * dest, size_t destSize, const void * src)
{
   if(type->vt->fpDataCopy == NULL)
   {
      rmTypeSafeCopy(type, dest, destSize, src, 0);
   }
   else
   {
      type->vt->fpDataCopy(type, dest, destSize, src);
   }
}

void rmValueStoredRelease(struct ryanmock_value_stored * value, const struct ryanmock_type * type)
{
   if(value->refCount > 0)
   {
      value->refCount --;
   }
   if(value->refCount == 0)
   {
      if(type->vt->fpDataFree != NULL)
      {
         type->vt->fpDataFree(type, value->data);
      }
      rmStackTeardown(&value->stack);
      free(value);
   }
}

void rmCheckRelease(struct ryanmock_check * check)
{
   if(check->refCount > 0)
   {
      check->refCount --;
   }
   if(check->refCount == 0)
   {
      if(check->vt->fpDestroy != NULL)
      {
         check->vt->fpDestroy(check);
      }
      rmTypeRelease(check->type);
      free(check);
   }
}

static inline
enum ryanmock_result rmCheckCheck(struct ryanmock_check * check,
                                  const struct ryanmock_value * value,
                                  struct ryanmock_stream * rmStreamMsg,
                                  struct ryanmock_stream * rmStreamInfo)
{
   return check->vt->fpCheck(check, value, rmStreamMsg, rmStreamInfo);
}

static inline
void rmCheckWriteInfo(struct ryanmock_check * check,
                      struct ryanmock_stream * rmStreamInfo)
{
   if(check->vt->fpWriteInfo != NULL)
   {
      rmStreamEnsureNewLine(rmStreamInfo);
      check->vt->fpWriteInfo(check, rmStreamInfo);
   }
}

// Check Data
struct ryanmock_check_data_expected2
{
   enum ryanmock_comparison_operator2 op;
   struct ryanmock_value_stored * expected;
};

struct ryanmock_check_data_expected3
{
   enum ryanmock_comparison_operator3 op;
   struct ryanmock_value_stored * expected1;
   struct ryanmock_value_stored * expected2;
};

struct ryanmock_check_data_always_pass
{
   struct ryanmock_stack stack;
};

struct ryanmock_check_data_callback
{
   rmcheck_function callback;
   const char * callbackName;
   void * userData;
   struct ryanmock_stack userDataStack;
};


static inline
const void * rmGetIndexedElement(const void * base, size_t stride, size_t index)
{
   return ((const unsigned char *)base) + (stride * index);
}


void rmCheck2WriteArrayFirstDiffrence(struct ryanmock_stream * stream,
                                      const void * actual,
                                      const void * expected,
                                      const struct ryanmock_type * elementType,
                                      size_t elementStride,
                                      size_t expectedCount,
                                      size_t actualCount,
                                      size_t indexOfFirstDiffrence,
                                      const struct ryanmock_list_appearance * appearance,
                                      const size_t indexToLookBack,
                                      const size_t indexToLookForward)
{
   struct ryanmock_stream expectedStream, actualStream, indicatorStream;
   size_t startIndex;
   size_t expectedEnd, actualEnd, end;
   size_t index;

   // Figure out where to start and end the sample
   if(indexOfFirstDiffrence >= indexToLookBack)
   {
      startIndex = indexOfFirstDiffrence - indexToLookBack;
   }
   else
   {
      startIndex = 0;
   }

   if(indexOfFirstDiffrence + indexToLookForward < expectedCount)
   {
      expectedEnd = indexOfFirstDiffrence + indexToLookForward + 1;
   }
   else
   {
      expectedEnd = expectedCount;
   }

   if(indexOfFirstDiffrence + indexToLookForward < actualCount)
   {
      actualEnd = indexOfFirstDiffrence + indexToLookForward + 1;
   }
   else
   {
      actualEnd = actualCount;
   }

   if(actualEnd > expectedEnd)
   {
      end = actualEnd;
   }
   else
   {
      end = expectedEnd;
   }

   rmStreamInit(&expectedStream);
   rmStreamInit(&actualStream);
   rmStreamInit(&indicatorStream);

   rmStreamWrite(&expectedStream, "Expected(");
   rmStreamWrite(&expectedStream, appearance->start);

   rmStreamWrite(&actualStream,   "  Actual(");
   rmStreamWrite(&actualStream,   appearance->start);

   rmStreamMatchLength(&indicatorStream, &expectedStream, '.');


   // Write Dots if not the start
   if(startIndex != 0)
   {
      rmStreamWrite(&expectedStream, "%s", appearance->hidden);
      rmStreamWrite(&expectedStream, "%s", appearance->seperator);

      rmStreamWrite(&actualStream,   "%s", appearance->hidden);
      rmStreamWrite(&actualStream,   "%s", appearance->seperator);

      rmStreamMatchLength(&indicatorStream, &expectedStream, '.');
   }


   // Clear the marks
   (void)rmStreamClearMarkCount(&expectedStream);
   (void)rmStreamClearMarkCount(&actualStream);

   for(index = startIndex; index < end; index ++)
   {
      const void * expectedData = rmGetIndexedElement(expected, elementStride, index);
      const void * actualData   = rmGetIndexedElement(actual,   elementStride, index);

      // if not the first pass
      if(index != startIndex)
      {
         size_t expectedMark = 0;
         size_t actualMark   = 0;
         size_t totalAdded   = 0;

         // Write Commas
         if(index < expectedEnd)
         {
            rmStreamWrite(&expectedStream, "%s", appearance->seperator);
         }
         if(index < actualEnd)
         {
            rmStreamWrite(&actualStream,   "%s", appearance->seperator);
         }

         // Add space padding
         expectedMark = rmStreamClearMarkCount(&expectedStream);
         actualMark   = rmStreamClearMarkCount(&actualStream);
         
         if(actualMark < expectedMark)
         {
            totalAdded = expectedMark;
         }
         else
         {
            totalAdded = actualMark;
         }

         if(index < actualEnd && actualMark < expectedMark)
         {
            rmStreamWriteChars(&actualStream, ' ', expectedMark - actualMark);
            (void)rmStreamClearMarkCount(&actualStream);

         }
         else if(index < expectedEnd && expectedMark < actualMark)
         {
            rmStreamWriteChars(&expectedStream, ' ', actualMark - expectedMark);
            (void)rmStreamClearMarkCount(&expectedStream);
         }

         if(index <= indexOfFirstDiffrence)
         {
            rmStreamWriteChars(&indicatorStream, '.', totalAdded);
         }
      }

      // Write the value
      if(index < expectedEnd)
      {
         rmTypeWriteValue(elementType, expectedData, rmTypeSize(elementType), &expectedStream);
      }
      if(index < actualEnd)
      {
         rmTypeWriteValue(elementType, actualData,   rmTypeSize(elementType), &actualStream);
      }
   }

   // Write Dots if not the end
   if(expectedEnd < expectedCount)
   {
      rmStreamWrite(&expectedStream, "%s", appearance->hidden);
   }
   
   if(actualEnd < actualCount)
   {
      rmStreamWrite(&actualStream,   "%s", appearance->hidden);
   }



   // Top Everything off
   rmStreamWrite(&expectedStream, "%s)", appearance->end);
   rmStreamWrite(&actualStream,   "%s)", appearance->end);
   rmStreamWrite(&indicatorStream, "^");

   // Put them all together
   rmStreamAppend(stream, "", &expectedStream,  "\n");
   rmStreamAppend(stream, "", &actualStream,    "\n");
   rmStreamAppend(stream, "", &indicatorStream, "");

   // Free streams
   rmStreamTeardown(&expectedStream);
   rmStreamTeardown(&actualStream);
   rmStreamTeardown(&indicatorStream);
}

static inline
void rmWriteValueWithNameToStream(struct ryanmock_stream * stream,
                                  const char * name,
                                  const struct ryanmock_value * value,
                                  const struct ryanmock_type * type,
                                  const char * prefix,
                                  const char * postfix)
{
   rmStreamWrite(stream, "%s%s(", prefix, name);
   rmValueWriteSummaryWithText(value, type, stream);
   rmStreamWrite(stream, ")%s", postfix);
}
static inline
void rmWriteNameToStream(struct ryanmock_stream * stream,
                         const char * name,
                         const struct ryanmock_value * value,
                         const char * prefix,
                         const char * postfix)
{
   rmStreamWrite(stream, "%s%s(", prefix, name);
   rmValueWriteValueText(value, "{", "}", stream);
   rmStreamWrite(stream, ")%s", postfix);
}

// Type Data

// Types

static inline
struct ryanmock_type * rmCreateType(const struct ryanmock_type_vt * vt,
                                    size_t size,
                                    size_t dataSize)
{
   struct ryanmock_type * type = malloc(sizeof(struct ryanmock_type) + dataSize);
   type->vt           = vt;
   type->refCount     = 1;
   type->size         = size;
   return type;
}

static inline
struct ryanmock_type * rmCreateTypeAndCopy(const struct ryanmock_type_vt * vt,
                                           size_t size,
                                           size_t dataSize,
                                           const void * data)
{
   struct ryanmock_type * type = rmCreateType(vt, size, dataSize);
   memcpy(type->data, data, dataSize);
   return type;
}

static inline
const char * rmGetOperator2String(enum ryanmock_comparison_operator2 op)
{
   const char * value;
   switch(op)
   {
   case eRMCO2_Equal:
      value = "does not equal";
      break;
   case eRMCO2_NotEqual:
      value = "equals";
      break;
   case eRMCO2_GreaterThan:
      value = "is less than or equal to";
      break;
   case eRMCO2_GreaterThanOrEqualTo:
      value = "is less than";
      break;
   case eRMCO2_LessThan:
      value = "is greater than or equal to";
      break;
   case eRMCO2_LessThanOrEqualTo:
      value = "is greater than";
      break;
   default:
      value = "?Unknown Operation?";
      break;
   }
   return value;
}

// Default Type functions
void rmWriteFailureText2Default(const struct ryanmock_type * type,
                                enum ryanmock_comparison_operator2 op,
                                const struct ryanmock_value * actual,
                                const struct ryanmock_value * expected,
                                struct ryanmock_stream * rmStreamMsg)
{
   switch(op)
   {
   case eRMCO2_Equal:
   case eRMCO2_NotEqual:
   case eRMCO2_GreaterThan:
   case eRMCO2_GreaterThanOrEqualTo:
   case eRMCO2_LessThan:
   case eRMCO2_LessThanOrEqualTo:
      rmWriteValueWithNameToStream(rmStreamMsg, "Actual",   actual,  type, "", "");
      rmStreamWrite(rmStreamMsg, " %s ", rmGetOperator2String(op));
      rmWriteValueWithNameToStream(rmStreamMsg, "Expected", expected, type, "", "!");
      break;
   default:
      rmStreamWrite(rmStreamMsg, "Unhanded Operation Type: %d", op);
      break;
   }
}

void rmWriteFailureText3Default(const struct ryanmock_type * type,
                                enum ryanmock_comparison_operator3 op,
                                const struct ryanmock_value * actual,
                                const struct ryanmock_value * expected1,
                                const struct ryanmock_value * expected2,
                                struct ryanmock_stream * rmStreamMsg)
{
   switch(op)
   {
   case eRMCO3_Between:
      rmWriteValueWithNameToStream(rmStreamMsg, "Actual", actual,    type, "", " is not between ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Min",    expected1, type, "", " and ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Max",    expected2, type, "", "!");

      break;
   case eRMCO3_NotBetween:
      rmWriteValueWithNameToStream(rmStreamMsg, "Actual", actual,    type, "", " is between ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Min",    expected1, type, "", " and ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Max",    expected2, type, "", "!");
      break;
   case eRMCO3_Within:
      rmWriteValueWithNameToStream(rmStreamMsg, "Actual",   actual,    type, "", " is not within ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Epsilon",  expected2, type, "", " of ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Expected", expected1, type, "", "!");
      break;
   case eRMCO3_NotWithin:
      rmWriteValueWithNameToStream(rmStreamMsg, "Actual",   actual,    type, "", " is within ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Epsilon",  expected2, type, "", " of ");
      rmWriteValueWithNameToStream(rmStreamMsg, "Expected", expected1, type, "", "!");
      break;
   default:
      rmStreamWrite(rmStreamMsg, "Unhanded Operation Type: %d", op);
      break;
   }
}


// === Integer Type === //
struct ryanmock_type_data_integer
{
   bool isSigned;
   enum ryanmock_endianness endianness;
   const char * typeName;
   bool isPickyAboutSize;
};

static inline
enum ryanmock_endianness rmGetEndianness(void)
{
   static enum ryanmock_endianness Endianness = eRME_Unknown;
   if(Endianness == eRME_Unknown)
   {
      uint16_t x = 1;
      uint8_t * ptr = (uint8_t*)&x;
      Endianness = ((*ptr) == 1) ? eRME_Little : eRME_Big;
   }
   return Endianness;
}

static inline
bool rmIsIntegerNegitive(const void * intPtr, size_t size,
                         enum ryanmock_endianness endianness)
{
   // Finding the most significant bit of the most significant byte
   // will tell us the sign.
   const uint8_t msbMask = 0x80;
   size_t mostSigByteIndex;
   uint8_t mostSigByte;
   if(endianness == eRME_Big)
   {
      mostSigByteIndex = 0;
   }
   else
   {
      mostSigByteIndex = size - 1;
   }
   mostSigByte = ((const uint8_t *)intPtr)[mostSigByteIndex];

   return (mostSigByte & msbMask) == msbMask;

}

static inline
void rmIntegerCopyInto(void * destIntPtr,      size_t destSize,
                       const void * srcIntPtr, size_t srcSize,
                       enum ryanmock_endianness endianness)
{
   if(destSize >= srcSize)
   {
      uint8_t * destBytePtr = (uint8_t*)destIntPtr;
      size_t startIndex;
      if(endianness == eRME_Little)
      {
         startIndex = 0;
      }
      else
      {
         startIndex = destSize - srcSize;
      }
      memcpy(destBytePtr + startIndex, srcIntPtr, srcSize);
   }
   else
   {
      // Data Loss Here, but there is nothing we can do about it.
      memcpy(destIntPtr, srcIntPtr, destSize);
   }
}

static inline
rmuintmax_t rmGenericIntegerExpand(const void * intPtr, size_t size,
                                   bool isSigned,
                                   enum ryanmock_endianness endianness)
{
   rmuintmax_t bigValue;
   if(isSigned)
   {
      // Sign extend
      if(rmIsIntegerNegitive(intPtr, size, endianness))
      {
         bigValue = ~((rmuintmax_t)0);
      }
      else
      {
         bigValue = 0;
      }
   }
   else
   {
      bigValue = 0;
   }
   rmIntegerCopyInto(&bigValue, sizeof(rmuintmax_t), intPtr, size,
                     endianness);

   return bigValue;
}

static
void rmType_Integer_WriteName(const struct ryanmock_type * type,
                              struct ryanmock_stream * stream)
{
   DECLARE_CAST(struct ryanmock_type_data_integer, typeData, type->data);
   if(typeData->typeName != NULL)
   {
      rmStreamWrite(stream, "%s", typeData->typeName);
   }
   else
   {
      // Figure out a name
      unsigned int bitSize = type->size * 8;
      if(typeData->isSigned)
      {
         rmStreamWrite(stream, "Signed ");
      }
      else
      {
         rmStreamWrite(stream, "Unsigned ");
      }

      rmStreamWrite(stream, "Int%u", bitSize);
   }
}

static
void rmType_Integer_WriteElement(const struct ryanmock_type * type,
                                 const void * value, size_t size,
                                 struct ryanmock_stream * stream)
{
   DECLARE_CAST(struct ryanmock_type_data_integer, typeData, type->data);
   rmuintmax_t bigUnsignedValue = rmGenericIntegerExpand(value,
                                                         size,
                                                         typeData->isSigned,
                                                         typeData->endianness);
   if(typeData->isSigned)
   {
      rmStreamWrite(stream, "%lld", (rmintmax_t)bigUnsignedValue);
   }
   else
   {
      rmStreamWrite(stream, "%llu", bigUnsignedValue);
   }
}

static inline
rmintmax_t rmType_Integer_Diff(const struct ryanmock_type_data_integer * typeData,
                               const struct ryanmock_value * a,
                               const struct ryanmock_value * b)
{
   rmuintmax_t aUnsigned, bUnsigned;
   rmintmax_t diff;

   aUnsigned = rmGenericIntegerExpand(a->data, a->size, typeData->isSigned, typeData->endianness);
   bUnsigned = rmGenericIntegerExpand(b->data, b->size, typeData->isSigned, typeData->endianness);

   if(typeData->isSigned)
   {
      diff = ((rmintmax_t)aUnsigned) - ((rmintmax_t)bUnsigned);
   }
   else
   {
      diff = aUnsigned - bUnsigned;
   }
   return diff;
}

static
int rmType_Integer_Compare(const struct ryanmock_type * type,
                           const struct ryanmock_value * a,
                           const struct ryanmock_value * b)
{
   DECLARE_CAST(struct ryanmock_type_data_integer, typeData, type->data);
   rmintmax_t diff = rmType_Integer_Diff(typeData, a, b);

   if(diff < INT_MIN)
   {
      diff = INT_MIN;
   }
   else if(diff > INT_MAX)
   {
      diff = INT_MAX;
   }

   return (int)diff;
}

static
bool rmType_Integer_IsWithin(const struct ryanmock_type * type,
                             const struct ryanmock_value * a,
                             const struct ryanmock_value * b,
                             const struct ryanmock_value * epsilon)
{
   DECLARE_CAST(struct ryanmock_type_data_integer, typeData, type->data);
   rmintmax_t diff = rmType_Integer_Diff(typeData, a, b);
   rmuintmax_t eps = rmGenericIntegerExpand(epsilon->data, type->size,
                                            typeData->isSigned,
                                            typeData->endianness);

   if(diff < 0)
   {
      diff = -diff;
   }

   return ((rmuintmax_t)diff) <= eps;
}


static const struct ryanmock_type_vt rmTypeVT_Integer =
{
   rmType_Integer_WriteName,
   NULL,
   NULL,
   rmType_Integer_Compare,
   rmType_Integer_IsWithin,
   rmType_Integer_WriteElement,
   NULL,
   NULL,
   NULL,
   false
};

struct ryanmock_type * rmCreateTypeInteger(size_t size, bool isSigned, 
                                           const char * typeName,
                                           bool isPickyAboutSize)
{
   struct ryanmock_type_data_integer intTypeData;
   struct ryanmock_type * type;

   if(size > sizeof(rmuintmax_t))
   {
      rmStreamWrite(&rmStreamMsg, "Unsupported Integer size: %ul!", size);
      rmTestError();

   }

   intTypeData.isSigned         = isSigned;
   intTypeData.typeName         = typeName;
   intTypeData.endianness       = rmGetEndianness();
   intTypeData.isPickyAboutSize = isPickyAboutSize;

   type = rmCreateTypeAndCopy(&rmTypeVT_Integer, size,
                              sizeof(struct ryanmock_type_data_integer),
                              &intTypeData);
   return type;
}

// === Floating Point Type === //
struct ryanmock_type_data_float
{
   const char * format;
   const char * typeName;
};


static
void rmType_Float_WriteName(const struct ryanmock_type * type,
                            struct ryanmock_stream * stream)
{
   DECLARE_CAST(struct ryanmock_type_data_float, typeData, type->data);
   rmStreamWrite(stream, "%s", typeData->typeName);
}

static
void rmType_Float_WriteValue(const struct ryanmock_type * type,
                             const void * value, size_t size,
                             struct ryanmock_stream * stream)
{
   DECLARE_CAST(struct ryanmock_type_data_float, typeData, type->data);
   DECLARE_CAST(float,       fp,  value);
   DECLARE_CAST(double,      dp,  value);
   DECLARE_CAST(long double, ldp, value);
   (void)size;
   
   switch(type->size)
   {
   case sizeof(float):
      rmStreamWrite(stream, typeData->format, *fp);
      break;
   case sizeof(double):
      rmStreamWrite(stream, typeData->format, *dp);
      break;
   case sizeof(long double):
      rmStreamWrite(stream, typeData->format, *ldp);
      break;
   default:
      rmStreamWrite(stream, "?%ld?", type->size);
      break;
   }
}

#define GenerateFloatCompare(FunctionName, TypeName)                         \
static inline                                                                \
int rmType_Float_ ## FunctionName ## _Compare(const void * a, const void *b) \
{                                                                            \
   DECLARE_CAST(TypeName, fa, a);                                            \
   DECLARE_CAST(TypeName, fb, b);                                            \
   if(*fa > *fb) return  1;                                                  \
   if(*fa < *fb) return -1;                                                  \
   return 0;                                                                 \
}

GenerateFloatCompare(Float,      float)
GenerateFloatCompare(Double,     double)
GenerateFloatCompare(LongDouble, long double)

#undef GenerateFloatCompare

static
int rmType_Float_Compare(const struct ryanmock_type * type,
                         const struct ryanmock_value * a,
                         const struct ryanmock_value * b)
{
   int result = 0;
   switch(type->size)
   {
   case sizeof(float):
      result = rmType_Float_Float_Compare(a->data, b->data);
      break;
   case sizeof(double):
      result = rmType_Float_Double_Compare(a->data, b->data);
      break;
   case sizeof(long double):
      result = rmType_Float_LongDouble_Compare(a->data, b->data);
      break;
   default:
      // This is probably going to be wrong
      result = memcmp(a->data, b->data, rmTypeSize(type));
      break;
   }
   return result;
}

#define GenerateFloatWithin(FunctionName, TypeName)                                    \
static inline                                                                          \
bool rmType_Float_ ## FunctionName ## _IsWithin(const struct ryanmock_value * a,       \
                                                const struct ryanmock_value * b,       \
                                                const struct ryanmock_value * epsilon) \
{                                                                                      \
   DECLARE_CAST(TypeName, fa,   a->data);                                              \
   DECLARE_CAST(TypeName, fb,   b->data);                                              \
   DECLARE_CAST(TypeName, feps, epsilon->data);                                        \
   TypeName diff = 0;                                                                  \
   if(*fa > *fb)                                                                       \
   {                                                                                   \
      diff = *fa - *fb;                                                                \
   }                                                                                   \
   else                                                                                \
   {                                                                                   \
      diff = *fb - *fa;                                                                \
   }                                                                                   \
   return diff < *feps;                                                                \
}

GenerateFloatWithin(Float,      float)
GenerateFloatWithin(Double,     double)
GenerateFloatWithin(LongDouble, long double)

#undef GenerateFloatWithin
static
bool rmType_Float_IsWithin(const struct ryanmock_type * type,
                           const struct ryanmock_value * a,
                           const struct ryanmock_value * b,
                           const struct ryanmock_value * epsilon)
{
   bool result = 0;
   switch(type->size)
   {
   case sizeof(float):
      result = rmType_Float_Float_IsWithin(a, b, epsilon);
      break;
   case sizeof(double):
      result = rmType_Float_Double_IsWithin(a, b, epsilon);
      break;
   case sizeof(long double):
      result = rmType_Float_LongDouble_IsWithin(a, b, epsilon);
      break;
   default:
      // This is probably going to be wrong
      result = false;
      break;
   }
   return result;
}

static const struct ryanmock_type_vt rmTypeVT_Float =
{
   rmType_Float_WriteName,
   NULL,
   NULL,
   rmType_Float_Compare,
   rmType_Float_IsWithin,
   rmType_Float_WriteValue,
   NULL,
   NULL,
   NULL,
   false
};

struct ryanmock_type * rmCreateTypeFloatingPoint(const char * typeName,
                                                 size_t size,
                                                 const char * format)
{
   struct ryanmock_type_data_float typeData;
   struct ryanmock_type * type;

   typeData.typeName = typeName;
   typeData.format   = format;


   type = rmCreateTypeAndCopy(&rmTypeVT_Float, size,
                              sizeof(struct ryanmock_type_data_float),
                              &typeData);
   return type;
}

// === Boolean Type === //
static
void rmType_Boolean_WriteName(const struct ryanmock_type * type,
                              struct ryanmock_stream * stream)
{
   (void)type;
   rmStreamWrite(stream, "Boolean");
}

static
void rmType_Boolean_WriteElement(const struct ryanmock_type * type,
                                 const void * value, size_t size,
                                 struct ryanmock_stream * stream)
{
   DECLARE_CAST(bool, boolPtr, value);
   (void)type;
   (void)size;
   if(*boolPtr)
   {
      rmStreamWrite(stream, "true");
   }
   else
   {
      rmStreamWrite(stream, "false");
   }
}


static
int rmType_Boolean_Compare(const struct ryanmock_type * type,
                           const struct ryanmock_value * a,
                           const struct ryanmock_value * b)
{
   DECLARE_CAST(bool, boolPtrA, a->data);
   DECLARE_CAST(bool, boolPtrB, b->data);
   (void)type;
   if(*boolPtrA == *boolPtrB)
   {
      return 0;
   }
   if((*boolPtrA) && !(*boolPtrB))
   {
      return 1;
   }

   return -1;
}

static const struct ryanmock_type_vt rmTypeVT_Boolean =
{
   rmType_Boolean_WriteName,
   NULL,
   NULL,
   rmType_Boolean_Compare,
   NULL,
   rmType_Boolean_WriteElement,
   NULL,
   NULL,
   NULL,
   false
};

static inline
struct ryanmock_type * rmCreateTypeBoolean(void)
{
   return rmCreateType(&rmTypeVT_Boolean, sizeof(bool), 0);
}

// === Pointer Type === //
static
void rmType_Pointer_WriteName(const struct ryanmock_type * type, struct ryanmock_stream * stream)
{
   (void)type;
   rmStreamWrite(stream, "Pointer");
}

static
void rmType_Pointer_WriteElement(const struct ryanmock_type * type,
                                 const void * value, size_t size,
                                 struct ryanmock_stream * stream)
{
   DECLARE_CAST(const void *, ptr, value);
   (void)type;
   (void)size;

   rmStreamWrite(stream, "%p", *ptr);
}

static inline
ptrdiff_t rmType_Pointer_Diff(const void * a,
                              const void * b)
{
   DECLARE_CAST(const unsigned char *, aPtr, a);
   DECLARE_CAST(const unsigned char *, bPtr, b);

   return *aPtr - *bPtr;
}

static
int rmType_Pointer_Compare(const struct ryanmock_type * type,
                           const struct ryanmock_value * a,
                           const struct ryanmock_value * b)
{
   ptrdiff_t diff = rmType_Pointer_Diff(a->data, b->data);
   (void)type;

   if(diff < INT_MIN)
   {
      diff = INT_MIN;
   }
   else if(diff > INT_MAX)
   {
      diff = INT_MAX;
   }

   return (int)diff;
}

static
bool rmType_Pointer_IsWithin(const struct ryanmock_type * type,
                             const struct ryanmock_value * a,
                             const struct ryanmock_value * b,
                             const struct ryanmock_value * epsilon)
{
   DECLARE_CAST(const void *, epsPtr, epsilon->data);
   ptrdiff_t diff = rmType_Pointer_Diff(a->data, b->data);
   (void)type;

   if(diff < 0)
   {
      diff = -diff;
   }
   return ((const void*)diff) < (*epsPtr);
}

static const struct ryanmock_type_vt rmTypeVT_Pointer =
{
   rmType_Pointer_WriteName,
   NULL,
   NULL,
   rmType_Pointer_Compare,
   rmType_Pointer_IsWithin,
   rmType_Pointer_WriteElement,
   NULL,
   NULL,
   NULL,
   false
};

struct ryanmock_type * rmCreateTypePointer(void)
{
   return rmCreateType(&rmTypeVT_Pointer, sizeof(void*), 0);
}

// === Word Type === //
static
void rmType_Word_WriteName(const struct ryanmock_type * type, struct ryanmock_stream * stream)
{
   (void)type;
   rmStreamWrite(stream, "Word");
}

static inline
void rmStreamWriteHexSubChunk(struct ryanmock_stream * stream,
                              unsigned char byte,
                              bool * hideZeros)
{
   if(byte != 0)
   {
      if(*hideZeros)
      {
         rmStreamWrite(stream, "%X", byte);
      }
      else
      {
         rmStreamWrite(stream, "%02X", byte);
      }
      *hideZeros = false;
   }
}

static inline
void rmStreamWriteHexChunk(struct ryanmock_stream * stream,
                           const void * ptr, size_t elementSize)
{
   size_t i;
   bool hideZeros = true;
   DECLARE_CAST(const unsigned char, bytes, ptr);
   if(rmGetEndianness() == eRME_Little)
   {
      for(i = elementSize - 1; i < elementSize; i--)
      {
         rmStreamWriteHexSubChunk(stream, bytes[i], &hideZeros);
      }
   }
   else
   {
      for(i = 0; i < elementSize; i++)
      {
         rmStreamWriteHexSubChunk(stream, bytes[i], &hideZeros);
      }
   }

   // Value is 0 so we need to write at least one zero
   if(hideZeros)
   {
      rmStreamWrite(stream, "0");
   }
}

static
void rmType_Word_WriteElement(const struct ryanmock_type * type,
                              const void * value, size_t size,
                              struct ryanmock_stream * stream)
{
   (void)size;
   rmStreamWrite(stream, "0x");
   rmStreamWriteHexChunk(stream, value, type->size);
}


static
bool rmType_Word_IsWithin(const struct ryanmock_type * type,
                          const struct ryanmock_value * a,
                          const struct ryanmock_value * b,
                          const struct ryanmock_value * epsilon)
{
   (void)type;
   (void)a;
   (void)b;
   (void)epsilon;
   // TODO: Make something up here
   return false;
}

static const struct ryanmock_type_vt rmTypeVT_Word =
{
   rmType_Word_WriteName,
   NULL,
   NULL,
   NULL,
   rmType_Word_IsWithin,
   rmType_Word_WriteElement,
   NULL,
   NULL,
   NULL,
   false
};


struct ryanmock_type * rmCreateTypeWord(size_t size)
{

   return rmCreateType(&rmTypeVT_Word, size, 0);
}


// === Array Type === //
struct ryanmock_type_data_array
{
   size_t count;
   struct ryanmock_type * elementType;
   const char * name;
   struct ryanmock_list_appearance appearance;
   size_t tryToWriteCount;
   size_t (*fpGetSize)(const struct ryanmock_type_data_array * arrayType, 
                       const void * value);
};

static
void rmType_Array_WriteName(const struct ryanmock_type * type, struct ryanmock_stream * stream)
{
   DECLARE_CAST(const struct ryanmock_type_data_array, typeData, type->data);
   rmStreamWrite(stream, "%s", typeData->name);
}


static inline
void rmArrayWriteSlice(const struct ryanmock_type * elementType,
                       size_t elementStride,
                       size_t elementCount,
                       const void * value,
                       struct ryanmock_stream * stream,
                       const struct ryanmock_list_appearance * appearance,
                       size_t tryToWriteCount)
{
   size_t index;

   const size_t writeCount = (elementCount < tryToWriteCount) ?
                              elementCount : tryToWriteCount;


   rmStreamWrite(stream, "%s", appearance->start);

   for(index = 0; index < writeCount; index ++)
   {
      const void * valueAtIndex = rmGetIndexedElement(value, elementStride, index);
      if(index != 0)
      {
         rmStreamWrite(stream, "%s", appearance->seperator);
      }
      rmTypeWriteValue(elementType, valueAtIndex, rmTypeSize(elementType), stream);
   }

   if(writeCount < elementCount)
   {
      rmStreamWrite(stream, "%s%s", appearance->seperator, appearance->hidden);
   }
   rmStreamWrite(stream, "%s", appearance->end);
}

static
void rmType_Array_WriteValue(const struct ryanmock_type * type,
                             const void * value, size_t size,
                             struct ryanmock_stream * stream)
{
   DECLARE_CAST(const struct ryanmock_type_data_array, typeData, type->data);
   size_t count = typeData->count;
   (void)size;
   if(typeData->fpGetSize != NULL)
   {
      count = typeData->fpGetSize(typeData, value);
   }
   
   rmArrayWriteSlice(typeData->elementType, typeData->elementType->size,
                     count, value, stream,
                     &typeData->appearance,
                     typeData->tryToWriteCount);
}



static inline
bool rmType_Array_IsSizeValid(const struct ryanmock_type_data_array * typeData,
                              const struct ryanmock_value * a,
                              const struct ryanmock_value * b)
{
   if(typeData->fpGetSize == NULL)
   {
      return true;
   }
   return typeData->fpGetSize(typeData, a->data) == 
          typeData->fpGetSize(typeData, b->data);
}

static
int rmType_Array_Compare(const struct ryanmock_type * type,
                         const struct ryanmock_value * a,
                         const struct ryanmock_value * b)
{
   DECLARE_CAST(const struct ryanmock_type_data_array, typeData, type->data);
   size_t i;
   if(!rmType_Array_IsSizeValid(typeData, a, b))
   {
      return 1;
   }
   for(i = 0; i < typeData->count; i++)
   {
      struct ryanmock_value av, bv;
      size_t elementSize = typeData->elementType->size;
      const void * ai = rmGetIndexedElement(a->data, elementSize, i);
      const void * bi = rmGetIndexedElement(b->data, elementSize, i);
      rmValueInit(&av, ai, a->valueText, elementSize);
      rmValueInit(&bv, bi, b->valueText, elementSize);
      const int result = rmTypeCompare(typeData->elementType, &av, &bv);
      if(result != 0)
      {
         return result;
      }
   }

   return 0;
}


static
bool rmType_Array_IsWithin(const struct ryanmock_type * type,
                           const struct ryanmock_value * a,
                           const struct ryanmock_value * b,
                           const struct ryanmock_value * epsilon)
{
   DECLARE_CAST(const struct ryanmock_type_data_array, typeData, type->data);
   size_t i;
   if(!rmType_Array_IsSizeValid(typeData, a, b))
   {
      return 1;
   }
   for(i = 0; i < typeData->count; i++)
   {
      struct ryanmock_value av, bv;
      size_t elementSize = typeData->elementType->size;
      const void * ai = rmGetIndexedElement(a->data, elementSize, i);
      const void * bi = rmGetIndexedElement(b->data, elementSize, i);
      rmValueInit(&av, ai, a->valueText, elementSize);
      rmValueInit(&bv, bi, b->valueText, elementSize);
      const bool result = rmTypeIsWithin(typeData->elementType, &av, &bv, epsilon);
      if(!result)
      {
         return false;
      }
   }
   return true;
}

static inline
enum ryanmock_result rmCheck_Check2_Test(enum ryanmock_comparison_operator2 op,
                                         const struct ryanmock_type * type,
                                         const struct ryanmock_value * a, 
                                         const struct ryanmock_value * b);


static inline
size_t rmArrayFindIndexOfFirstFailure2(size_t elementStride,
                                       size_t elementCount,
                                       const struct ryanmock_type * elementType,
                                       const struct ryanmock_value * a, 
                                       const struct ryanmock_value * b,
                                       enum ryanmock_comparison_operator2 op)
{
   size_t i;
   for(i = 0; i < elementCount; i++)
   {
      struct ryanmock_value av, bv;
      const void * ai = rmGetIndexedElement(a->data, elementStride, i);
      const void * bi = rmGetIndexedElement(b->data, elementStride, i);
      rmValueInit(&av, ai, a->valueText, elementStride);
      rmValueInit(&bv, bi, b->valueText, elementStride);
      const enum ryanmock_result result = rmCheck_Check2_Test(op, elementType,
                                                              &av, &bv);
      if(result != eRMR_passed)
      {
         return i;
      }
   }
   return elementCount;
}

static inline
void rmArrayWriteFailureText2(const struct ryanmock_type * elementType,
                              size_t elementStride,
                              size_t expectedCount,
                              size_t actualCount,
                              const struct ryanmock_value * actual,
                              const struct ryanmock_value * expected,
                              size_t indexOfFirstFailure,
                              struct ryanmock_stream * rmStreamMsg,
                              const struct ryanmock_list_appearance * appearance,
                              const size_t indexToLookBack,
                              const size_t indexToLookForward)
{
   const void * a = actual->data;
   const void * b = expected->data;
   

   rmWriteNameToStream(rmStreamMsg, "Actual", actual, "", " has a failure at index ");
   rmStreamWrite(rmStreamMsg, "%lu with ", indexOfFirstFailure);
   rmWriteNameToStream(rmStreamMsg, "Expected", expected, "", "!\n");

   if(expectedCount != actualCount)
   {
      rmStreamWrite(rmStreamMsg, "Actual has a size of %lu, Expected has a size of %lu.\n", 
                                 actualCount, expectedCount);
   }

   rmCheck2WriteArrayFirstDiffrence(rmStreamMsg, a, b, elementType,
                                    elementStride, expectedCount, actualCount,
                                    indexOfFirstFailure, appearance, 
                                    indexToLookBack, indexToLookForward);
}


static
void rmType_Array_WriteFailureText2(const struct ryanmock_type * type,
                                    enum ryanmock_comparison_operator2 op,
                                    const struct ryanmock_value * actual,
                                    const struct ryanmock_value * expected,
                                    struct ryanmock_stream * rmStreamMsg)
{
   DECLARE_CAST(const struct ryanmock_type_data_array, typeData, type->data);
   const size_t lookAround = typeData->tryToWriteCount;
   const void * a = actual;
   const void * b = expected;
   size_t indexOfFirstFailure =
                rmArrayFindIndexOfFirstFailure2(typeData->elementType->size,
                                                typeData->count,
                                                typeData->elementType,
                                                a, b, op);
   size_t actualCount, expectedCount;
   if(typeData->fpGetSize == NULL)
   {
      actualCount   = typeData->count;
      expectedCount = typeData->count;
   }
   else
   {
      actualCount   = typeData->fpGetSize(typeData, actual->data);
      expectedCount = typeData->fpGetSize(typeData, expected->data);
   }

   
   rmArrayWriteFailureText2(typeData->elementType, typeData->elementType->size,
                            expectedCount, actualCount, actual, expected,
                            indexOfFirstFailure, rmStreamMsg,
                            &typeData->appearance,
                            lookAround, lookAround);

}

static
void rmType_Array_Destroy(const struct ryanmock_type * type)
{
   DECLARE_CAST(const struct ryanmock_type_data_array, typeData, type->data);
   rmTypeRelease(typeData->elementType);
}

static
void rmType_Array_DataCopy(const struct ryanmock_type * type, 
                           void * dest, size_t destSize, const void * src)
{
   rmSafeCopy(dest, destSize, src, rmTypeSize(type));
}

static const struct ryanmock_type_vt rmTypeVT_Array =
{
   rmType_Array_WriteName,
   rmType_Array_DataCopy,
   NULL,
   rmType_Array_Compare,
   rmType_Array_IsWithin,
   rmType_Array_WriteValue,
   rmType_Array_WriteFailureText2,
   NULL,
   rmType_Array_Destroy,
   true
};


struct ryanmock_type * rmCreateTypeCustomArray(struct ryanmock_type * elementType,
                                               size_t count,
                                               const char * name,
                                               const char * listSeperator,
                                               const char * listStart,
                                               const char * listEnd,
                                               const char * listHidden,
                                               size_t tryToWriteCount,
                                               size_t (*fpGetSize)(const struct ryanmock_type_data_array * type, const void * value))
{
   struct ryanmock_type_data_array array;
   rmTypeTake(elementType);
   array.elementType          = elementType;
   array.count                = count;
   array.name                 = name;
   array.appearance.seperator = listSeperator;
   array.appearance.start     = listStart;
   array.appearance.end       = listEnd;
   array.appearance.hidden    = listHidden;
   array.tryToWriteCount      = tryToWriteCount;
   array.fpGetSize            = fpGetSize;
   return rmCreateTypeAndCopy(&rmTypeVT_Array, count * elementType->size,
                              sizeof(struct ryanmock_type_data_array),
                              &array);
}

struct ryanmock_type * rmCreateTypeArray(struct ryanmock_type * elementType,
                                         size_t count)
{
   return rmCreateTypeCustomArray(elementType, count, "Array",
                                  ", ", "[", "]", "...", 3, NULL);
}


// === Memory Type === //

struct ryanmock_type * rmCreateTypeMemory(size_t elementSize,
                                          size_t elementCount)
{
   struct ryanmock_type * wordType = rmCreateTypeWord(elementSize);
   struct ryanmock_type * memoryType = rmCreateTypeCustomArray(wordType,
                                                               elementCount,
                                                               "Memory",
                                                               ", ", "[",
                                                               "]", "...", 3, 
                                                               NULL);
   rmTypeRelease(wordType);
   return memoryType;
}



// === String Type === //
static
void rmType_Character_WriteName(const struct ryanmock_type * type, struct ryanmock_stream * stream)
{
   (void)type;
   rmStreamWrite(stream, "Character");
}


static
void rmType_Character_WriteValue(const struct ryanmock_type * type,
                                  const void * value, size_t size,
                                  struct ryanmock_stream * stream)
{
   DECLARE_CAST(const char, c, value);
   (void)type;
   (void)size;
   // TODO: Do a better job handling non printable character codes
   switch(*c)
   {
   case '"':
      rmStreamWrite(stream, "\\\"");
      break;
   case '\n':
      rmStreamWrite(stream, "\\n");
      break;
   case '\t':
      rmStreamWrite(stream, "\\t");
      break;
   case '\r':
      rmStreamWrite(stream, "\\r");
      break;
   case '\\':
      rmStreamWrite(stream, "\\\\");
      break;
   case '\a':
      rmStreamWrite(stream, "\\a");
      break;
   case '\b':
      rmStreamWrite(stream, "\\b");
      break;
   case '\v':
      rmStreamWrite(stream, "\\v");
      break;
   case '\0':
      rmStreamWrite(stream, "\\0");
      break;
   default:
      rmStreamWrite(stream, "%c", *c);
      break;

   }
}

static
int rmType_Character_Compare(const struct ryanmock_type * type,
                             const struct ryanmock_value * a,
                             const struct ryanmock_value * b)
{
   DECLARE_CAST(const char, ca, a->data);
   DECLARE_CAST(const char, cb, b->data);
   (void)type;
   return (*ca) - (*cb);
}


static
bool rmType_Character_IsWithin(const struct ryanmock_type * type,
                               const struct ryanmock_value * a,
                               const struct ryanmock_value * b,
                               const struct ryanmock_value * epsilon)
{
   (void)type;
   (void)a;
   (void)b;
   (void)epsilon;
   // TODO: Make something up here
   return false;
}

static const struct ryanmock_type_vt rmTypeVT_Character =
{
   rmType_Character_WriteName,
   NULL,
   NULL,
   rmType_Character_Compare,
   rmType_Character_IsWithin,
   rmType_Character_WriteValue,
   NULL,
   NULL,
   NULL,
   false
};

struct ryanmock_type * rmCreateTypeCharacter(void)
{
   return rmCreateType(&rmTypeVT_Character, 1, 0);
}

// === String Type === //

static
size_t rmStringGetSize(const struct ryanmock_type_data_array * type, 
                       const void * value)
{
   (void)type;
   return strlen(value);
}


struct ryanmock_type * rmCreateTypeString(size_t stringSize, bool isNullTerminated)
{
   size_t (*fpGetSize)(const struct ryanmock_type_data_array * type, const void * value);
   if(isNullTerminated)
   {
      fpGetSize = rmStringGetSize;
      stringSize ++; // Make room for Null termination character.
   }
   else
   {
      fpGetSize = NULL;
   }
   struct ryanmock_type * charType = rmCreateTypeCharacter();
   struct ryanmock_type * stringType = rmCreateTypeCustomArray(charType,
                                                               stringSize,
                                                               "String",
                                                               "", "\"", "\"",
                                                               "...", 10, 
                                                               fpGetSize);
   rmTypeRelease(charType);
   return stringType;
}

static inline
void rmWriteValueStackTypeToStream(struct ryanmock_stream * stream,
                                   const char * name,
                                   const struct ryanmock_value * value,
                                   const struct ryanmock_type * type,
                                   const struct ryanmock_stack * stack)
{
   rmStreamEnsureNewLine(stream);
   rmStreamWrite(stream, "%s(", name);
   rmValueWriteFullSummary(value, type, stream);
   rmStreamWrite(stream, ") Stack Trace:\n");
   rmStackWriteTraceToStream(stack, stream, "   ");
}

static inline
void rmWriteValueStoredStackTypeToStream(struct ryanmock_stream * stream,
                                         const char * name,
                                         const struct ryanmock_value_stored * value,
                                         const struct ryanmock_type * type)
{
   rmWriteValueStackTypeToStream(stream, name, &value->parent, type,
                                 &value->stack);
}


static inline
void rmWriteValueStackTraceWithNameToStream(struct ryanmock_stream * stream,
                                            const char * name,
                                            const struct ryanmock_stack * stack)
{
   rmStreamEnsureNewLine(stream);
   rmStreamWrite(stream, "%s Stack Trace:\n", name);
   rmStackWriteTraceToStream(stack, stream, "   ");
}

// Check VTs
static
void rmCheck_Any_DestroyExpected(struct ryanmock_check * check)
{
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   rmValueStoredRelease(expectedPtr->expected, check->type);
}


static
void rmCheck_Check3_Destroy(struct ryanmock_check * check)
{
   DECLARE_CAST(struct ryanmock_check_data_expected3, expectedPtr, check->data);
   rmValueStoredRelease(expectedPtr->expected1, check->type);
   rmValueStoredRelease(expectedPtr->expected2, check->type);
}

static
void rmCheck_Any_WriteInfoExpected(struct ryanmock_check * check,
                                   struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);

   rmWriteValueStoredStackTypeToStream(rmStreamInfo, "Expected",
                                       expectedPtr->expected, check->type);
}

static
void rmCheck_Any_WriteInfoExpectedNoValue(struct ryanmock_check * check,
                                          struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   rmWriteValueStackTraceWithNameToStream(rmStreamInfo, "Expected",
                                          &expectedPtr->expected->stack);
}




static inline
enum ryanmock_result rmCheck_Check2_Test(enum ryanmock_comparison_operator2 op,
                                         const struct ryanmock_type * type,
                                         const struct ryanmock_value * a, 
                                         const struct ryanmock_value * b)
{


   switch(op)
   {
   case eRMCO2_Equal:
      if(rmTypeCompare(type, a, b) == 0)
      {
         return eRMR_passed;
      }
      break;
   case eRMCO2_NotEqual:
      if(rmTypeCompare(type, a, b) != 0)
      {
         return eRMR_passed;
      }
      break;
   case eRMCO2_GreaterThan:
      if(rmTypeCompare(type, a, b) > 0)
      {
         return eRMR_passed;
      }
      break;
   case eRMCO2_GreaterThanOrEqualTo:
      if(rmTypeCompare(type, a, b) >= 0)
      {
         return eRMR_passed;
      }
      break;
   case eRMCO2_LessThan:
      if(rmTypeCompare(type, a, b) < 0)
      {
         return eRMR_passed;
      }
      break;
   case eRMCO2_LessThanOrEqualTo:
      if(rmTypeCompare(type, a, b) <= 0)
      {
         return eRMR_passed;
      }
      break;
   default:
      return eRMR_error;
      break;
   }
   return eRMR_failed;
}


static
enum ryanmock_result rmCheck_Check2_Check(struct ryanmock_check * check,
                                          const struct ryanmock_value * value,
                                          struct ryanmock_stream * rmStreamMsg,
                                          struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   const enum ryanmock_comparison_operator2 op = expectedPtr->op;
   const struct ryanmock_type * type = check->type;
   const struct ryanmock_value * expected = &expectedPtr->expected->parent;
   const struct ryanmock_value * a = value;
   const struct ryanmock_value * b = expected;
   const enum ryanmock_result result = rmCheck_Check2_Test(op, type, a, b);
   (void)rmStreamInfo;

   switch(result)
   {
   case eRMR_error:
      // This can only happen if we have an unhanded type
      rmStreamWrite(rmStreamMsg, "Unhandled Operation Type: %d", op);
      break;
   case eRMR_failed:
      rmTypeWriteFailureText2(type, op, value, expected, rmStreamMsg);
      break;
   default:
      // Do Nothing Intentionally
      break;
   }
   return result;
}

static
enum ryanmock_result rmCheck_Boolean_Check(struct ryanmock_check * check,
                                           const struct ryanmock_value * value,
                                           struct ryanmock_stream * rmStreamMsg,
                                           struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   const struct ryanmock_type * type = check->type;
   const struct ryanmock_value * expected = &expectedPtr->expected->parent;
   DECLARE_CAST(bool, a, value->data);
   DECLARE_CAST(bool, b, expected->data);
   (void)rmStreamInfo;

   if(*a == *b)
   {
      return eRMR_passed;
   }
   if(*a)
   {
      rmWriteValueWithNameToStream(rmStreamMsg, "Actual", value, type, "", " is not False!");
   }
   else
   {
      rmWriteValueWithNameToStream(rmStreamMsg, "Actual", value, type, "", " is not True!");
   }
   return eRMR_failed;
}

static
enum ryanmock_result rmCheck_NULL_Check(struct ryanmock_check * check,
                                        const struct ryanmock_value * value,
                                        struct ryanmock_stream * rmStreamMsg,
                                        struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   const struct ryanmock_type * type = check->type;
   const struct ryanmock_value * shouldBeNULL = &expectedPtr->expected->parent;
   DECLARE_CAST(void *, a, value->data);
   DECLARE_CAST(bool, shouldBeNULLDataPtr, shouldBeNULL->data);
   (void)rmStreamInfo;


   if(*shouldBeNULLDataPtr)
   {
      if(*a == NULL)
      {
         return eRMR_passed;
      }
      else
      {
         rmWriteValueWithNameToStream(rmStreamMsg, "Actual", value, type, "", " is not NULL!");
      }
   }
   else
   {
      if(*a != NULL)
      {
         return eRMR_passed;
      }
      else
      {
         rmWriteValueWithNameToStream(rmStreamMsg, "Actual", value, type, "", " is NULL!");
      }
   }
   return eRMR_failed;
}

static
void rmCheck_Check3_WriteInfo(struct ryanmock_check * check,
                              struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_expected3, expectedPtr, check->data);
   const char * name1;
   const char * name2;
   switch(expectedPtr->op)
   {
   case eRMCO3_Between:
      name1 = "Min";
      name2 = "Max";
      break;
   default:
      name1 = "Expected1";
      name2 = "Expected2";
      break;
   }

   rmWriteValueStoredStackTypeToStream(rmStreamInfo, name1,
                                       expectedPtr->expected1, check->type);
   rmWriteValueStoredStackTypeToStream(rmStreamInfo, name2,
                                       expectedPtr->expected2, check->type);
}


static inline
enum ryanmock_result rmCheck_Check3_Test(enum ryanmock_comparison_operator3 op,
                                         const struct ryanmock_type * type,
                                         const struct ryanmock_value * actual,
                                         const struct ryanmock_value * expected1,
                                         const struct ryanmock_value * expected2)
{
   const void * a = actual;
   const void * b = expected1;
   const void * c = expected2;

   switch(op)
   {
   case eRMCO3_Between:
      if(rmTypeCompare(type, a, b) >= 0 &&
         rmTypeCompare(type, a, c) <= 0)
      {
         return eRMR_passed;
      }
      break;
   case eRMCO3_NotBetween:
      if(!(rmTypeCompare(type, a, b) >= 0 &&
           rmTypeCompare(type, a, c) <= 0))
      {
         return eRMR_passed;
      }
      break;
   case eRMCO3_Within:
      if(rmTypeIsWithin(type, a, b, c))
      {
         return eRMR_passed;
      }
      break;
   case eRMCO3_NotWithin:
      if(!rmTypeIsWithin(type, a, b, c))
      {
         return eRMR_passed;
      }
      break;
   default:

      return eRMR_error;
      break;
   }
   return eRMR_failed;
}

static
enum ryanmock_result rmCheck_Check3_Check(struct ryanmock_check * check,
                                          const struct ryanmock_value * value,
                                          struct ryanmock_stream * rmStreamMsg,
                                          struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_expected3, expectedPtr, check->data);
   const enum ryanmock_comparison_operator3 op = expectedPtr->op;
   const struct ryanmock_type * type = check->type;
   const struct ryanmock_value * expected1 = &expectedPtr->expected1->parent;
   const struct ryanmock_value * expected2 = &expectedPtr->expected2->parent;
   const enum ryanmock_result result = rmCheck_Check3_Test(op, type, value,
                                                           expected1,
                                                           expected2);

   (void)rmStreamInfo;

   switch(result)
   {
   case eRMR_error:
      rmStreamWrite(rmStreamMsg, "Unhandled Operation Type: %d", op);
      break;
   case eRMR_failed:
      rmTypeWriteFailureText3(type, op, value, expected1, expected2,
                              rmStreamMsg);
      break;
   default:
      // Do Nothing Intentionally
      break;
   }

   return result;
}



static struct ryanmock_check_vt rmCheckVT_NULL =
{
   rmCheck_NULL_Check,
   rmCheck_Any_WriteInfoExpectedNoValue,
   rmCheck_Any_DestroyExpected
};

static struct ryanmock_check_vt rmCheckVT_Boolean =
{
   rmCheck_Boolean_Check,
   rmCheck_Any_WriteInfoExpectedNoValue,
   rmCheck_Any_DestroyExpected
};

static struct ryanmock_check_vt rmCheckVT3 =
{
   rmCheck_Check3_Check,
   rmCheck_Check3_WriteInfo,
   rmCheck_Check3_Destroy
};

static struct ryanmock_check_vt rmCheckVT2 =
{
   rmCheck_Check2_Check,
   rmCheck_Any_WriteInfoExpected,
   rmCheck_Any_DestroyExpected
};


// Check Always Pass
static
enum ryanmock_result rmCheck_AlwaysPass_Check(struct ryanmock_check * check,
                                              const struct ryanmock_value * value,
                                              struct ryanmock_stream * rmStreamMsg,
                                              struct ryanmock_stream * rmStreamInfo)
{
   (void)check;
   (void)value;
   (void)rmStreamInfo;
   (void)rmStreamMsg;

   return eRMR_passed;
}

static
void rmCheck_AlwaysPass_WriteInfo(struct ryanmock_check * check,
                                   struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_always_pass,
                expectedPtr, check->data);

   rmStackWriteTraceToStream(&expectedPtr->stack, rmStreamInfo, "   ");
}

static
void rmCheck_AlwaysPass_Destroy(struct ryanmock_check * check)
{
   DECLARE_CAST(struct ryanmock_check_data_always_pass,
                expectedPtr, check->data);

   rmStackTeardown(&expectedPtr->stack);
}


static struct ryanmock_check_vt rmCheckVT_AlwaysPass =
{
   rmCheck_AlwaysPass_Check,
   rmCheck_AlwaysPass_WriteInfo,
   rmCheck_AlwaysPass_Destroy
};

// Check Callback
static
enum ryanmock_result rmCheck_Callback_Check(struct ryanmock_check * check,
                                            const struct ryanmock_value * value,
                                            struct ryanmock_stream * rmStreamMsg,
                                            struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_callback,
                expectedPtr, check->data);
   (void)rmStreamInfo;
   (void)rmStreamMsg;
   
   rmEnterFunction(expectedPtr->callbackName, __LINE__, __FILE__);
   expectedPtr->callback(value->data, expectedPtr->userData);
   rmExitFunction(expectedPtr->callbackName, __LINE__, __FILE__);

   return eRMR_passed;
}

static
void rmCheck_Callback_WriteInfo(struct ryanmock_check * check,
                                struct ryanmock_stream * rmStreamInfo)
{
   DECLARE_CAST(struct ryanmock_check_data_callback,
                expectedPtr, check->data);

   rmStreamWrite(rmStreamInfo, "User Data Stack Trace:\n");
   rmStackWriteTraceToStream(&expectedPtr->userDataStack, rmStreamInfo, "   ");
}

static
void rmCheck_Callback_Destroy(struct ryanmock_check * check)
{
   DECLARE_CAST(struct ryanmock_check_data_callback,
                expectedPtr, check->data);

   rmStackTeardown(&expectedPtr->userDataStack);
}


static struct ryanmock_check_vt rmCheckVT_Callback =
{
   rmCheck_Callback_Check,
   rmCheck_Callback_WriteInfo,
   rmCheck_Callback_Destroy
};



// Mock Data

enum ryanmock_mock_entry_type
{
   eRMMET_return,
   eRMMET_parameterOutput,
   eRMMET_parameterCheck,
   eRMMET_functionCall
};

struct ryanmock_mock_mock_info
{
   enum ryanmock_mock_entry_type type;
   struct ryanmock_value_stored * value;
   struct ryanmock_type * valueType;
   enum ryanmock_comparison_operator2 destSizeOpCheck;
};

struct ryanmock_mock_entry_return
{
   struct ryanmock_mock_mock_info info;
};

struct ryanmock_mock_entry_parameter_output
{
   struct ryanmock_mock_mock_info info;
   const char * parameterName;
};

struct ryanmock_mock_entry_parameter_check
{
   enum ryanmock_mock_entry_type type;
   const char * parameterName;
   struct ryanmock_check * check;
   enum ryanmock_comparison_operator2 destSizeOpCheck;
};

struct ryanmock_mock_entry_function
{
   enum ryanmock_mock_entry_type type;
   struct ryanmock_stack stack;
   bool ignore;
};

union ryanmock_mock_entry_detail
{
   enum ryanmock_mock_entry_type                type;
   struct ryanmock_mock_entry_return            ret;
   struct ryanmock_mock_entry_parameter_output  paramOut;
   struct ryanmock_mock_entry_parameter_check   paramCheck;
   struct ryanmock_mock_entry_function          function;
};

struct ryanmock_mock_entry
{
   const char * functionName;
   int expectedCount;
   int actualCount;
   union ryanmock_mock_entry_detail detail;
};

struct ryanmock_mock_list
{
   struct ryanmock_mock_entry * base;
   size_t size;
   size_t count;
};


static inline
void rmMockListInit(struct ryanmock_mock_list * list)
{
   list->base  = NULL;
   list->count = 0;
   list->size  = 0;
}

static inline
void rmMockListClear(struct ryanmock_mock_list * list);

static inline
void rmMockListTeardown(struct ryanmock_mock_list * list)
{
   rmMockListClear(list);
   if(list->base != NULL)
   {
      free(list->base);
      list->base = NULL;
   }
   list->count = 0;
   list->size  = 0;
}

static inline
struct ryanmock_mock_entry * rmMockListGet(struct ryanmock_mock_list * list, size_t * count)
{
   if(count != NULL)
   {
      (*count) = list->count;
   }
   return list->base;
}

static inline
void rmMockListClear(struct ryanmock_mock_list * list)
{
   // Cleanup Everything
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct ryanmock_mock_entry * entry = &list->base[i];
      switch(entry->detail.type)
      {
      case eRMMET_return:
         rmValueStoredRelease(entry->detail.ret.info.value,
                              entry->detail.ret.info.valueType);
         rmTypeRelease(entry->detail.ret.info.valueType);
         break;
      case eRMMET_parameterOutput:
         rmValueStoredRelease(entry->detail.paramOut.info.value,
                              entry->detail.paramOut.info.valueType);
         rmTypeRelease(entry->detail.paramOut.info.valueType);
         break;
      case eRMMET_parameterCheck:
         rmCheckRelease(entry->detail.paramCheck.check);
         break;
      case eRMMET_functionCall:
         rmStackTeardown(&entry->detail.function.stack);
         break;
      }
   }

   // Clear the list
   list->count = 0;
}

static inline
struct ryanmock_mock_entry * rmMockListAdd(struct ryanmock_mock_list * list)
{
   struct ryanmock_mock_entry * entry;
   // Make room if necessary
   if(list->count >= list->size)
   {
      list->size = list->count + 32;
      list->base = realloc(list->base, sizeof(struct ryanmock_mock_entry) * list->size);
   }

   // Add the element
   entry = &list->base[list->count];
   list->count ++;
   return entry;
}

static struct ryanmock_mock_list rmMockList;

static inline
void rmStackInit(struct ryanmock_stack * stack)
{
   stack->base  = NULL;
   stack->size  = 0;
   stack->count = 0;
}

static inline
void rmStackClear(struct ryanmock_stack * stack)
{
   size_t i;
   for(i = 0; i < stack->count; i++)
   {
      rmStackFrameCleanup(&stack->base[i]);
   }
   stack->count = 0;
}

static inline
void rmStackTeardown(struct ryanmock_stack * stack)
{
   if(stack->base != NULL)
   {
      rmStackClear(stack);
      free(stack->base);
      stack->base = NULL;
   }
   stack->size  = 0;
   stack->count = 0;
}


static inline
struct ryanmock_stack_frame * rmStackAdd(struct ryanmock_stack * stack)
{
   struct ryanmock_stack_frame * frame;
   // Make room if needed
   if(stack->count >= stack->size)
   {
      stack->size = stack->count + 32;
      stack->base = realloc(stack->base, sizeof(struct ryanmock_stack_frame) * stack->size);
   }
   // Get the frame at count
   frame = &stack->base[stack->count];
   stack->count ++;
   return frame;
}

static inline
struct ryanmock_stack_frame * rmStackPeek(struct ryanmock_stack * stack)
{
   if(stack->count == 0)
   {
      return NULL;
   }
   return &stack->base[stack->count - 1];
}
static inline
bool rmStackPopInternal(struct ryanmock_stack * stack)
{

   if(stack->count == 0)
   {
      return false;
   }
   stack->count --;
   rmStackFrameCleanup(&stack->base[stack->count]);
   return true;
}

static inline
const struct ryanmock_stack_frame * rmStackGetConst(const struct ryanmock_stack * stack, size_t * count)
{
   if(count != NULL)
   {
      (*count) = stack->count;
   }
   return stack->base;
}

static inline
void rmStackCopyDeep(struct ryanmock_stack * dest, const struct ryanmock_stack * src)
{
   size_t byteSize, i;
   dest->count = src->count;
   dest->size  = src->count;

   byteSize = sizeof(struct ryanmock_stack_frame) * dest->size;
   dest->base = malloc(byteSize);
   for(i = 0; i < src->count; i++)
   {
      rmStackFrameClone(&dest->base[i], &src->base[i]);
   }
}
static inline
const struct ryanmock_stack_frame * rmStackFindMatchingToken(const struct ryanmock_stack * stack, 
                                                             const void * token)
{
   size_t i;
   for(i = stack->count - 1; i < stack->count; i--)
   {
      const struct ryanmock_stack_frame * frame = &stack->base[i];
      if(frame->token == token)
      {
         return frame;
      }
   }
   return NULL;
}


static inline
void rmStackWriteTraceToStream(const struct ryanmock_stack * stack,
                               struct ryanmock_stream * stream,
                               const char * startLineWith)
{
   const struct ryanmock_stack_frame * frames;
   size_t count, i;
   frames = rmStackGetConst(stack, &count);

   for(i = 0; i < count; i++)
   {
      const struct ryanmock_stack_frame * frame = &frames[i];
      rmStreamWrite(stream, "%sFrame: %3d, Line: %5d, File: %s", startLineWith,
              i, frame->line, frame->filename);
      if(frame->text != NULL)
      {
         rmStreamWrite(stream, ", Text: %s", frame->text);
      }

      if(i < count - 1)
      {
         rmStreamWrite(stream, "\n");
      }
   }

   if(count == 0)
   {
      rmStreamWrite(stream, "%s[Empty]", startLineWith);
   }
}



struct ryanmock_value_stored * rmTypeCreateStoredValue(const struct ryanmock_type * type,
                                                       const void * value,
                                                       const char * valueText,
                                                       size_t size)
{
   struct ryanmock_value_stored * stored;
   stored = malloc(sizeof(struct ryanmock_value_stored) + size);
   stored->parent.isStored = true;
   stored->parent.valueText = valueText;
   stored->parent.data = stored->data;
   stored->parent.size = size;
   stored->refCount = 1;
   rmStackCopyDeep(&stored->stack, &rmStack);
   rmTypeDataCopy(type, stored->data, rmTypeSize(type), value);
   return stored;
}

struct ryanmock_value_stored * rmTypeCreateStoredValueTypeSize(const struct ryanmock_type * type,
                                                               const void * value,
                                                               const char * valueText)
{
   return rmTypeCreateStoredValue(type, value, valueText, rmTypeSize(type));
}


void rmStackPush(int rmLine, const char * rmFilename, 
                 const char * rmFunctionName, 
                 int line, const char * filename,
                 const void * token, const char * format, ...)
{
   if(token == NULL)
   {
      rmEnterFunction(rmFunctionName, rmLine, rmFilename);
      rmStreamWrite(&rmStreamMsg, "token parameter is NULL");
      rmTestError();
   }
   else
   {
      const struct ryanmock_stack_frame * matchedFrame = rmStackFindMatchingToken(&rmStack, token);
      if(matchedFrame != NULL)
      {
         rmEnterFunction(rmFunctionName, rmLine, rmFilename);
         rmStreamWrite(&rmStreamMsg, "Provided token %p matches previous frame\n"
                                     "Previous Frame Name: %s", 
                                     token, matchedFrame->text);
         rmTestError();
      }
      else
      {
         struct ryanmock_stack_frame * frame = rmStackAdd(&rmStack);
         va_list args;
         va_start(args, format);
         rmStackFrameInitVTextf(frame, filename, line, token, format, args);
         va_end(args);
      }
   }
}

void rmStackPop(const void * token, int rmLine, const char * rmFilename, 
                const char * rmFunctionName)
{
   if(token == NULL)
   {
      rmEnterFunction(rmFunctionName, rmLine, rmFilename);
      rmStreamWrite(&rmStreamMsg, "token parameter is NULL");
      rmTestError();
   }
   else
   {
      struct ryanmock_stack_frame * frame = rmStackPeek(&rmStack);
      if(frame->token != token)
      {
         rmEnterFunction(rmFunctionName, rmLine, rmFilename);
         rmStreamWrite(&rmStreamMsg, "Expected token %p does not match "
                                     "actual token %p\n"
                                     "The frame text is: \"%s\"\n"
                                     "The Stack is probably corrupted",
                                     token, frame->token, frame->text);
         rmTestError();
         
      }
      else
      {
         rmStackPopInternal(&rmStack);
      }
   }
   
}

void rmEnterFunction(const char * functionCallText, int line, const char * filename)
{
   struct ryanmock_stack_frame * frame = rmStackAdd(&rmStack);
   rmStackFrameInitFunction(frame, filename, line, functionCallText);
}

void rmExitFunction(const char * functionCallText, int line, const char * filename)
{
   struct ryanmock_stack_frame * frame = rmStackPeek(&rmStack);
   if(frame != NULL && (
      (frame->text == NULL && functionCallText == NULL) ||
      strcmp(frame->text, functionCallText) == 0))
   {
      rmStackPopInternal(&rmStack);
   }
   else
   {
      rmEnterFunction(__func__, line, filename);
      // Treat this is an error
      rmStreamWrite(&rmStreamMsg, "Unexpected Stack Exit Transition!\n"
                                  "Expected: %s\n"
                                  "Actual:   %s\n"
                                  "The Stack is probably corrupted",
                                  frame->text,
                                  functionCallText);
      rmTestError();
   }
}

static void PrintTestsHeader(const struct ryanmock_config * config, 
                             const char * name, size_t numberOfTestsToRun)
{
   if(config->writeResultsToStdOut)
   {
      rmColorPrintf(eRMTC_Green, RM_TEXT_DIV);
      printf("Running %d test(s) from %s.\n", 
             (int)numberOfTestsToRun, name);
   }
}

struct ryanmock_test_stats
{
   size_t passed;
   size_t failed;
   size_t errored;
   size_t skipped;
   size_t notSkipped;
};

enum rmtest_result_state
{
   e_rmtrs_notRun,
   e_rmtrs_successs,
   e_rmtrs_failure,
   e_rmtrs_error,
   e_rmtrs_skipped
};

struct rmtest_result
{
   enum rmtest_result_state state;
   char * messageText;
   char * infoText;
};

struct rmtest_data
{
   const struct ryanmock_test * test;
   struct rmtest_result result;
};

static inline
void PrintTestsFooterTestDataStatus(const struct rmtest_result * testResult, 
                                    const char * text)
{
   switch(testResult->state)
   {
   case e_rmtrs_failure:
      rmColorPrintf(eRMTC_Red, RM_TEXT_FAILED);
      printf("%s\n", text);
      break;
   case e_rmtrs_error:
      rmColorPrintf(eRMTC_Red, RM_TEXT_ERROR);
      printf("%s\n", text);
      break;
   default:
      // Do nothing intentionaly
      break;
   }
}

static inline
void PrintTestsFooter(const struct ryanmock_config * config,
                      const struct ryanmock_test_stats * stats,
                      const struct rmtest_result * setupTestData,
                      const struct rmtest_result * teardownTestData)
{
   if(config->writeResultsToStdOut)
   {
      rmColorPrintf(eRMTC_Green, RM_TEXT_DIV);
      printf("%d test(s) run.\n", (int)(stats->passed + stats->failed + stats->errored));
      rmColorPrintf(eRMTC_Green, RM_TEXT_PASSED);
      printf("%d test(s).\n", (int)stats->passed);
      
      if(stats->skipped > 0)
      {
         rmColorPrintf(eRMTC_Yellow, RM_TEXT_SKIPPED);
         printf("%d test(s).\n", (int)stats->skipped);
      }
      if(stats->errored > 0)
      {
         rmColorPrintf(eRMTC_Red, RM_TEXT_ERROR);
         printf("%d test(s).\n", (int)stats->errored);
      }
      if(stats->failed > 0)
      {
         rmColorPrintf(eRMTC_Red, RM_TEXT_FAILED);
         printf("%d test(s).\n", (int)stats->failed);
      }
      PrintTestsFooterTestDataStatus(setupTestData,    "Test Setup Function");
      PrintTestsFooterTestDataStatus(teardownTestData, "Test Teardown Function");
   }
}

static void PrintTestHeader(const struct ryanmock_config * config,
                            const char * testName)
{
   if(config->writeResultsToStdOut)
   {
      rmColorPrintf(eRMTC_Green, RM_TEXT_RUN);
      printf("%s\n", testName);
   }
}

static void PrintTestPassedFooter(const struct ryanmock_config * config,
                                  const char * testName)
{
   if(config->writeResultsToStdOut)
   {
      rmColorPrintf(eRMTC_Green, RM_TEXT_OK);
      printf("%s\n", testName);
   }
}

static void PrintLineThatStartWith(const char * firstStart,
                                   const char * restStart,
                                   enum ryanmock_term_color startColor,
                                   const char * text)
{
   const char * next, *start;
   if(text != NULL && text[0] != '\0')
   {
      next = strchr(text, '\n');
      if(next == NULL)
      {
         rmColorPrintf(startColor, "%s", firstStart);
         
         printf("%s\n", text);
      }
      else
      {
         rmColorPrintf(startColor, "%s", firstStart);
         printf("%.*s\n", (int)(next - text), text);
         while(next != NULL)
         {
            start = next + 1;
            next = strchr(start, '\n');

            if(next == NULL)
            {
               rmColorPrintf(startColor, "%s", restStart);
               printf("%s\n", start);
            }
            else
            {
               rmColorPrintf(startColor, "%s", restStart);
               printf("%.*s\n", (int)(next - start), start);
            }
         }
      }
   }
}

static inline
void PrintTestFailedFooter(const struct ryanmock_config * config,
                           const char * testName, const char * testMessage,
                                                  const char * testInfo)
{
   if(config->writeResultsToStdOut)
   {
      PrintLineThatStartWith(RM_TEXT_FAIL, RM_TEXT_INFO, eRMTC_Red,    testMessage);
      PrintLineThatStartWith(RM_TEXT_INFO, RM_TEXT_INFO, eRMTC_Yellow, testInfo);
      rmColorPrintf(eRMTC_Red, RM_TEXT_FAILURE);
      printf("%s\n", testName);
   }
}

static inline
void PrintTestSkippedFooter(const struct ryanmock_config * config,
                            const char * testName, const char * testMessage,
                                                   const char * testInfo)
{
   if(config->writeResultsToStdOut)
   {
      PrintLineThatStartWith(RM_TEXT_SKIP, RM_TEXT_INFO, eRMTC_Yellow, testMessage);
      PrintLineThatStartWith(RM_TEXT_INFO, RM_TEXT_INFO, eRMTC_Yellow, testInfo);
      rmColorPrintf(eRMTC_Yellow, RM_TEXT_SKIPPED);
      printf("%s\n", testName);
   }
}


static inline
void PrintTestErrorFooter(const struct ryanmock_config * config,
                          const char * testName, const char * testMessage,
                                                 const char * testInfo)
{
   if(config->writeResultsToStdOut)
   {
      PrintLineThatStartWith(RM_TEXT_ERROR, RM_TEXT_ERROR, eRMTC_Red,    testMessage);
      PrintLineThatStartWith(RM_TEXT_INFO,  RM_TEXT_INFO,  eRMTC_Yellow, testInfo);
      rmColorPrintf(eRMTC_Red, RM_TEXT_ERROR);
      printf("%s\n", testName);
   }
}

static
void TestSegfaultSignalHandler(int sig)
{
   (void)sig;

   rmStreamEnsureNewLine(&rmStreamMsg);
   rmStreamWrite(&rmStreamMsg, "Segmentation Fault!");

   rmTestError();
}



static inline 
void FreeTestResult(struct rmtest_result * testResult)
{
   if(testResult->messageText != NULL)
   {
      free(testResult->messageText);
   }
   if(testResult->infoText != NULL)
   {
      free(testResult->infoText);
   }
}

static inline
void WriteXMLSingleTestResult(FILE * f, const char * name, const struct rmtest_result * testResult)
{
   fprintf(f, "   <testcase name=\"%s\"", name);
   switch(testResult->state)
   {
   case e_rmtrs_failure:
      fprintf(f, ">\n");
      fprintf(f, "      <failure message=\"%s\">\n", testResult->messageText);
      fprintf(f, "%s\n", testResult->infoText);
      fprintf(f, "      </failure>\n");
      fprintf(f, "   </testcase>\n");
      break;
   case e_rmtrs_error:
      fprintf(f, ">\n");
      fprintf(f, "      <error message=\"%s\">\n", testResult->messageText);
      fprintf(f, "%s\n", testResult->infoText);
      fprintf(f, "      </error>\n");
      fprintf(f, "   </testcase>\n");
      break;
   default:
      fprintf(f, " />\n");
      break;
   }
}

static inline
void WriteXMLFile(const struct ryanmock_config * config,
                  const struct rmtest_data * testData,
                  const char * testSetupName,
                  const struct rmtest_result * setupTestData,
                  const char * testTeardownName,
                  const struct rmtest_result * teardownTestData,
                  const struct ryanmock_test_stats * stats)
{
   FILE * f;
   f = fopen(config->xmlOutputFilename, "w");
   if(f != NULL)
   {
      size_t i;
      fprintf(f, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
      // TODO: Add time="25"
      fprintf(f, "<testsuite tests=\"%d\" failures=\"%d\" disabled=\"%d\" errors=\"%d\" name=\"%s\"",
                  (int)config->numberOfTests, (int)stats->failed,
                  (int)stats->skipped, (int)stats->errored,
                  config->name);
      if(config->order == eRMCRO_Random)
      {
         fprintf(f, " seed=\"%u\"", config->randomTestOrderSeed);
      }
      if(config->stackFillCount > 0)
      {
         fprintf(f, " stackfillcount=\"%" PRI_SIZET "\" stackfillvalue=\"0x%02X\"",
                    config->stackFillCount, config->stackFillValue);
      }
      fprintf(f, ">\n");
      
      if(setupTestData->state != e_rmtrs_successs &&
         setupTestData->state != e_rmtrs_notRun)
      {
         WriteXMLSingleTestResult(f, testSetupName, setupTestData);
      }
      
      for(i = 0; i < stats->notSkipped; i++)
      {
         WriteXMLSingleTestResult(f, testData[i].test->name, &testData[i].result);
      }
      
      if(teardownTestData->state != e_rmtrs_successs &&
         teardownTestData->state != e_rmtrs_notRun)
      {
         WriteXMLSingleTestResult(f, testTeardownName, teardownTestData);
      }
      
      fprintf(f, "</testsuite>\n");
      fclose(f);
   }
}

static inline
bool rmParseInteger(const char * string, unsigned int * parsedValue, int base)
{
   char * endptr;
   long int result;
   errno = 0;
   result = strtol(string, &endptr, base);
   if(result == 0 && (errno != 0 || string == endptr))
   {
      return false;
   }
   
   *parsedValue = result;
   return true;
}

static inline
bool rmParseSizeT(const char * string, size_t * parsedValue, int base)
{
   unsigned int tempValue;
   bool result = rmParseInteger(string, &tempValue, base);
   *parsedValue = (size_t)tempValue;
   return result;
}

static inline
bool rmParseUnsignedChar(const char * string, unsigned char * parsedValue, int base)
{
   unsigned int tempValue;
   bool result = rmParseInteger(string, &tempValue, base);
   *parsedValue = (unsigned char)tempValue;
   return result;
}

static inline 
const char * rmGetFilenameFromPath(const char * path)
{
   const char * lastForward, * lastBackward;
   if(path == NULL)
   {
      return NULL;
   }
   lastForward  = strrchr(path, '/');
   lastBackward = strrchr(path, '\\');
   if(lastForward == NULL && lastBackward == NULL)
   {
      return path;
   }
   if(lastForward == NULL)
   {
      return lastBackward + 1;
   }
   if(lastBackward == NULL)
   {
      return lastForward + 1;
   }
   if(lastForward > lastBackward)
   {
      return lastForward + 1;
   }
   return lastBackward + 1;
}

static inline
bool rmIsCommandLineParam(const char * str)
{
   return str[0] == '-';
}

bool rmConfigCommandLine(struct ryanmock_config * config,
                         int argc, char * args[])
{
   int argIndex = 1;
   if(config->name == NULL)
   {
      config->name              = rmGetFilenameFromPath(args[0]);
   }
   config->order                = eRMCRO_InOrder;
   config->randomTestOrderSeed  = 0;
   config->soloTestNames        = NULL;
   config->xmlOutputFilename    = NULL;
   config->writeResultsToStdOut = true;
   config->justListTests        = false;
   config->breakOnBadResult     = false;
   config->stackFillCount       = 0;
   config->stackFillValue       = 0;
   while(argIndex < argc)
   {
      const char * arg = args[argIndex];
      if(strcmp(arg, "-r")       == 0 ||
         strcmp(arg, "--random") == 0)
      {
         bool generateSeed = true;
         if(config->order != eRMCRO_InOrder)
         {
            fprintf(stderr, "Can't set random order "
                            "because already configured.\n");
            return false;
         }
         config->order = eRMCRO_Random;
         if(argIndex + 1 < argc)
         {
            const char * seedArg = args[argIndex + 1];
            if(rmParseInteger(seedArg, &config->randomTestOrderSeed, 0))
            {
               generateSeed = false;
               argIndex ++;
            }
         }
         if(generateSeed)
         {
            config->randomTestOrderSeed = time(NULL);
         }
      }
      else if(strcmp(arg, "-x")           == 0 ||
              strcmp(arg, "--xml-output") == 0)
      {
         if(argIndex + 1 < argc && 
            !rmIsCommandLineParam(args[argIndex + 1]))
         {
            argIndex ++;
            config->xmlOutputFilename = args[argIndex];
         }
         else
         {
            fprintf(stderr, "Expected XML Output Filename after %s.\n", arg);
            return false;
         }
      }
      else if(strcmp(arg, "-q")      == 0 ||
              strcmp(arg, "--quiet") == 0)
      {
         config->writeResultsToStdOut = false;
      }
      else if(strcmp(arg, "-s")      == 0 ||
              strcmp(arg, "--solo")  == 0)
      {
         if(config->order != eRMCRO_InOrder)
         {
            fprintf(stderr, "Can't set solo order "
                            "because already configured.\n");
            return false;
         }
         config->order = eRMCRO_Solo;
         size_t soloTestNameCount = 0;
         int firstArgIndex = argIndex + 1;
         while(argIndex + 1 < argc && 
               !rmIsCommandLineParam(args[argIndex + 1]))
         {
            argIndex ++;
            soloTestNameCount ++;
         }
         if(soloTestNameCount > 0)
         {
            size_t i;
            config->soloTestNameCount = soloTestNameCount;
            config->soloTestNames = calloc(soloTestNameCount, sizeof(const char *));
            for(i = 0; i < soloTestNameCount; i++)
            {
               config->soloTestNames[i] = args[firstArgIndex + i];
            }
         }
         else
         {
            fprintf(stderr, "Expected at least one test function name "
                             "after %s.\n", arg);
            return false;
         }
      }
      else if(strcmp(arg, "-c")       == 0 ||
              strcmp(arg, "--color")  == 0 ||
              strcmp(arg, "--colour") == 0)
      {
         if(argIndex + 1 < argc && 
            !rmIsCommandLineParam(args[argIndex + 1]))
         {
            const char * param = args[argIndex + 1];
            enum ryanmock_config_color_output colorOutput = eRMCCO_EnableIfPossible;
            argIndex ++;
            
            if(strcmp(param, "enable")  == 0 ||
               strcmp(param, "enabled") == 0 ||
               strcmp(param, "1")       == 0 ||
               strcmp(param, "true")    == 0 ||
               strcmp(param, "True")    == 0)
            {
               colorOutput = eRMCCO_Enabled;
            }
            else if(strcmp(param, "disable")  == 0 ||
                    strcmp(param, "disabled") == 0 ||
                    strcmp(param, "0")        == 0 ||
                    strcmp(param, "false")    == 0 ||
                    strcmp(param, "False")    == 0)
            {
               colorOutput = eRMCCO_Disabled;
            }
            else
            {
               fprintf(stderr, "Expected Color Name after %s, instead got %s.\n", arg, param);
               return false;
            }
            
            config->colorOutput = colorOutput;
         }
         else
         {
            config->colorOutput = eRMCCO_EnableIfPossible;
         }
      }
      else if(strcmp(arg, "-l")      == 0 ||
              strcmp(arg, "--list")  == 0)
      {
         config->justListTests = true;
      }
      else if(strcmp(arg, "-p")      == 0 ||
              strcmp(arg, "--combination")  == 0)
      {
         if(config->order != eRMCRO_InOrder)
         {
            fprintf(stderr, "Can't set combination order "
                            "because already configured.\n");
            return false;
         }
         config->order = eRMCRO_Combination; 
      }
      else if(strcmp(arg, "-b")      == 0 ||
              strcmp(arg, "--break")  == 0)
      {
         config->breakOnBadResult = true;
      }
      else if(strcmp(arg, "-f")      == 0 ||
              strcmp(arg, "--fill")  == 0)
      {
         if(argIndex + 1 < argc && 
            !rmIsCommandLineParam(args[argIndex + 1]))
         {
            argIndex ++;
            if(rmParseSizeT(args[argIndex], &config->stackFillCount, 0) && 
               argIndex + 1 < argc && 
               !rmIsCommandLineParam(args[argIndex + 1]))
            {
               argIndex ++;
               (void)rmParseUnsignedChar(args[argIndex], &config->stackFillValue, 0);
            }
         }
      }
      else if(strcmp(arg, "-h")     == 0 ||
              strcmp(arg, "-?")     == 0 ||
              strcmp(arg, "--help") == 0)
      {
         // Not really an error, but the effect is the same.
         return false;
      }
      else
      {
         fprintf(stderr, "Unknown argument: %s.\n", arg);
         return false;
      }
      argIndex ++;
   }
   return true;
}

static inline
void rmPrintHelp(const char * appName)
{
   printf(
"Usage %s [OPTION]...\n"
"Runs this test based on options.\n"
"\n"
"None of these OPTIONs are required:\n"
"  -r, --random SEED      Randomizes test order. If SEED is not specified a\n"
"                         random one will be chosen.\n"
"  -s, --solo TESTNAME    Runs only the test named TESTNAME.\n"
"  -p, --combination      Order tests such that each test is run after\n"
"                         every other test. Make it easy to spot setup issues.\n"
"  -x, --xml-output FILE  Writes XML results to file FILE.\n"
"  -q, --quiet            Suppresses Test output to Standard Output.\n"
"  -f, --fill SIZE VALUE  Fill the stack with SIZE bytes of VALUE.\n"             
"  -b, --break            Triggers a breakpoint if supported on error or \n"
"                         failure.\n"
"  -c, --color ENABLED    Set color output to be enabled or disabled.\n"
"  -l, --list             List tests.\n"
"  -h, --help, -?         Shows this message.\n"
"\n"
"Exit status:\n"
"  0 if all tests pass, no errors or failures,\n"
"  1 if at least one test fails and no test errors,\n"
"  2 if at least one test has an error,\n"
"  3 if configuration is incorrect (bad command line arguments),\n"
"  4 if configuraiton is not supported (system limitations),\n"
"  5 if information was supplied (listing tests, or requesting help)\n"
"\n"
"This test is using the ryanmock library:\n"
"  <https://gitlab.com/hansonry/ryanmock>\n",
          appName);
}

int rmRunTestsCmdLine(const struct ryanmock_test * tests, 
                      size_t numberOfTests,
                      const char * name,
                      rmstate_function testStateFunction,
                      int argc, char * args[])
{
   struct ryanmock_config config;
   int result = 0;
   rmConfigTestsSimple(&config, tests, numberOfTests, name, testStateFunction);
   if(!rmConfigCommandLine(&config, argc, args))
   {
      rmPrintHelp(rmGetFilenameFromPath(args[0]));
      return RYANMOCK_EXITCODE_INFORMATION_SUPPLIED;
   }
   result = rmRunTests(&config);

   // Free dynamic config memory
   if(config.soloTestNames != NULL)
   {
      free(config.soloTestNames);
      config.soloTestNames = NULL;
   }  

   return result;
}


void rmConfigTestsSimple(struct ryanmock_config * config,
                         const struct ryanmock_test * tests, 
                         size_t numberOfTests,
                         const char * name, 
                         rmstate_function suiteStateFunction)
{
   config->name                 = name;
   config->tests                = tests;
   config->numberOfTests        = numberOfTests;
   
   config->suiteStateFunction   = suiteStateFunction;
  
   config->order                = eRMCRO_InOrder; 
   config->randomTestOrderSeed  = 0;
   config->soloTestNames        = NULL;
   
   config->writeResultsToStdOut = true;
   config->xmlOutputFilename    = NULL;
   config->justListTests        = false;
   config->breakOnBadResult     = false;
   config->stackFillCount       = 0;
   config->stackFillValue       = 0;
   
   config->colorOutput          = eRMCCO_EnableIfPossible;
}

int rmRunTestsSimple(const struct ryanmock_test * tests, size_t numberOfTests, 
                     const char * name)
{
   struct ryanmock_config config;
   rmConfigTestsSimple(&config, tests, numberOfTests, name, NULL);
   return rmRunTests(&config);
}

static inline
void rmRandomizeTests(struct rmtest_data * tests, 
                      size_t numberOfTestsToRun, unsigned int seed)
{
   size_t i;
   const struct ryanmock_test * temp;
   srand(seed);
   
   // Implementing Fisher-Yates algorithm
   for(i = 0; i < numberOfTestsToRun - 1; i ++) 
   {
      const size_t j = (rand() % (numberOfTestsToRun - i)) + i;
      // Swap i and j
      temp = tests[i].test;
      tests[i].test = tests[j].test;
      tests[j].test = temp;
   }
}

static inline
void rmTestResultInit(struct rmtest_result * testResult)
{
   testResult->state       = e_rmtrs_notRun;
   testResult->messageText = NULL;
   testResult->infoText    = NULL;
}

typedef void (*rmprotected_function)(void * data);

static inline
enum rmtest_result_state rmRunInProtectedMode(rmprotected_function function,
                                              void * data,
                                              struct rmtest_result * testResult,
                                              bool breakOnBadResult, 
                                              size_t stackFillCount, 
                                              unsigned char stackFillValue)
{
   sighandler_t originalSegfaultSignalHandler = NULL;
   BreakOnBadResult = breakOnBadResult;
   switch(setjmp(JumpBuffer))
   {
   case JUMP_VALUE_NOJUMP:
      rmStackClear(&rmStack);
      rmMockListClear(&rmMockList);
      rmCheckStackRelease();
      MockBeginFunctionName = NULL;
      // Run the test
      originalSegfaultSignalHandler = signal(SIGSEGV, TestSegfaultSignalHandler);

      // Clear Stack
      rmFillStackData(stackFillCount, stackFillValue);

      // Do the thing all protected 
      function(data); 

      rmStackClear(&rmStack); // Remove possibly confusing Stack
      if(MockBeginFunctionName != NULL)
      {
         rmStreamWrite(&rmStreamMsg, "End of Test reached without expected rmMockEnd");
         rmTestError();
      }
      rmFunctionUnderTestCalled(__LINE__, __FILE__, 
                                "rmFunctionUnderTestCalled");
      // If we got here, the test must have passed
      signal(SIGSEGV, originalSegfaultSignalHandler);
      
      testResult->state = e_rmtrs_successs;
      break;
   case JUMP_VALUE_FAILURE:
      // Test Fail
      signal(SIGSEGV, originalSegfaultSignalHandler);
      testResult->state = e_rmtrs_failure;
      break;
   default: // Rollover to ERROR Intentional
   case JUMP_VALUE_ERROR:
      signal(SIGSEGV, originalSegfaultSignalHandler);
      testResult->state = e_rmtrs_error;
      break;
   case JUMP_VALUE_SKIPPED:
      signal(SIGSEGV, originalSegfaultSignalHandler);
      testResult->state = e_rmtrs_skipped;
      break;
   }
   rmCheckStackRelease();

   switch(testResult->state)
   {
   case e_rmtrs_failure:
   case e_rmtrs_error:
   case e_rmtrs_skipped:
      testResult->messageText = rmStreamGetCopy(&rmStreamMsg);
      testResult->infoText    = rmStreamGetCopy(&rmStreamInfo);
      break;
   default:
      testResult->messageText = NULL;
      testResult->infoText    = NULL;
      break;
   }

   return testResult->state;
}

struct rmProtectedTestRunData
{
   const struct ryanmock_test * test;
   rmstate_function suiteStateFunction;
};

static
void rmProtectedTestRun(void * dataVoid)
{
   struct rmProtectedTestRunData * data = dataVoid;
   const struct ryanmock_test * test = data->test;
   // Run Setups
   if(data->suiteStateFunction != NULL)
   {
      data->suiteStateFunction(eRMS_TestSetup, test);
   }
   if(data->test->stateFunction != NULL)
   {
      test->stateFunction(eRMS_TestSetup, test);
   }
   
   // Test Function
   if(test->testFunction != NULL)
   {
      test->testFunction();
   }

   // Run Teardowns
   if(test->stateFunction != NULL)
   {
      test->stateFunction(eRMS_TestTeardown, test);
   }
   if(data->suiteStateFunction != NULL)
   {
      data->suiteStateFunction(eRMS_TestTeardown, test);
   }
   
}

struct rmProtectedInternalSuiteData
{
   rmstate_function suiteStateFunction;
   enum ryanmock_state state;
};

static 
void rmProtectedInternalSuite(void * dataVoid)
{
   struct rmProtectedInternalSuiteData * data = dataVoid;
   data->suiteStateFunction(data->state, NULL);
}

static inline
int rmInternalRunTests(const struct ryanmock_config * config,
                       struct rmtest_data * testData, 
                       size_t numberOfTestsToRun,
                       const char * testSetupName,
                       struct rmtest_result * setupTestResult,
                       const char * testTeardownName,
                       struct rmtest_result * teardownTestResult,
                       rmstate_function suiteStateFunction,
                       struct ryanmock_test_stats * stats)
{
   size_t testIndex;
   const int resultSucess  = RYANMOCK_EXITCODE_PASS;
   const int resultFailure = RYANMOCK_EXITCODE_FAIL;
   const int resultError   = RYANMOCK_EXITCODE_ERROR;
   
   stats->passed     = 0;
   stats->failed     = 0;
   stats->errored    = 0;
   stats->skipped    = 0;
   
   RootCheck = NULL;
   
   // Run test wide setup
   if(suiteStateFunction != NULL)
   {
      struct rmProtectedInternalSuiteData pisd = 
      {
         .suiteStateFunction = suiteStateFunction,
         .state = eRMS_SuiteSetup
      };
      rmStreamClear(&rmStreamMsg);
      rmStreamClear(&rmStreamInfo);
      (void)rmRunInProtectedMode(rmProtectedInternalSuite, &pisd, 
                                 setupTestResult, config->breakOnBadResult,
                                 config->stackFillCount, 
                                 config->stackFillValue);
      switch(setupTestResult->state)
      {
      case e_rmtrs_skipped:
      case e_rmtrs_successs:
         // Do nothing on purpose
         break;
      case e_rmtrs_failure:
         PrintTestFailedFooter(config, testSetupName,
                               setupTestResult->messageText,
                               setupTestResult->infoText);
         return resultFailure;
         break;

      case e_rmtrs_error:
         stats->errored ++;
         PrintTestErrorFooter(config, testSetupName, 
                              setupTestResult->messageText,
                              setupTestResult->infoText);
         return resultError;
         break;
      case e_rmtrs_notRun:
         // This shouldn't be possible
         break;
      }
   }
   
   for(testIndex = 0; testIndex < numberOfTestsToRun; testIndex ++)
   {
      const struct ryanmock_test * test = testData[testIndex].test;
      struct rmtest_result * testResult = &testData[testIndex].result;
      if(testResult->state == e_rmtrs_skipped)
      {
         testResult->messageText = NULL;
         testResult->infoText    = NULL;
         stats->skipped ++;
      }
      else
      {
         struct rmProtectedTestRunData ptrd = {
            .test = test,
            .suiteStateFunction = suiteStateFunction
         };
         rmStreamClear(&rmStreamMsg);
         rmStreamClear(&rmStreamInfo);

         PrintTestHeader(config, test->name);
         switch(rmRunInProtectedMode(rmProtectedTestRun, &ptrd, 
                                     testResult, config->breakOnBadResult,
                                     config->stackFillCount, 
                                     config->stackFillValue))
         {
         case e_rmtrs_successs:
            stats->passed ++;
            stats->notSkipped ++;
            PrintTestPassedFooter(config, test->name);
            break;
         case e_rmtrs_failure:
            stats->failed ++;
            stats->notSkipped ++;
            PrintTestFailedFooter(config, test->name, 
                                  testResult->messageText,
                                  testResult->infoText);

            break;

         case e_rmtrs_error:
            stats->errored ++;
            stats->notSkipped ++;
            PrintTestErrorFooter(config, test->name, 
                                 testResult->messageText,
                                 testResult->infoText);
            break;
         case e_rmtrs_skipped:
            stats->skipped ++;
            PrintTestSkippedFooter(config, test->name, 
                                   testResult->messageText,
                                   testResult->infoText);
            break;
         case e_rmtrs_notRun:
            // This shouldn't be possible
            break;
         }
      }

   }
   // Run test wide teardown
   if(suiteStateFunction != NULL)
   {
      struct rmProtectedInternalSuiteData pisd = 
      {
         .suiteStateFunction = suiteStateFunction,
         .state = eRMS_SuiteTeardown
      };
      rmStreamClear(&rmStreamMsg);
      rmStreamClear(&rmStreamInfo);
      (void)rmRunInProtectedMode(rmProtectedInternalSuite, &pisd,
                                 teardownTestResult, config->breakOnBadResult,
                                 config->stackFillCount, 
                                 config->stackFillValue);
      switch(teardownTestResult->state)
      {
      case e_rmtrs_skipped:
      case e_rmtrs_successs:
         // Do nothing on purpose
         break;
      case e_rmtrs_failure:
         PrintTestFailedFooter(config, testTeardownName, 
                               teardownTestResult->messageText,
                               teardownTestResult->infoText);
         return resultFailure;
         break;

      case e_rmtrs_error:
         stats->errored ++;
         PrintTestErrorFooter(config, testTeardownName, 
                              teardownTestResult->messageText,
                              teardownTestResult->infoText);
         return resultError;
         break;
      case e_rmtrs_notRun:
         // This shouldn't be possible
         break;
      }
   }
  
   if(stats->errored > 0)
   {
      return resultError;
   }
   if(stats->failed > 0)
   {
      return resultFailure;
   }
   return resultSucess;
 
}

static inline
bool MatchTestName(const char * pattern, const char * testName)
{
   const char * pc = pattern;
   const char * lastWild = NULL;
   const char * tnc = testName;

   while(*pc != '\0' && *tnc != '\0')
   {
      if(*pc == *tnc)
      {
         pc  ++;
         tnc ++;
      }
      else if(*pc == '*')
      {
         pc ++;
         lastWild = pc;
      }
      else
      {
         if(lastWild == NULL)
         {
            return false;
         }
         else
         {
            pc = lastWild;
            tnc ++;
         }
      }
   }
   if(*pc == '\0' && *tnc == '\0')
   {
      return true;
   }

   if(*tnc == '\0')
   {
      // Check to see if there is any non wild charavters left in the pattern
      while(*pc != '\0')
      {
         if(*pc != '*')
         {
            return false;
         }
         pc ++;
      }
     return true;
   }
   return lastWild == pc;
}


int rmRunTests(const struct ryanmock_config * config)
{
   struct ryanmock_test_stats stats = {0};
   struct rmtest_data * testData = NULL;
   struct rmtest_result setupTestResult, teardownTestResult;
   const char * name = config->name;
   int testsResult;
   size_t numberOfTestsToRun     = 0;
   const char * testSetupName    = "Test::Setup";
   const char * testTeardownName = "Test::Teardown";
   size_t i, k, testDataIndex;

   if(config->stackFillCount > 0 && !rmFillStackIsSupported())
   {

      fprintf(stderr, "ryanmock does not support stack fill for this "
                      "archecture\n");
      return RYANMOCK_EXITCODE_CONFIG_NOT_SUPPORTED;
   }

   if(config->justListTests)
   {
      size_t i;
      for(i = 0; i < config->numberOfTests; i++)
      {
         printf("%s\n", config->tests[i].name);
      }
      return RYANMOCK_EXITCODE_INFORMATION_SUPPLIED;
   }   

   switch(config->order)
   {
   case eRMCRO_InOrder:
   case eRMCRO_Random:
      numberOfTestsToRun = config->numberOfTests;
      testData = calloc(numberOfTestsToRun, sizeof(struct rmtest_data));
      for(i = 0; i < config->numberOfTests; i++)
      {
         testData[i].test = &config->tests[i];
      } 
      // Randomize Tests if desired
      if(config->order == eRMCRO_Random && numberOfTestsToRun > 1)
      {
         rmRandomizeTests(testData, numberOfTestsToRun, 
                          config->randomTestOrderSeed);
      }

      break;
   case eRMCRO_Solo:
      // Get the counts of things
      numberOfTestsToRun = 0;
      for(i = 0; i < config->soloTestNameCount; i ++)
      {
         bool foundMatch = false;
         for(k = 0; k < config->numberOfTests; k++)
         {
            if(MatchTestName(config->soloTestNames[i], config->tests[k].name))
            {
               numberOfTestsToRun ++;
               foundMatch = true;
            }
         }
         if(!foundMatch)
         {
            fprintf(stderr, "Failed to find tests for soloing using \"%s\""
                            " For a match!\n", config->soloTestNames[i]);
            return RYANMOCK_EXITCODE_CONFIG_BAD;
         }
      }
      if(numberOfTestsToRun == 0)
      {
         fprintf(stderr, "Failed to find tests for soloing!\n");
         fprintf(stderr, "Attempted To Search Using:\n");
         for(i = 0; i < config->soloTestNameCount; i ++)
         {
            fprintf(stderr, "%s\n", config->soloTestNames[i]);
         }
      
         return RYANMOCK_EXITCODE_CONFIG_BAD;
      }
      testData = calloc(numberOfTestsToRun, sizeof(struct rmtest_data));
      testDataIndex = 0;
      // Find matches
      for(i = 0; i < config->soloTestNameCount; i ++)
      {
         for(k = 0; k < config->numberOfTests; k++)
         {
            if(MatchTestName(config->soloTestNames[i], config->tests[k].name))
            {
               testData[testDataIndex].test = &config->tests[k];
               testDataIndex ++;
            }
         }
      }
      break;
   case eRMCRO_Combination:
      numberOfTestsToRun = (config->numberOfTests * (config->numberOfTests - 1)) + 1;
      testData = calloc(numberOfTestsToRun, sizeof(struct rmtest_data));
      testDataIndex = 0;
      for(i = 0; i < config->numberOfTests; i++)
      {
         for(k = i + 1; k < config->numberOfTests; k++)
         {
            testData[testDataIndex + 0].test = &config->tests[i];
            testData[testDataIndex + 1].test = &config->tests[k];
            testDataIndex += 2;
         }
      } 
      testData[testDataIndex].test = &config->tests[0];
      break;
   }

   // Initialize Results
   rmTestResultInit(&setupTestResult);
   rmTestResultInit(&teardownTestResult);
   for(i = 0; i < numberOfTestsToRun; i++)
   {
      rmTestResultInit(&testData[i].result);
   }
   
   
   // Setup Color
   rmColorPrintInit(config->colorOutput);

   rmStreamInit(&rmStreamMsg);
   rmStreamInit(&rmStreamInfo);
   rmMockListInit(&rmMockList);
   rmStackInit(&rmStack);
   
   PrintTestsHeader(config, name, numberOfTestsToRun);
   
   if(config->stackFillCount > 0)
   {
      rmColorPrintf(eRMTC_Green, RM_TEXT_INFO);
      printf("Stack Prefill Count: %" PRI_SIZET ", Value: 0x%02X\n", 
             config->stackFillCount, config->stackFillValue);
   }

   if(config->order == eRMCRO_Random &&
      config->writeResultsToStdOut)
   {
      rmColorPrintf(eRMTC_Green, RM_TEXT_INFO);
      printf("Test Order Randomized, Seed: %u\n", 
             config->randomTestOrderSeed);
   }
   

   testsResult = rmInternalRunTests(config, 
                                    testData,
                                    numberOfTestsToRun,
                                    testSetupName,
                                    &setupTestResult,
                                    testTeardownName,
                                    &teardownTestResult,
                                    config->suiteStateFunction,
                                    &stats);

   PrintTestsFooter(config, &stats, &setupTestResult, &teardownTestResult);
   if(config->xmlOutputFilename != NULL)
   {
      WriteXMLFile(config, testData, 
                   testSetupName,    &setupTestResult, 
                   testTeardownName, &teardownTestResult, 
                   &stats);
   }

   // Free up Used Data
   for(i = 0; i < numberOfTestsToRun; i++)
   {
      FreeTestResult(&testData[i].result);
   }
   FreeTestResult(&setupTestResult);
   FreeTestResult(&teardownTestResult);
   free(testData);

   rmStreamTeardown(&rmStreamMsg);
   rmStreamTeardown(&rmStreamInfo);
   rmMockListTeardown(&rmMockList);
   rmStackTeardown(&rmStack);
   
   return testsResult;
}

static inline
void rmMockEntryWriteInfo(const struct ryanmock_mock_entry * entry)
{
   switch(entry->detail.type)
   {
   case eRMMET_return:
      rmWriteValueStoredStackTypeToStream(&rmStreamInfo, "MockValue",
                                          entry->detail.ret.info.value,
                                          entry->detail.ret.info.valueType);
      break;
   case eRMMET_parameterOutput:
      // TODO: Maybe write parameter here?
      rmWriteValueStoredStackTypeToStream(&rmStreamInfo, "MockValue",
                                          entry->detail.paramOut.info.value,
                                          entry->detail.paramOut.info.valueType);
      break;
   case eRMMET_parameterCheck:
      rmCheckWriteInfo(entry->detail.paramCheck.check, &rmStreamInfo);
      break;
   case eRMMET_functionCall:
      rmStreamEnsureNewLine(&rmStreamInfo);
      if(entry->detail.function.ignore)
      {
         rmStreamWrite(&rmStreamInfo, "(Ignored %s", entry->functionName);
      }
      else
      {
         rmStreamWrite(&rmStreamInfo, "(Expected %s", entry->functionName);
      }

      rmStreamWrite(&rmStreamInfo, ") Stack Trace:\n");
      rmStackWriteTraceToStream(&entry->detail.function.stack, &rmStreamInfo, "   ");
      break;
   }
}

void rmFunctionUnderTestCalled(int line, const char * filename, const char * function)
{
   struct ryanmock_mock_entry * entries;
   size_t count, i;
   int type = JUMP_VALUE_NOJUMP;
   rmEnterFunction(function, line, filename);

   // Check and complain about unused mocks
   entries = rmMockListGet(&rmMockList, &count);
   for(i = 0; i < count; i++)
   {
      struct ryanmock_mock_entry * entry = &entries[i];

      if(entry->expectedCount > 0 &&
         entry->actualCount < entry->expectedCount)
      {
         switch(entry->detail.type)
         {
         case eRMMET_return:
            rmStreamWrite(&rmStreamMsg, "Return Value for function %s only used %d out of %d times",
                                        entry->functionName,
                                        entry->actualCount, entry->expectedCount);
            break;
         case eRMMET_parameterOutput:
           rmStreamWrite(&rmStreamMsg, "Output Value for parameter %s in function %s only used %d out of %d times",
                                        entry->detail.paramOut.parameterName, entry->functionName,
                                        entry->actualCount,                   entry->expectedCount);
            break;
         case eRMMET_parameterCheck:
            rmStreamWrite(&rmStreamMsg, "Parameter Check Value for parameter %s in function %s only used %d out of %d times",
                                         entry->detail.paramCheck.parameterName, entry->functionName,
                                         entry->actualCount,                     entry->expectedCount);
            break;
         case eRMMET_functionCall:
            if(entry->detail.function.ignore)
            {
               rmStreamWrite(&rmStreamMsg, "Expected ignored function Call %s only used %d out of %d times",
                                            entry->functionName,
                                            entry->actualCount,                     entry->expectedCount);
            }
            else
            {
               rmStreamWrite(&rmStreamMsg, "Expected function Call %s only used %d out of %d times",
                                            entry->functionName,
                                            entry->actualCount,                     entry->expectedCount);
            }
            break;
         }
         rmMockEntryWriteInfo(entry);
         type = JUMP_VALUE_FAILURE;
         break;
      }
      else if(entry->expectedCount == rmmCOUNT_ALLWAYS &&
              entry->actualCount == 0)
      {
         switch(entry->detail.type)
         {
         case eRMMET_return:
            rmStreamWrite(&rmStreamMsg, "Return Value for function %s was expected to be used at least once",
                                        entry->functionName,
                                        entry->actualCount, entry->expectedCount);

            break;
         case eRMMET_parameterOutput:
            rmStreamWrite(&rmStreamMsg, "Output Value for parameter %s in function %s was expected to be used at least once",
                                        entry->detail.paramOut.parameterName, entry->functionName,
                                        entry->actualCount,                   entry->expectedCount);

            break;
         case eRMMET_parameterCheck:
            rmStreamWrite(&rmStreamMsg, "Output Value for parameter %s in function %s was expected to be used at least once",
                                        entry->detail.paramCheck.parameterName, entry->functionName,
                                        entry->actualCount,                     entry->expectedCount);

            break;
         case eRMMET_functionCall:
            if(entry->detail.function.ignore)
            {
               rmStreamWrite(&rmStreamMsg, "Function %s was expected to be ignored at least once",
                                           entry->functionName,
                                           entry->actualCount, entry->expectedCount);

            }
            else
            {
               rmStreamWrite(&rmStreamMsg, "Function %s was expected to be called at least once",
                                           entry->functionName,
                                           entry->actualCount, entry->expectedCount);
            }
            break;
         }

         rmMockEntryWriteInfo(entry);
         type = JUMP_VALUE_FAILURE;
         break;
      }
   }

   // Clear the mock list for next check
   rmMockListClear(&rmMockList);

   if(type != JUMP_VALUE_NOJUMP)
   {
      rmTestFailure();
   }
   rmExitFunction(function, line, filename);
}

void rmSkip(int line, const char * filename, const char * function)
{
   rmEnterFunction(function, line, filename);
   rmStreamWrite(&rmStreamMsg, "Test skipped itself");
   rmTestSkipped();
   // We will never reach this point
}

static inline
void rmCheckStackAdd(struct ryanmock_check * check)
{
   rmCheckTake(check);
   check->parent = RootCheck;
   RootCheck = check;
}

static inline
void rmCheckStackRelease(void)
{
   struct ryanmock_check * check, * temp;
   check = RootCheck;
   while(check != NULL)
   {
      temp = check;
      check = check->parent;
      temp->parent = NULL;
      rmCheckRelease(temp);
   }
   RootCheck = NULL;
}

static inline
void rmCheckStackPop(void)
{
   struct ryanmock_check * check = RootCheck;
   RootCheck = check->parent;
   check->parent = NULL;
   rmCheckRelease(check);
}

static inline
void rmCheckStackWriteInfo(struct ryanmock_stream * rmStreamInfo)
{
   struct ryanmock_check * check = RootCheck;
   while(check != NULL)
   {
      rmCheckWriteInfo(check, rmStreamInfo);
      check = check->parent;
   }
}

static inline
void rmDoCheck(struct ryanmock_check * check, 
               struct ryanmock_value * actualValue,
               enum ryanmock_comparison_operator2 destSizeOpCheck)
{
   enum ryanmock_result result;
   bool sizeCheckPassed;
   size_t expectedSize = rmTypeSize(check->type);

   rmCheckStackAdd(check);
   rmCheckRelease(check);
   
   sizeCheckPassed = rmSizeOperatorCheck(destSizeOpCheck, 
                                         actualValue->size, 
                                         expectedSize);
   if(sizeCheckPassed)
   {
      result = rmCheckCheck(check, actualValue, &rmStreamMsg, &rmStreamInfo);
   }
   else
   {
      const char * opText = rmGetOperator2String(destSizeOpCheck);
      rmStreamWrite(&rmStreamMsg, 
                    "Actual Size (%d) %s Expected Size (%d). Did you use the right type?",
                    (int)actualValue->size, opText, (int)expectedSize);
      result = eRMR_error;
   }

   if(result != eRMR_passed)
   {
      rmWriteValueStackTypeToStream(&rmStreamInfo, "Actual", actualValue,
                                    check->type, &rmStack);
   }

   
   rmValueRelease(actualValue, check->type);

   switch(result)
   {
   case eRMR_passed:
      // Intentionaly left empty
      break;
   case eRMR_failed:
      rmTestFailure();
      break;
   case eRMR_error:
      rmTestError();
      break;
   default:
      rmEnterFunction(__func__, __LINE__, __FILE__);
      rmStreamClear(&rmStreamMsg);
      rmStreamClear(&rmStreamInfo);
      rmStreamWrite(&rmStreamMsg, "Check Function Returned invalid value: %d", result);
      rmTestError();
      break;
   }
   rmCheckStackPop();
}

static inline
void rmValueInit(struct ryanmock_value * value,
                 const void * data,
                 const char * valueText,
                 size_t size)
{
   value->data        = data;
   value->valueText   = valueText;
   value->size        = size;
   value->isStored    = false;
}

static inline
void rmDoCheckValue(struct ryanmock_check * check, const void * actualPtr, 
                    const char * actualText, size_t actualSize)
{
   struct ryanmock_value actual;
   rmValueInit(&actual, actualPtr, actualText, actualSize);
   rmDoCheck(check, &actual, eRMCO2_NotApplicable);
}

static inline
void rmAssert2(enum ryanmock_comparison_operator2 op,
               struct ryanmock_type * type,
               const void * actual,   const char * actualText,
               const void * expected, const char * expectedText)
{
   struct ryanmock_check * check2;
   struct ryanmock_value_stored * expectedValue;
   size_t size = rmTypeSize(type);

   expectedValue = rmTypeCreateStoredValue(type, expected, expectedText, size);
   check2 = rmCreateCheck2(op, type, expectedValue);

   rmDoCheckValue(check2, actual, actualText, size);
   // Note: check2 and actualValue will be freed at this point in time

}

void rmAssertType2(enum ryanmock_comparison_operator2 op,
                   struct ryanmock_type * type,
                   void * actual,   const char * actualText,
                   void * expected, const char * expectedText,
                   int line, const char * filename,
                   const char * functionName)
{
   rmEnterFunction(functionName, line, filename);

   rmAssert2(op, type, actual, actualText, expected, expectedText);

   rmExitFunction(functionName, line, filename);
}


// Generic 2 and 3 value assert helper function
#define COMMA ,
#define REF &
#define NOREF
#define NOPARAMS

#define MAKE_ASSERT2_FUNCTION(TypeName, Type, OtherParams, CreateType, Ref)  \
void rmAssert ## TypeName ## 2(enum ryanmock_comparison_operator2 op,        \
                               Type actual,   const char * actualText,       \
                               Type expected, const char * expectedText,     \
                               OtherParams                                   \
                               int line,     const char * filename,          \
                               const char * functionName)                    \
{                                                                            \
   struct ryanmock_type * type;                                              \
                                                                             \
   rmEnterFunction(functionName, line, filename);                            \
   type = CreateType;                                                        \
                                                                             \
   rmAssert2(op,           type,                                             \
             Ref actual,   actualText,                                       \
             Ref expected, expectedText);                                    \
                                                                             \
   rmExitFunction(functionName, line, filename);                             \
}


#define MAKE_ASSERT3_FUNCTION(TypeName, Type, OtherParams, CreateType, Ref)  \
void rmAssert ## TypeName ## 3(enum ryanmock_comparison_operator3 op,        \
                               Type actual,   const char * actualText,       \
                               Type expected1, const char * expected1Text,   \
                               Type expected2, const char * expected2Text,   \
                               OtherParams                                   \
                               int line,     const char * filename,          \
                               const char * functionName)                    \
{                                                                            \
   struct ryanmock_type * type;                                              \
                                                                             \
   rmEnterFunction(functionName, line, filename);                            \
   type = CreateType;                                                        \
                                                                             \
   rmAssert3(op,            type,                                            \
             Ref actual,    actualText,                                      \
             Ref expected1, expected1Text,                                   \
             Ref expected2, expected2Text);                                  \
                                                                             \
   rmExitFunction(functionName, line, filename);                             \
}



static inline
void rmAssert3(enum ryanmock_comparison_operator3 op,
               struct ryanmock_type * type,
               const void * actual,    const char * actualText,
               const void * expected1, const char * expected1Text,
               const void * expected2, const char * expected2Text)
{
   struct ryanmock_check * check3;
   struct ryanmock_value_stored * expected1Value, * expected2Value;
   size_t size = rmTypeSize(type);

   expected1Value = rmTypeCreateStoredValue(type, expected1, expected1Text, size);
   expected2Value = rmTypeCreateStoredValue(type, expected2, expected2Text, size);
   check3 = rmCreateCheck3(op, type, expected1Value, expected2Value);
   rmDoCheckValue(check3, actual, actualText, size);
   // Note: check3 and actualValue will be freed at this point in time
}

// Asserts

void rmFail(int line, const char * filename,
            const char * functionName, const char * reasonFormat, ...)
{
   va_list args;

   rmEnterFunction(functionName, line, filename);

   va_start(args, reasonFormat);
   rmStreamVWrite(&rmStreamMsg, reasonFormat, args);
   va_end(args);
   rmTestFailure();

   rmExitFunction(functionName, line, filename);
}


void rmAssert(bool value, const char * valueText, bool expected,
              int line, const char * filename, const char * functionName)
{
   struct ryanmock_check * trueCheck;
   rmEnterFunction(functionName, line, filename);
   trueCheck = rmCreateCheckBoolean(expected);

   rmDoCheckValue(trueCheck, &value, valueText, rmTypeSize(trueCheck->type));
   // Note: trueCheck and actualValue will be freed at this point in time
   rmExitFunction(functionName, line, filename);
}




void rmAssertPtrNULL(const void * value, const char * valueText,
                     bool shouldBeNULL, int line, const char * filename,
                     const char * functionName)
{
   struct ryanmock_check * nullCheck;
   rmEnterFunction(functionName, line, filename);
   nullCheck = rmCreateCheckNULL(shouldBeNULL);

   rmDoCheckValue(nullCheck, &value, valueText, rmTypeSize(nullCheck->type));
   // Note: nullCheck and actualValue will be freed at this point in time
   rmExitFunction(functionName, line, filename);
}


// Mocks
void rmMockBegin(const char * functionName, int expectedCount,
                 int line, const char * filename,
                 const char * thisFunctionName)
{
   rmEnterFunction(thisFunctionName, line, filename);
   if(MockBeginFunctionName != NULL)
   {
      rmStreamWrite(&rmStreamMsg, "rmMockBegin called without rmMockEnd");
      rmTestError();
   }
   MockFunctionExpectedCount = expectedCount;
   MockBeginFunctionName     = functionName;
   rmExitFunction(thisFunctionName, line, filename);
}

static inline
void rmInternalMockEnd(void)
{
   if(MockBeginFunctionName == NULL)
   {
      rmStreamWrite(&rmStreamMsg, "rmMockEnd called without rmMockBegin");
      rmTestError();
   }
   MockBeginFunctionName = NULL;
}

void rmMockEnd(int line, const char * filename,
               const char * thisFunctionName)
{
   rmEnterFunction(thisFunctionName, line, filename);
   rmInternalMockEnd();
   rmExitFunction(thisFunctionName, line, filename);
}

static inline
void rmCheckBeginFunctionName(void)
{
   if(MockBeginFunctionName == NULL)
   {
      struct ryanmock_stack_frame * currentFrame = rmStackPeek(&rmStack);
      rmStreamWrite(&rmStreamMsg, "%s called without MockBegin", currentFrame->text);
      rmTestError();
   }
}

static inline
struct ryanmock_mock_entry * rmMockListAddBasic(struct ryanmock_mock_list * list)
{
   struct ryanmock_mock_entry * entry;
   rmCheckBeginFunctionName();
   entry = rmMockListAdd(list);
   entry->functionName  = MockBeginFunctionName;
   entry->expectedCount = MockFunctionExpectedCount;
   entry->actualCount   = 0;
   return entry;
}

static inline
struct ryanmock_mock_entry * rmMockListAddReturn(struct ryanmock_mock_list * list, 
                                                 struct ryanmock_value_stored * value, 
                                                 struct ryanmock_type * type,
                                                 enum ryanmock_comparison_operator2 destSizeOpCheck)
{
   struct ryanmock_mock_entry * entry = rmMockListAddBasic(list);

   entry->detail.type                      = eRMMET_return;
   entry->detail.ret.info.value            = value;
   entry->detail.ret.info.valueType        = type;
   entry->detail.ret.info.destSizeOpCheck  = destSizeOpCheck;
   return entry;
}

static inline
struct ryanmock_mock_entry * rmMockListAddParamOut(struct ryanmock_mock_list * list, 
                                                   const char * parameterName, 
                                                   struct ryanmock_value_stored * value, 
                                                   struct ryanmock_type * type,
                                                   enum ryanmock_comparison_operator2 destSizeOpCheck)
{
   struct ryanmock_mock_entry * entry = rmMockListAddBasic(list);

   entry->detail.type                           = eRMMET_parameterOutput;
   entry->detail.paramOut.parameterName         = parameterName;
   entry->detail.paramOut.info.value            = value;
   entry->detail.paramOut.info.valueType        = type;
   entry->detail.paramOut.info.destSizeOpCheck  = destSizeOpCheck;
   return entry;
}

static inline
struct ryanmock_mock_entry * rmMockListAddParamCheck(struct ryanmock_mock_list * list, 
                                                     const char * parameterName, 
                                                     struct ryanmock_check * check,
                                                     enum ryanmock_comparison_operator2 destSizeOpCheck)
{
   struct ryanmock_mock_entry * entry = rmMockListAddBasic(list);

   entry->detail.type                             = eRMMET_parameterCheck;
   entry->detail.paramCheck.parameterName         = parameterName;
   entry->detail.paramCheck.check                 = check;
   entry->detail.paramCheck.destSizeOpCheck       = destSizeOpCheck;
   return entry;
}

static inline
struct ryanmock_mock_entry * rmMockListAddFunctionCalled(struct ryanmock_mock_list * list, bool ignore)
{
   struct ryanmock_mock_entry * entry = rmMockListAddBasic(list);
   entry->detail.type = eRMMET_functionCall;
   entry->detail.function.ignore = ignore;
   rmStackCopyDeep(&entry->detail.function.stack, &rmStack);
   return entry;
}

void rmExpectOrdered(bool ignore,
                     int line, const char * filename,
                     const char * functionName)
{
   rmEnterFunction(functionName, line, filename);
   (void)rmMockListAddFunctionCalled(&rmMockList, ignore);
   rmExitFunction(functionName, line, filename);
}

#define MAKE_WILL_RETURN_FUNCTION(TypeName, Type, OtherParams, CreateType,     \
                                  Ref, DestSizeOpCheck)                        \
void rmMockOutput ## TypeName (Type value, const char * valueText,         \
                                   const char * parameter,                     \
                                   OtherParams                                 \
                                   int line, const char * filename,            \
                                   const char * functionName)                  \
{                                                                              \
   struct ryanmock_value_stored * stored;                                      \
   struct ryanmock_type * type;                                                \
   rmEnterFunction(functionName, line, filename);                              \
   type = CreateType;                                                          \
   stored = rmTypeCreateStoredValueTypeSize(type, Ref value, valueText);       \
                                                                               \
   if(parameter == NULL)                                                       \
   {                                                                           \
      (void)rmMockListAddReturn(&rmMockList,                                   \
                                stored, type, DestSizeOpCheck);                \
      rmInternalMockEnd();                                                     \
   }                                                                           \
   else                                                                        \
   {                                                                           \
      (void)rmMockListAddParamOut(&rmMockList,                                 \
                                  parameter,                                   \
                                  stored, type, DestSizeOpCheck);              \
   }                                                                           \
   rmExitFunction(functionName, line, filename);                               \
}


static inline
bool rmMockListIsValidEntry(const struct ryanmock_mock_entry * entry,
                            const char * functionName,
                            const char * parameterName,
                            enum ryanmock_mock_entry_type type)
{
   if(entry->detail.type == type &&
      strcmp(entry->functionName, functionName) == 0)
   {
      switch(type)
      {
      case eRMMET_functionCall:
      case eRMMET_return:
         return true;
         break;
      case eRMMET_parameterOutput:
         return strcmp(entry->detail.paramOut.parameterName,
                       parameterName) == 0;
         break;
      case eRMMET_parameterCheck:
         return strcmp(entry->detail.paramCheck.parameterName,
                       parameterName) == 0;
         break;
      }
   }
   return false;
}

static inline
const char * rmMockEntryTypeGetName(enum ryanmock_mock_entry_type type)
{
   switch(type)
   {
   case eRMMET_return:
      return "Return";
      break;
   case eRMMET_parameterOutput:
      return "Parameter Output";
      break;
   case eRMMET_parameterCheck:
      return "Parameter Check";
      break;
   case eRMMET_functionCall:
      return "Function Call";
      break;
   default:
      return "?Unknown?";
      break;
   }
}

static inline
bool rmMockEntryHasMoreUsage(const struct ryanmock_mock_entry * entry)
{
   return entry->actualCount < entry->expectedCount ||
          entry->expectedCount == rmmCOUNT_ALLWAYS  ||
          entry->expectedCount == rmmCOUNT_MAYBE;
}


static inline
void rmMockListHandleNotFound(const char * functionName,
                              const char * parameterName,
                              const struct ryanmock_mock_entry * foundEntry,
                              const struct ryanmock_mock_entry * lastMatch,
                              const struct ryanmock_mock_entry * expectedMatch,
                              bool errorOnNotFound,
                              enum ryanmock_mock_entry_type type)
{
   if(foundEntry == NULL && errorOnNotFound)
   {
      bool isError = true;
      const char * typeName = rmMockEntryTypeGetName(type);
      if(parameterName == NULL)
      {
         if(type == eRMMET_return)
         {
            rmStreamWrite(&rmStreamMsg, "Return value for \"%s\" needed, but "
                                        "no matching mock Data Found",
                                        functionName);
         }
         else
         {
            rmStreamWrite(&rmStreamMsg, "Function \"%s\" called, but no "
                                        "matching mock Data Found",
                                        functionName);
         }
      }
      else
      {
         rmStreamWrite(&rmStreamMsg, "%s for parameter \"%s\" expected "
                                     "for function \"%s\", but no matching "
                                     "Data Found",
                                     typeName, parameterName, functionName);
      }
      
      if(lastMatch != NULL)
      {
         rmStreamEnsureNewLine(&rmStreamInfo);
         rmStreamWrite(&rmStreamInfo, "=== Previous Data Defined at ===");
         rmMockEntryWriteInfo(lastMatch);
         isError = false;
      }

      if(expectedMatch != NULL)
      {
         rmStreamEnsureNewLine(&rmStreamInfo);
         rmStreamWrite(&rmStreamInfo, "=== Expected Data Defined at ===");
         rmMockEntryWriteInfo(expectedMatch);
         isError = false;
      }

      if(isError)
      {
         rmTestError();
      }
      else
      {
         rmTestFailure();
      }

   }
}


static inline
struct ryanmock_mock_entry * rmMockListGetNextValidEntry(struct ryanmock_mock_list * list,
                                                         const char * functionName,
                                                         const char * parameterName,
                                                         bool errorOnNotFound,
                                                         enum ryanmock_mock_entry_type type)
{
   struct ryanmock_mock_entry * entries, *foundEntry, *lastMatch;
   size_t count, i;

   entries = rmMockListGet(list, &count);

   // Find first match to function/parameter
   foundEntry = NULL;
   lastMatch  = NULL;
   for(i = 0; i < count; i++)
   {
      struct ryanmock_mock_entry * entry = &entries[i];
      bool match = rmMockListIsValidEntry(entry, functionName,
                                          parameterName, type);
      if(match)
      {
         if(rmMockEntryHasMoreUsage(entry))
         {
            foundEntry = entry;
            break;
         }
         else
         {
            lastMatch = entry;
         }
      }
   }

   rmMockListHandleNotFound(functionName, parameterName, foundEntry,
                            lastMatch, NULL, errorOnNotFound, type);
   return foundEntry;
}

static inline
struct ryanmock_mock_entry * rmMockListGetFirstValidFunction(struct ryanmock_mock_list * list,
                                                             const char * functionName,
                                                             bool errorOnNotFound)
{
   struct ryanmock_mock_entry *entries, *foundEntry, *lastMatch,
                              *expectedMatch;
   size_t count, i;

   entries = rmMockListGet(list, &count);

   // Find first match to function/parameter
   foundEntry     = NULL;
   lastMatch      = NULL;
   expectedMatch  = NULL;
   for(i = 0; i < count; i++)
   {
      struct ryanmock_mock_entry * entry = &entries[i];
      if(entry->detail.type == eRMMET_functionCall)
      {
         struct ryanmock_mock_entry_function * function = &entry->detail.function;

         bool hasMoreUsage = rmMockEntryHasMoreUsage(entry);
         if(strcmp(entry->functionName, functionName) == 0)
         {
            if(hasMoreUsage)
            {
               foundEntry = entry;
               break;
            }
            else
            {
               lastMatch = entry;
            }
         }
         else if(!function->ignore && hasMoreUsage)
         {
            // We found a function that is not the one we are looking
            // for but it has more usages, so we are out of order.
            expectedMatch = entry;
            break;
         }
      }
   }

   rmMockListHandleNotFound(functionName, NULL, foundEntry, lastMatch, 
                            expectedMatch, errorOnNotFound, 
                            eRMMET_functionCall);
   return foundEntry;
}

void rmMock(const char * functionName, const char * parameter,
            void * value, size_t size, bool isArray,
            int line, const char * filename, 
            const char * thisFunctionName)
{
   struct ryanmock_mock_entry * foundEntry;
   const struct ryanmock_mock_mock_info * info;
   rmEnterFunction(thisFunctionName, line, filename);
   
   if(parameter == NULL)
   {
      foundEntry = rmMockListGetNextValidEntry(&rmMockList, functionName,
                                               NULL, true, eRMMET_return);
      info = &foundEntry->detail.ret.info;
   }
   else
   {
      foundEntry = rmMockListGetNextValidEntry(&rmMockList, functionName,
                                               parameter, true,
                                               eRMMET_parameterOutput);
      info = &foundEntry->detail.paramOut.info;

   }
   foundEntry->actualCount ++;
  
   rmTypeMockSizeCheck(info->destSizeOpCheck, info->valueType, size, 
                       info->value);
  
   if(isArray)
   {
      const void * ptr = info->value->data;
      memcpy(value, &ptr, sizeof(void*));
   }
   else
   {
      rmTypeDataCopy(info->valueType, value, size, info->value->data);
   }

   rmExitFunction(thisFunctionName, line, filename);
}

// Generic 2 and 3 value expect helper function

#define MAKE_EXPECT2_FUNCTION(TypeName, Type, OtherParams, CreateType, Ref,  \
                              DestSizeOpCheck)                               \
void rmExpect ## TypeName ## 2(enum ryanmock_comparison_operator2 op,        \
                               const char * parameter,                       \
                               Type value, const char * valueText,           \
                               OtherParams                                   \
                               int line, const char * filename,              \
                               const char * functionName)                    \
{                                                                            \
   struct ryanmock_type * type;                                              \
                                                                             \
   rmEnterFunction(functionName, line, filename);                            \
   type = CreateType;                                                        \
   rmExpect2(op,        type,                                                \
             parameter,                                                      \
             Ref value, valueText,                                           \
             DestSizeOpCheck);                                               \
   rmExitFunction(functionName, line, filename);                             \
}

#define MAKE_EXPECT3_FUNCTION(TypeName, Type, OtherParams, CreateType, Ref,  \
                              DestSizeOpCheck)                               \
void rmExpect ## TypeName ## 3(enum ryanmock_comparison_operator3 op,        \
                               const char * parameter,                       \
                               Type value1, const char * value1Text,         \
                               Type value2, const char * value2Text,         \
                               OtherParams                                   \
                               int line, const char * filename,              \
                               const char * functionName)                    \
{                                                                            \
   struct ryanmock_type * type;                                              \
                                                                             \
   rmEnterFunction(functionName, line, filename);                            \
   type = CreateType;                                                        \
   rmExpect3(op,         type,                                               \
             parameter,                                                      \
             Ref value1, value1Text,                                         \
             Ref value2, value2Text,                                         \
             DestSizeOpCheck);                                               \
   rmExitFunction(functionName, line, filename);                             \
}

static inline
void rmExpect2(enum ryanmock_comparison_operator2 op,
               struct ryanmock_type * type,
               const char * parameter,
               const void * value, const char * valueText,
               enum ryanmock_comparison_operator2 destSizeOpCheck)
{
   struct ryanmock_check * check;
   struct ryanmock_value_stored * expected;

   expected = rmTypeCreateStoredValueTypeSize(type, value, valueText);
   check = rmCreateCheck2(op, type, expected);
   (void)rmMockListAddParamCheck(&rmMockList, parameter, check, 
                                 destSizeOpCheck);
}

static inline
void rmExpect3(enum ryanmock_comparison_operator3 op,
               struct ryanmock_type * type,
               const char * parameter,
               const void * value1, const char * value1Text,
               const void * value2, const char * value2Text,
               enum ryanmock_comparison_operator2 destSizeOpCheck)
{
   struct ryanmock_check * check;
   struct ryanmock_value_stored * expected1, * expected2;

   expected1 = rmTypeCreateStoredValueTypeSize(type, value1, value1Text);
   expected2 = rmTypeCreateStoredValueTypeSize(type, value2, value2Text);
   check = rmCreateCheck3(op, type, expected1, expected2);
   (void)rmMockListAddParamCheck(&rmMockList, parameter, check, 
                                 destSizeOpCheck);
}



// Expected Functions
void rmExpectAny(const char * parameter,
                 int line, const char * filename,
                 const char * functionName)
{
   struct ryanmock_check * check;
   rmEnterFunction(functionName, line, filename);
   check = rmCreateCheckAlwaysPass();
   (void)rmMockListAddParamCheck(&rmMockList, parameter, check, eRMCO2_NotApplicable);
   rmExitFunction(functionName, line, filename);
}

void rmExpectAssert(const char * parameter, bool expected,
                    int line, const char * filename, const char * functionName)
{
   struct ryanmock_check * check;
   rmEnterFunction(functionName, line, filename);
   check = rmCreateCheckBoolean(expected);
   (void)rmMockListAddParamCheck(&rmMockList, parameter, check, eRMCO2_GreaterThanOrEqualTo);
   rmExitFunction(functionName, line, filename);
}

void rmExpectCallback(const char * parameter,
                      rmcheck_function callbackFunction,
                      const char * callbackFunctionName,
                      void * userData,
                      int line, const char * filename,
                      const char * functionName)
{
   struct ryanmock_check * check;
   rmEnterFunction(functionName, line, filename);
   
   check = rmCreateCheckCallback(callbackFunction, 
                                 callbackFunctionName, 
                                 userData);
   (void)rmMockListAddParamCheck(&rmMockList, parameter, check, eRMCO2_NotApplicable);
   
   rmExitFunction(functionName, line, filename);

}



void rmExpectPtrNULL(const char * parameter, bool shouldBeNULL,
                     int line, const char * filename, const char * functionName)
{
   struct ryanmock_check * check;
   rmEnterFunction(functionName, line, filename);
   check = rmCreateCheckNULL(shouldBeNULL);
   (void)rmMockListAddParamCheck(&rmMockList, parameter, check, eRMCO2_Equal);
   rmExitFunction(functionName, line, filename);
}

// Param Checks
void rmParamCheck(const char * functionName, const char * parameterName,
                  const void * data, const char * valueText, 
                  size_t elementSize, size_t elementCount, bool isArray,
                  int line, const char * filename,
                  const char * thisFunctionName)
{
   struct ryanmock_value value;
   struct ryanmock_mock_entry * foundEntry;
   struct ryanmock_check * check;
   const void * addrToCheck;
   size_t size;
   bool dereferenceParameter;
   rmEnterFunction(thisFunctionName, line, filename);

   foundEntry = rmMockListGetNextValidEntry(&rmMockList, functionName,
                                            parameterName, true,
                                            eRMMET_parameterCheck);

   foundEntry->actualCount ++;
   check = foundEntry->detail.paramCheck.check;
   dereferenceParameter = foundEntry->detail.paramCheck.check->type->vt->dereferenceParameter;

   
   rmCheckTake(check);

   
   if(dereferenceParameter)
   {
      addrToCheck = *(const void * const *)data;
   }
   else
   {
      addrToCheck = data;
   }
   
   if(dereferenceParameter)
   {
      size = elementSize * elementCount;
   }
   else if(isArray)
   {
      size = sizeof(void*);
   }
   else
   {
      size = elementSize;
   }
   

   
   rmValueInit(&value, addrToCheck, valueText, size);
   
   rmDoCheck(check, &value, foundEntry->detail.paramCheck.destSizeOpCheck);

   rmExitFunction(thisFunctionName, line, filename);
}

void rmFunctionCalled(const char * functionName,
                      int line, const char * filename,
                      const char * thisFunctionName)
{
   struct ryanmock_mock_entry * foundEntry;
   rmEnterFunction(thisFunctionName, line, filename);


   foundEntry = rmMockListGetFirstValidFunction(&rmMockList, functionName,
                                                true);

   foundEntry->actualCount ++;

   rmExitFunction(thisFunctionName, line, filename);
}

// Check Create functions
static inline
struct ryanmock_check * rmCreateCheckWithSpace(const struct ryanmock_check_vt * vt,
                                               struct ryanmock_type * type,
                                               size_t size)
{
   struct ryanmock_check * check = malloc(sizeof(struct ryanmock_check) + size);
   check->vt       = vt;
   check->type     = type;
   check->parent   = NULL;
   check->refCount = 1;
   return check;
}


struct ryanmock_check * rmCreateCheck2(enum ryanmock_comparison_operator2 op,
                                       struct ryanmock_type * type,
                                       struct ryanmock_value_stored * expected)

{
   struct ryanmock_check * check = rmCreateCheckWithSpace(&rmCheckVT2, type,
                                                          sizeof(struct ryanmock_check_data_expected2));
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   expectedPtr->op       = op;
   expectedPtr->expected = expected;
   return check;
}

struct ryanmock_check * rmCreateCheckBoolean(bool expected)
{
   struct ryanmock_type * type = rmCreateTypeBoolean();
   struct ryanmock_check * check = rmCreateCheckWithSpace(&rmCheckVT_Boolean, type,
                                                          sizeof(struct ryanmock_check_data_expected2));
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   expectedPtr->op       = eRMCO2_NotApplicable;
   expectedPtr->expected = rmTypeCreateStoredValueTypeSize(type, &expected, NULL);
   return check;
}

struct ryanmock_check * rmCreateCheckNULL(bool shouldBeNULL)
{
   struct ryanmock_type * ptrType = rmCreateTypePointer();
   struct ryanmock_type * boolType = rmCreateTypeBoolean();
   struct ryanmock_check * check = rmCreateCheckWithSpace(&rmCheckVT_NULL, ptrType,
                                                          sizeof(struct ryanmock_check_data_expected2));
   DECLARE_CAST(struct ryanmock_check_data_expected2, expectedPtr, check->data);
   expectedPtr->op       = eRMCO2_NotApplicable;
   expectedPtr->expected = rmTypeCreateStoredValueTypeSize(boolType, &shouldBeNULL, NULL);
   rmTypeRelease(boolType);
   return check;
}


struct ryanmock_check * rmCreateCheck3(enum ryanmock_comparison_operator3 op,
                                       struct ryanmock_type * type,
                                       struct ryanmock_value_stored * expected1,
                                       struct ryanmock_value_stored * expected2)
{
   struct ryanmock_check * check = rmCreateCheckWithSpace(&rmCheckVT3, type,
                                                          sizeof(struct ryanmock_check_data_expected3));
   DECLARE_CAST(struct ryanmock_check_data_expected3, expectedPtr, check->data);
   expectedPtr->op        = op;
   expectedPtr->expected1 = expected1;
   expectedPtr->expected2 = expected2;
   return check;
}

struct ryanmock_check * rmCreateCheckAlwaysPass(void)

{
   // Type doesn't matter because the check will always pass
   struct ryanmock_type * type = rmCreateTypePointer();
   struct ryanmock_check * check = rmCreateCheckWithSpace(&rmCheckVT_AlwaysPass, type,
                                                          sizeof(struct ryanmock_check_data_always_pass));
   DECLARE_CAST(struct ryanmock_check_data_always_pass,
                expectedPtr, check->data);

   rmStackCopyDeep(&expectedPtr->stack, &rmStack);
   return check;
}

struct ryanmock_check * rmCreateCheckCallback(rmcheck_function callback, 
                                              const char * callbackName, 
                                              void * userData)

{
   // Type doesn't matter because the check will always pass
   struct ryanmock_type * type = rmCreateTypePointer();
   struct ryanmock_check * check = rmCreateCheckWithSpace(&rmCheckVT_Callback, type,
                                                          sizeof(struct ryanmock_check_data_callback));
   DECLARE_CAST(struct ryanmock_check_data_callback,
                expectedPtr, check->data);

   rmStackCopyDeep(&expectedPtr->userDataStack, &rmStack);
   expectedPtr->callback     = callback;
   expectedPtr->callbackName = callbackName;
   expectedPtr->userData     = userData;
   return check;
}

// Symbol Table

static inline
bool rmSymbolTableLookup(const struct ryanmock_symbol * table, 
                         void ** dest,
                         const char * symbolName)
{
   size_t index = 0;
   while(table[index].name != NULL)
   {
      if(strcmp(table[index].name, symbolName) == 0)
      {
         if(table[index].isFunc)
         {
            void (**funcDest)(void) = (void (**)(void))dest;
            (*funcDest) = table[index].sym.func;
         }
         else
         {
            (*dest) = table[index].sym.ptr;
         }
         return true;
      }
      index ++;
   }
   return false;
}

void rmSymbolTableLink(void ** dest, 
                       const char * symbolName,
                       const struct ryanmock_symbol * table, 
                       const char * tableName,
                       int line, const char * filename,
                       const char * functionName)
{
   bool found;
   rmEnterFunction(functionName, line, filename);
   found = rmSymbolTableLookup(table, dest, symbolName);
   if(!found)
   {
      rmStreamWrite(&rmStreamMsg, "Failed to find Symbol \"%s\" in module \"%s\"!",
                                  symbolName, tableName);
      rmTestError();
   }
   rmExitFunction(functionName, line, filename);
}

// =========================== Generated Functions ========================== //

// int
MAKE_ASSERT2_FUNCTION(    Int, int, NOPARAMS, rmCreateTypeIntegerInt(), REF)
MAKE_ASSERT3_FUNCTION(    Int, int, NOPARAMS, rmCreateTypeIntegerInt(), REF)
MAKE_EXPECT2_FUNCTION(    Int, int, NOPARAMS, rmCreateTypeIntegerInt(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Int, int, NOPARAMS, rmCreateTypeIntegerInt(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Int, int, NOPARAMS, rmCreateTypeIntegerInt(), REF, eRMCO2_Equal)

// unsigned int
MAKE_ASSERT2_FUNCTION(    UInt, unsigned int, NOPARAMS, rmCreateTypeIntegerUInt(), REF)
MAKE_ASSERT3_FUNCTION(    UInt, unsigned int, NOPARAMS, rmCreateTypeIntegerUInt(), REF)
MAKE_EXPECT2_FUNCTION(    UInt, unsigned int, NOPARAMS, rmCreateTypeIntegerUInt(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    UInt, unsigned int, NOPARAMS, rmCreateTypeIntegerUInt(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(UInt, unsigned int, NOPARAMS, rmCreateTypeIntegerUInt(), REF, eRMCO2_Equal)

// Enumerations
MAKE_ASSERT2_FUNCTION(    Enum, rmuintmax_t, NOPARAMS, rmCreateTypeIntegerUnsignedMax(), REF)
MAKE_ASSERT3_FUNCTION(    Enum, rmuintmax_t, NOPARAMS, rmCreateTypeIntegerUnsignedMax(), REF)
MAKE_EXPECT2_FUNCTION(    Enum, rmuintmax_t, NOPARAMS, rmCreateTypeIntegerUnsignedMax(), REF, eRMCO2_LessThanOrEqualTo)
MAKE_EXPECT3_FUNCTION(    Enum, rmuintmax_t, NOPARAMS, rmCreateTypeIntegerUnsignedMax(), REF, eRMCO2_LessThanOrEqualTo)
MAKE_WILL_RETURN_FUNCTION(Enum, rmuintmax_t, NOPARAMS, rmCreateTypeIntegerUnsignedMax(), REF, eRMCO2_LessThanOrEqualTo)

// unsigned long
MAKE_ASSERT2_FUNCTION(    ULong, unsigned long, NOPARAMS, rmCreateTypeIntegerULong(), REF)
MAKE_ASSERT3_FUNCTION(    ULong, unsigned long, NOPARAMS, rmCreateTypeIntegerULong(), REF)
MAKE_EXPECT2_FUNCTION(    ULong, unsigned long, NOPARAMS, rmCreateTypeIntegerULong(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    ULong, unsigned long, NOPARAMS, rmCreateTypeIntegerULong(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(ULong, unsigned long, NOPARAMS, rmCreateTypeIntegerULong(), REF, eRMCO2_Equal)

// unsigned short
MAKE_ASSERT2_FUNCTION(    UShort, unsigned short, NOPARAMS, rmCreateTypeIntegerUShort(), REF)
MAKE_ASSERT3_FUNCTION(    UShort, unsigned short, NOPARAMS, rmCreateTypeIntegerUShort(), REF)
MAKE_EXPECT2_FUNCTION(    UShort, unsigned short, NOPARAMS, rmCreateTypeIntegerUShort(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    UShort, unsigned short, NOPARAMS, rmCreateTypeIntegerUShort(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(UShort, unsigned short, NOPARAMS, rmCreateTypeIntegerUShort(), REF, eRMCO2_Equal)

// long
MAKE_ASSERT2_FUNCTION(    Long, long, NOPARAMS, rmCreateTypeIntegerLong(), REF)
MAKE_ASSERT3_FUNCTION(    Long, long, NOPARAMS, rmCreateTypeIntegerLong(), REF)
MAKE_EXPECT2_FUNCTION(    Long, long, NOPARAMS, rmCreateTypeIntegerLong(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Long, long, NOPARAMS, rmCreateTypeIntegerLong(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Long, long, NOPARAMS, rmCreateTypeIntegerLong(), REF, eRMCO2_Equal)

// short
MAKE_ASSERT2_FUNCTION(    Short, short, NOPARAMS, rmCreateTypeIntegerShort(), REF)
MAKE_ASSERT3_FUNCTION(    Short, short, NOPARAMS, rmCreateTypeIntegerShort(), REF)
MAKE_EXPECT2_FUNCTION(    Short, short, NOPARAMS, rmCreateTypeIntegerShort(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Short, short, NOPARAMS, rmCreateTypeIntegerShort(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Short, short, NOPARAMS, rmCreateTypeIntegerShort(), REF, eRMCO2_Equal)

// uint8_t
MAKE_ASSERT2_FUNCTION(    IntU8, uint8_t, NOPARAMS, rmCreateTypeIntegerU8(), REF)
MAKE_ASSERT3_FUNCTION(    IntU8, uint8_t, NOPARAMS, rmCreateTypeIntegerU8(), REF)
MAKE_EXPECT2_FUNCTION(    IntU8, uint8_t, NOPARAMS, rmCreateTypeIntegerU8(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    IntU8, uint8_t, NOPARAMS, rmCreateTypeIntegerU8(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(IntU8, uint8_t, NOPARAMS, rmCreateTypeIntegerU8(), REF, eRMCO2_Equal)

// uint16_t
MAKE_ASSERT2_FUNCTION(    IntU16, uint16_t, NOPARAMS, rmCreateTypeIntegerU16(), REF)
MAKE_ASSERT3_FUNCTION(    IntU16, uint16_t, NOPARAMS, rmCreateTypeIntegerU16(), REF)
MAKE_EXPECT2_FUNCTION(    IntU16, uint16_t, NOPARAMS, rmCreateTypeIntegerU16(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    IntU16, uint16_t, NOPARAMS, rmCreateTypeIntegerU16(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(IntU16, uint16_t, NOPARAMS, rmCreateTypeIntegerU16(), REF, eRMCO2_Equal)

// uint32_t
MAKE_ASSERT2_FUNCTION(    IntU32, uint32_t, NOPARAMS, rmCreateTypeIntegerU32(), REF)
MAKE_ASSERT3_FUNCTION(    IntU32, uint32_t, NOPARAMS, rmCreateTypeIntegerU32(), REF)
MAKE_EXPECT2_FUNCTION(    IntU32, uint32_t, NOPARAMS, rmCreateTypeIntegerU32(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    IntU32, uint32_t, NOPARAMS, rmCreateTypeIntegerU32(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(IntU32, uint32_t, NOPARAMS, rmCreateTypeIntegerU32(), REF, eRMCO2_Equal)

// uint64_t
#ifdef UINT64_MAX
MAKE_ASSERT2_FUNCTION(    IntU64, uint64_t, NOPARAMS, rmCreateTypeIntegerU64(), REF)
MAKE_ASSERT3_FUNCTION(    IntU64, uint64_t, NOPARAMS, rmCreateTypeIntegerU64(), REF)
MAKE_EXPECT2_FUNCTION(    IntU64, uint64_t, NOPARAMS, rmCreateTypeIntegerU64(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    IntU64, uint64_t, NOPARAMS, rmCreateTypeIntegerU64(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(IntU64, uint64_t, NOPARAMS, rmCreateTypeIntegerU64(), REF, eRMCO2_Equal)
#endif // UINT64_MAX

// int8_t
MAKE_ASSERT2_FUNCTION(    Int8, int8_t, NOPARAMS, rmCreateTypeIntegerS8(), REF)
MAKE_ASSERT3_FUNCTION(    Int8, int8_t, NOPARAMS, rmCreateTypeIntegerS8(), REF)
MAKE_EXPECT2_FUNCTION(    Int8, int8_t, NOPARAMS, rmCreateTypeIntegerS8(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Int8, int8_t, NOPARAMS, rmCreateTypeIntegerS8(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Int8, int8_t, NOPARAMS, rmCreateTypeIntegerS8(), REF, eRMCO2_Equal)

// int16_t
MAKE_ASSERT2_FUNCTION(    Int16, int16_t, NOPARAMS, rmCreateTypeIntegerS16(), REF)
MAKE_ASSERT3_FUNCTION(    Int16, int16_t, NOPARAMS, rmCreateTypeIntegerS16(), REF)
MAKE_EXPECT2_FUNCTION(    Int16, int16_t, NOPARAMS, rmCreateTypeIntegerS16(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Int16, int16_t, NOPARAMS, rmCreateTypeIntegerS16(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Int16, int16_t, NOPARAMS, rmCreateTypeIntegerS16(), REF, eRMCO2_Equal)

// int32_t
MAKE_ASSERT2_FUNCTION(    Int32, int32_t, NOPARAMS, rmCreateTypeIntegerS32(), REF)
MAKE_ASSERT3_FUNCTION(    Int32, int32_t, NOPARAMS, rmCreateTypeIntegerS32(), REF)
MAKE_EXPECT2_FUNCTION(    Int32, int32_t, NOPARAMS, rmCreateTypeIntegerS32(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Int32, int32_t, NOPARAMS, rmCreateTypeIntegerS32(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Int32, int32_t, NOPARAMS, rmCreateTypeIntegerS32(), REF, eRMCO2_Equal)

// int64_t
#ifdef UINT64_MAX
MAKE_ASSERT2_FUNCTION(    Int64, int64_t, NOPARAMS, rmCreateTypeIntegerS64(), REF)
MAKE_ASSERT3_FUNCTION(    Int64, int64_t, NOPARAMS, rmCreateTypeIntegerS64(), REF)
MAKE_EXPECT2_FUNCTION(    Int64, int64_t, NOPARAMS, rmCreateTypeIntegerS64(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Int64, int64_t, NOPARAMS, rmCreateTypeIntegerS64(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Int64, int64_t, NOPARAMS, rmCreateTypeIntegerS64(), REF, eRMCO2_Equal)
#endif // UINT64_MAX

// size_t
MAKE_ASSERT2_FUNCTION(    SizeT, size_t, NOPARAMS, rmCreateTypeIntegerSizeT(), REF)
MAKE_ASSERT3_FUNCTION(    SizeT, size_t, NOPARAMS, rmCreateTypeIntegerSizeT(), REF)
MAKE_EXPECT2_FUNCTION(    SizeT, size_t, NOPARAMS, rmCreateTypeIntegerSizeT(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    SizeT, size_t, NOPARAMS, rmCreateTypeIntegerSizeT(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(SizeT, size_t, NOPARAMS, rmCreateTypeIntegerSizeT(), REF, eRMCO2_Equal)


// bool
MAKE_ASSERT2_FUNCTION(    Bool, bool, NOPARAMS, rmCreateTypeBoolean(), REF)
MAKE_ASSERT3_FUNCTION(    Bool, bool, NOPARAMS, rmCreateTypeBoolean(), REF)
MAKE_EXPECT2_FUNCTION(    Bool, bool, NOPARAMS, rmCreateTypeBoolean(), REF, eRMCO2_LessThanOrEqualTo)
MAKE_EXPECT3_FUNCTION(    Bool, bool, NOPARAMS, rmCreateTypeBoolean(), REF, eRMCO2_LessThanOrEqualTo)
MAKE_WILL_RETURN_FUNCTION(Bool, bool, NOPARAMS, rmCreateTypeBoolean(), REF, eRMCO2_Equal)


// double
MAKE_ASSERT2_FUNCTION(    Double, double, NOPARAMS, rmCreateTypeDouble(), REF)
MAKE_ASSERT3_FUNCTION(    Double, double, NOPARAMS, rmCreateTypeDouble(), REF)
MAKE_EXPECT2_FUNCTION(    Double, double, NOPARAMS, rmCreateTypeDouble(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Double, double, NOPARAMS, rmCreateTypeDouble(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Double, double, NOPARAMS, rmCreateTypeDouble(), REF, eRMCO2_Equal)

// float
MAKE_ASSERT2_FUNCTION(    Float, float, NOPARAMS, rmCreateTypeFloat(), REF)
MAKE_ASSERT3_FUNCTION(    Float, float, NOPARAMS, rmCreateTypeFloat(), REF)
MAKE_EXPECT2_FUNCTION(    Float, float, NOPARAMS, rmCreateTypeFloat(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Float, float, NOPARAMS, rmCreateTypeFloat(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Float, float, NOPARAMS, rmCreateTypeFloat(), REF, eRMCO2_Equal)

// Pointers
MAKE_ASSERT2_FUNCTION(    Ptr, const void *, NOPARAMS, rmCreateTypePointer(), REF)
MAKE_ASSERT3_FUNCTION(    Ptr, const void *, NOPARAMS, rmCreateTypePointer(), REF)
MAKE_EXPECT2_FUNCTION(    Ptr, const void *, NOPARAMS, rmCreateTypePointer(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Ptr, const void *, NOPARAMS, rmCreateTypePointer(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Ptr, const void *, NOPARAMS, rmCreateTypePointer(), REF, eRMCO2_Equal)

// uintptr_t
MAKE_ASSERT2_FUNCTION(    IntPtrU, uintptr_t, NOPARAMS, rmCreateTypeIntegerUIntPtr(), REF)
MAKE_ASSERT3_FUNCTION(    IntPtrU, uintptr_t, NOPARAMS, rmCreateTypeIntegerUIntPtr(), REF)
MAKE_EXPECT2_FUNCTION(    IntPtrU, uintptr_t, NOPARAMS, rmCreateTypeIntegerUIntPtr(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    IntPtrU, uintptr_t, NOPARAMS, rmCreateTypeIntegerUIntPtr(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(IntPtrU, uintptr_t, NOPARAMS, rmCreateTypeIntegerUIntPtr(), REF, eRMCO2_Equal)

// intptr_t
MAKE_ASSERT2_FUNCTION(    IntPtr, intptr_t, NOPARAMS, rmCreateTypeIntegerIntPtr(), REF)
MAKE_ASSERT3_FUNCTION(    IntPtr, intptr_t, NOPARAMS, rmCreateTypeIntegerIntPtr(), REF)
MAKE_EXPECT2_FUNCTION(    IntPtr, intptr_t, NOPARAMS, rmCreateTypeIntegerIntPtr(), REF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    IntPtr, intptr_t, NOPARAMS, rmCreateTypeIntegerIntPtr(), REF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(IntPtr, intptr_t, NOPARAMS, rmCreateTypeIntegerIntPtr(), REF, eRMCO2_Equal)

// Memory
MAKE_ASSERT2_FUNCTION(    Memory, const void *,
                          size_t elementSize COMMA size_t count COMMA,
                          rmCreateTypeMemory(elementSize, count), NOREF)
MAKE_ASSERT3_FUNCTION(    Memory, const void *,
                          size_t elementSize COMMA size_t count COMMA,
                          rmCreateTypeMemory(elementSize, count), NOREF)
MAKE_EXPECT2_FUNCTION(    Memory, const void *,
                          size_t elementSize COMMA size_t count COMMA,
                          rmCreateTypeMemory(elementSize, count), NOREF, eRMCO2_Equal)
MAKE_EXPECT3_FUNCTION(    Memory, const void *,
                          size_t elementSize COMMA size_t count COMMA,
                          rmCreateTypeMemory(elementSize, count), NOREF, eRMCO2_Equal)
MAKE_WILL_RETURN_FUNCTION(Memory, const void *,
                          size_t elementSize COMMA size_t count COMMA,
                          rmCreateTypeMemory(elementSize, count), NOREF, eRMCO2_GreaterThanOrEqualTo)

// C Strings
MAKE_ASSERT2_FUNCTION(    String, const char *, NOPARAMS,
                          rmCreateTypeString(strlen(expected) COMMA true),  NOREF)
MAKE_ASSERT3_FUNCTION(    String, const char *, NOPARAMS,
                          rmCreateTypeString(strlen(expected1) COMMA true), NOREF)
MAKE_EXPECT2_FUNCTION(    String, const char *, NOPARAMS,
                          rmCreateTypeString(strlen(value) COMMA true), NOREF, eRMCO2_NotApplicable)
MAKE_EXPECT3_FUNCTION(    String, const char *, NOPARAMS,
                          rmCreateTypeString(strlen(value1) COMMA true), NOREF, eRMCO2_NotApplicable)
MAKE_WILL_RETURN_FUNCTION(String, const char *, NOPARAMS,
                          rmCreateTypeString(strlen(value) COMMA true), NOREF, eRMCO2_Equal)

