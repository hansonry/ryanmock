#!/usr/bin/env python3

import sys

class Action:
   def __init__(self, name, text):
      self.name = name
      self.text = text

class DoxygenParam:
   def __init__(self, name, description):
      self.name = name
      self.description = description

class Type:
   def __init__(self, signature, title, text,
                extraParams = "", defineParams = "", callParams = "",
                preProcessorCheck = "" ):
      self.signature         = signature
      self.title             = title
      self.text              = text
      self.extraParams       = extraParams
      self.defineParams      = defineParams
      self.callParams        = callParams
      self.preProcessorCheck = preProcessorCheck

TypeTable = [
   Type("bool",           "Bool",    "boolean"),
   Type("unsigned int",   "UInt",    "unsigned int"),
   Type("unsigned long",  "ULong",   "unsigned long"),
   Type("unsigned short", "UShort",  "unsigned short"),
   Type("int",            "Int",     "int"),
   Type("long",           "Long",    "long"),
   Type("short",          "Short",   "short"),
   Type("uint8_t",        "IntU8",   "unsigned 8 bit integer"),
   Type("uint16_t",       "IntU16",  "unsigned 16 bit integer"),
   Type("uint32_t",       "IntU32",  "unsigned 32 bit integer"),
   Type("uint64_t",       "IntU64",  "unsigned 64 bit integer", "", "", "",
        "#ifdef UINT64_MAX"),
   Type("uintptr_t",      "IntPtrU", "pointer sized unsigned integer"),
   Type("int8_t",         "Int8",    "8 bit integer"),
   Type("int16_t",        "Int16",   "16 bit integer"),
   Type("int32_t",        "Int32",   "32 bit integer"),
   Type("int64_t",        "Int64",   "64 bit integer", "", "", "",
        "#ifdef UINT64_MAX"),
   Type("intptr_t",       "IntPtr",  "pointer sized integer"),
   Type("size_t",         "SizeT",   "memory size"),
   Type("rmuintmax_t",    "Enum",    "enumeration"),
   Type("const void *",   "Ptr",     "pointer"),
   Type("double",         "Double",  "double"),
   Type("float",          "Float",   "float"),
   Type("const void *",   "Memory",  "memory",
        "size_t elementSize, size_t count", "count", "sizeof(*(%1)), count"),
   Type("const char *",   "String",  "string"),
]

Action2Table = [
   Action("Equal",                "equal to"),
   Action("NotEqual",             "not equal to"),
   Action("GreaterThan",          "greater than"),
   Action("GreaterThanOrEqualTo", "greater than or equal to"),
   Action("LessThan",             "less than"),
   Action("LessThanOrEqualTo",    "less than or equal to"),
]


def ComputeMaxParamNameLength(params):
   maxLength = 0
   for param in params:
      length = len(param.name)
      if length > maxLength:
         maxLength = length
   return maxLength


def PrintWordsStopBefore(fh, words, stopBefore):
   if len(words) <= stopBefore:
      fh.write(f"{words}\n")
      return ""

   lastSpace = words.rfind(' ', 0, stopBefore)
   fh.write(f"{words[:lastSpace]}\n")
   return words[lastSpace:].strip()

def PrintWrappedCommentLines(fh, lines, wrapLineStart):
   indentText = ' ' * wrapLineStart
   for line in lines.split('\n'):
      isFirstLine = True
      while line:
         if isFirstLine:
            line = f" * {line}"
            isFirstLine = False
         else:
            line = f" * {indentText}{line}"

         line = PrintWordsStopBefore(fh, line, 80)


def PrintFunction(fh, functionName, params, lineEnding = "", functionEnding = ""):
   paramLines = params.split("\n")
   startOfFunction = f"{functionName}("
   whitespace = " " * len(startOfFunction)
   fh.write(f"{startOfFunction}{paramLines[0]}{lineEnding}\n")
   for paramLine in paramLines[1:-1]:
      fh.write(f"{whitespace}{paramLine}{lineEnding}\n")
   fh.write(f"{whitespace}{paramLines[-1]}){functionEnding}\n")

def PrintDoxygenComment(fh, briefText, descriptionText, params, returnText):
   fh.write("/**\n")
   PrintWrappedCommentLines(fh, f"@brief {briefText}", 7)
   fh.write(" * \n")
   if descriptionText:
      PrintWrappedCommentLines(fh, f"{descriptionText}", 0)
      fh.write(" * \n")
   maxParamNameLength = ComputeMaxParamNameLength(params)
   parameterWhiteSpace = maxParamNameLength + 8
   for param in params:
      diff = maxParamNameLength - len(param.name)
      trailingWhitespace = ' ' * diff
      PrintWrappedCommentLines(fh, f"@param {param.name}{trailingWhitespace} "
                               f"{param.description}",
                               parameterWhiteSpace)
   if returnText:
      PrintWrappedCommentLines(fh, f"@return {returnText}", parameterWhiteSpace)

   fh.write(" */\n")


def PrintGroupStart(fh, name, short, description):
   fh.write("/**\n")
   wrapLineStart = len(name) + 11
   PrintWrappedCommentLines(fh, f"@defgroup {name} {short}",
                            wrapLineStart)
   if description:
      fh.write(" * \n")
      PrintWrappedCommentLines(fh, description, 0)
      fh.write(" * \n")
   fh.write(" * @{\n")
   fh.write(" */\n\n")

def PrintGroupEnd(fh, name):
   fh.write("/**@}*/ " f"// End of {name}\n\n")

def PrintGenericMacro(fh, t, isExpect, macroName, functionName, actionEnum, params):
   startOfFunctionCall = f"{functionName}({actionEnum}, "


   macroParamList = ", ".join(params)
   if t.defineParams:
      macroParamList = f"{macroParamList}, {t.defineParams}"


   funcParamList = ""

   if isExpect:
      macroParamList = f"parameter, {macroParamList}"
      startOfFunctionCall = f"{startOfFunctionCall}#parameter, "
   else:
      macroParamList = f"actual, {macroParamList}"
      funcParamList = f"actual, #actual, \\\n"

   whiteSpace = " " * len(startOfFunctionCall)

   for param in params:
      if funcParamList:
         funcParamList = f"{funcParamList}{whiteSpace}"
      funcParamList = f"{funcParamList}{param}, #{param}, \\\n"
   if t.callParams:
      modifiedCallParams = t.callParams.replace("%1", params[0])
      funcParamList = f"{funcParamList}{whiteSpace}{modifiedCallParams}, \\\n"



   fh.write(
f"#define {macroName}({macroParamList}) \\\n"
f"{startOfFunctionCall}{funcParamList}"
f"{whiteSpace}__LINE__, __FILE__,  \\\n"
f"{whiteSpace}\"{macroName}\")\n\n"
)

def PrintAssert2Macro(fh, t, actionName, actionText):
   typeAction = f"{t.title}{actionName}"
   whiteSpace = " " * (len(t.title) + len(actionName))
   PrintDoxygenComment(fh, f"Verify the {t.text} @p actual is {actionText} the @p expected.\n",
                       "",
                       [ DoxygenParam("actual",   f"The value to check."),
                         DoxygenParam("expected", f"The value that the @p actual needs to be {actionText} for a pass.")],
                       "")
   PrintGenericMacro(fh, t, False,
                     f"rmmAssert{typeAction}",
                     f"rmAssert{t.title}2",
                     f"eRMCO2_{actionName}",
                     [ "expected" ])


def PrintAssert3Between(fh, t, negate):
   actionName = "Between"
   if negate:
      actionName = f"Not{actionName}"
   typeAction = f"{t.title}{actionName}"
   whiteSpace = " " * (len(t.title) + len(actionName))
   if negate:
      PrintDoxygenComment(fh, f"Verify the {t.text} @p actual is not between the @p min and @p max.",
                          f"The minimum and maximum are inclusive.",
                          [ DoxygenParam("actual", f"The value to check."),
                            DoxygenParam("min",    f"The minimum value the @p actual needs to be below for a pass."),
                            DoxygenParam("max",    f"The maximum value the @p actual needs to be above for a pass.") ],
                          f"")
   else:
      PrintDoxygenComment(fh, f"Verify the {t.text} @p actual is between the @p min and @p max.",
                          f"The minimum and maximum are inclusive.",
                          [ DoxygenParam("actual", f"The value to check."),
                            DoxygenParam("min",    f"The minimum value the @p actual needs to be for a pass."),
                            DoxygenParam("max",    f"The maximum value the @p actual needs to be for a pass.") ],
                          f"")
   PrintGenericMacro(fh, t, False,
                     f"rmmAssert{typeAction}",
                     f"rmAssert{t.title}3",
                     f"eRMCO3_{actionName}",
                     [ "min", "max" ])


def PrintAssert3Within(fh, t, negate):
   actionName = "Within"
   if negate:
      actionName = f"Not{actionName}"
   typeAction = f"{t.title}{actionName}"
   whiteSpace = " " * (len(t.title) + len(actionName))
   if negate:
      PrintDoxygenComment(fh, f"Verify that the signed int @p actual is not approximately @p expected.",
                          f"The equation used is abs(@p actual - @p expected) > @p epsilon",
                          [ DoxygenParam("actual",   f"The value to check."),
                            DoxygenParam("expected", f"The value we expect the @p actual to not approximately be."),
                            DoxygenParam("epsilon",  f"The error allowed on the approximation.") ],
                          f"")
   else:
      PrintDoxygenComment(fh, f"Verify that the signed int @p actual is approximately @p expected.",
                          f"The equation used is abs(@p actual - @p expected) > @p epsilon",
                          [ DoxygenParam("actual",   f"The value to check."),
                            DoxygenParam("expected", f"The value we expect the @p actual to approximately be."),
                            DoxygenParam("epsilon",  f"The error allowed on the approximation.") ],
                          f"")
   PrintGenericMacro(fh, t, False,
                     f"rmmAssert{typeAction}",
                     f"rmAssert{t.title}3",
                     f"eRMCO3_{actionName}",
                     [ "expected", "epsilon" ])



def PrintAssert2FunctionPrototype(fh, t):
   whiteSpace = " " * len(t.title)
   fh.write(
f"void rmAssert{t.title}2(enum ryanmock_comparison_operator2 op,\n"
f"             {whiteSpace}  {t.signature} actual,   const char * actualText,\n"
f"             {whiteSpace}  {t.signature} expected, const char * expectedText,\n"
)
   if t.extraParams:
      fh.write(
f"             {whiteSpace}  {t.extraParams},\n"
)
   fh.write(
f"             {whiteSpace}  int line,     const char * filename,\n"
f"             {whiteSpace}  const char * functionName);\n\n"
)

def PrintAssert3FunctionPrototype(fh, t):
   whiteSpace = " " * len(t.title)
   fh.write(
f"void rmAssert{t.title}3(enum ryanmock_comparison_operator3 op,\n"
f"             {whiteSpace}  {t.signature} actual,    const char * actualText,\n"
f"             {whiteSpace}  {t.signature} expected1, const char * expectedText1,\n"
f"             {whiteSpace}  {t.signature} expected2, const char * expectedText2,\n"
)
   if t.extraParams:
      fh.write(
f"             {whiteSpace}  {t.extraParams},\n"
)
   fh.write(
f"             {whiteSpace}  int line,      const char * filename,\n"
f"             {whiteSpace}  const char * functionName);\n\n"
)

def PrintGenericWillReturnMacro(fh, t, isOutput, macroName, functionName, params = []):
   callParams = ""
   if t.callParams:
      modifiedCallParams = t.callParams.replace("%1", "value")
      callParams = f"{modifiedCallParams},\n"
   defineParams = ""
   if t.defineParams:
      defineParams = f", {t.defineParams}"
   if isOutput:
      PrintDoxygenComment(fh, "Make the Mocked function set an output parameter.", 
                          "@see rmmMockParam",
                          [ DoxygenParam("param", "The parameter to set."),
                            DoxygenParam("value", "The value to set") ],
                          "")
      fh.write(f"#define {macroName}(param, value{defineParams}) \\\n")
      PrintFunction(fh, f"rmMockOutput{t.title}",
                    f"(value), #value, #param,\n"
                    f"{callParams}"
                    f"__LINE__, __FILE__,\n"
                    f"\"{macroName}\"", " \\")
   else:
      PrintDoxygenComment(fh, "Make the Mocked function return a value.", 
                          "@see rmmMockReturn",
                          [ DoxygenParam("value", "The value to return") ],
                          "")
      fh.write(f"#define {macroName}(value{defineParams}) \\\n")
      PrintFunction(fh, f"rmMockOutput{t.title}",
                    f"(value), #value, NULL,\n"
                    f"{callParams}"
                    f"__LINE__, __FILE__,\n"
                    f"\"{macroName}\"", " \\")
   fh.write("\n")

def PrintWillReturnMacrosAndPrototype(fh, t):
   PrintGenericWillReturnMacro(fh, t, False,
                               f"rmmMockEndReturn{t.title}",
                               f"rmMockOutput{t.title}")

   PrintGenericWillReturnMacro(fh, t, True,
                               f"rmmWillOutput{t.title}",
                               f"rmMockWillOutput{t.title}")

   extraParams = ""
   if t.extraParams:
      extraParams = f"{t.extraParams},\n"
   PrintFunction(fh, f"void rmMockOutput{t.title}",
                 f"{t.signature} value,\n"
                 f"const char * valueText, const char * parameter,\n"
                 f"{extraParams}"
                 f"int line, const char * filename,\n"
                 f"const char * functionName", "", ";")
   fh.write("\n")

def PrintExpect2Macro(fh, t, actionName, actionText):
   typeAction = f"{t.title}{actionName}"
   whiteSpace = " " * (len(t.title) + len(actionName))
   PrintDoxygenComment(fh, f"Verify that the {t.text} passed into @p parameter is {actionText} to @p expected.",
                       f"",
                       [ DoxygenParam("parameter", f"The parameter name to check."),
                         DoxygenParam("expected",  f"The value that the passed in value named @p parameter needs to be {actionText} for a pass.") ],
                       f"")
   PrintGenericMacro(fh, t, True,
                     f"rmmExpect{typeAction}",
                     f"rmExpect{t.title}2",
                     f"eRMCO2_{actionName}",
                     [ "expected" ])


def PrintExpect2FunctionPrototype(fh, t):
   whiteSpace = " " * len(t.title)
   fh.write(
f"void rmExpect{t.title}2(enum ryanmock_comparison_operator2 op,\n"
f"             {whiteSpace}  const char * parameter, \n"
f"             {whiteSpace}  {t.signature} expected, const char * expectedText,\n"
)
   if t.extraParams:
      fh.write(
f"             {whiteSpace}  {t.extraParams},\n"
)
   fh.write(
f"             {whiteSpace}  int line,     const char * filename,\n"
f"             {whiteSpace}  const char * functionName);\n\n"
)

def PrintExpect3FunctionPrototype(fh, t):
   whiteSpace = " " * len(t.title)
   fh.write(
f"void rmExpect{t.title}3(enum ryanmock_comparison_operator3 op,\n"
f"             {whiteSpace}  const char * parameter, \n"
f"             {whiteSpace}  {t.signature} expected1, const char * expectedText1,\n"
f"             {whiteSpace}  {t.signature} expected2, const char * expectedText2,\n"
)
   if t.extraParams:
      fh.write(
f"             {whiteSpace}  {t.extraParams},\n"
)
   fh.write(
f"             {whiteSpace}  int line,      const char * filename,\n"
f"             {whiteSpace}  const char * functionName);\n\n"
)

def PrintExpect3Between(fh, t, negate):
   actionName = "Between"
   if negate:
      actionName = f"Not{actionName}"
   typeAction = f"{t.title}{actionName}"
   whiteSpace = " " * (len(t.title) + len(actionName))
   if negate:
      PrintDoxygenComment(fh, f"Verify that the {t.text} passed into @p parameter is not between @p min and @p max.",
                          f"The minimum and maximum are inclusive.",
                          [ DoxygenParam("parameter", f"The parameter name to check."),
                            DoxygenParam("min",       f"The minimum value that the passed in value named @p parameter is compared to."),
                            DoxygenParam("max",       f"The maximum value that the passed in value named @p parameter is compared to.") ],
                          f"")
   else:
      PrintDoxygenComment(fh, f"Verify that the {t.text} passed into @p parameter is between @p min and @p max.",
                          f"The minimum and maximum are inclusive.",
                          [ DoxygenParam("parameter", f"The parameter name to check."),
                            DoxygenParam("min",       f"The minimum value that the passed in value named @p parameter is compared to."),
                            DoxygenParam("max",       f"The maximum value that the passed in value named @p parameter is compared to.") ],
                          f"")
   PrintGenericMacro(fh, t, True,
                     f"rmmExpect{typeAction}",
                     f"rmExpect{t.title}3",
                     f"eRMCO3_{actionName}",
                     [ "min", "max" ])

def PrintExpect3Within(fh, t, negate):
   actionName = "Within"
   if negate:
      actionName = f"Not{actionName}"
   typeAction = f"{t.title}{actionName}"
   whiteSpace = " " * (len(t.title) + len(actionName))
   if negate:
      PrintDoxygenComment(fh, f"Verify that the {t.text} passed into @p parameter is not approximately @p origin.",
                          f"The equation used is abs(@p parameter value - @p origin) <= @p epsilon",
                          [ DoxygenParam("parameter", f"The parameter name to check."),
                            DoxygenParam("origin",    f"The value that the passed in value named @p parameter is not approximately equal to."),
                            DoxygenParam("epsilon",   f"The error allowed on the approximation.") ],
                          f"")
   else:
      PrintDoxygenComment(fh, f"Verify that the {t.text} passed into @p parameter is approximately @p origin.",
                          f"The equation used is abs(@p parameter value - @p origin) <= @p epsilon",
                          [ DoxygenParam("parameter", f"The parameter name to check."),
                            DoxygenParam("origin",    f"The value that the passed in value named @p parameter is approximately equal to."),
                            DoxygenParam("epsilon",   f"The error allowed on the approximation.") ],
                          f"")
   PrintGenericMacro(fh, t, True,
                     f"rmmExpect{typeAction}",
                     f"rmExpect{t.title}3",
                     f"eRMCO3_{actionName}",
                     [ "origin", "epsilon" ])


def PrintManualAssertMacrosAndPrototypes(fh):
   PrintDoxygenComment(fh, "Fail the test.",
                       "This is a variable argument function. It's parameters act like printf.",
                       [ DoxygenParam("reasonFormat", "An optional reason why the test is being failed.") ],
                       "")
   fh.write(
"#define rmmFail(...) \\\n"
"rmFail(__LINE__, __FILE__, \"rmmFail\", __VA_ARGS__)\n\n"
)
   fh.write(
"void rmFail(int line, const char * filename, \n"
"            const char * functionName, const char * reasonFormat, ...);\n\n"
)
   PrintDoxygenComment(fh, "Verify that @p value is true.",
                       "",
                       [ DoxygenParam("value", "If false, the test will fail.") ],
                       "")
   fh.write(
"#define rmmAssertTrue(value) \\\n"
"rmAssert((value), #value,  true, __LINE__, __FILE__, \"rmmAssertTrue\")\n\n"
)
   PrintDoxygenComment(fh, "Verify that @p value is false.",
                       "",
                       [ DoxygenParam("value", "If true, the test will fail.") ],
                       "")
   fh.write(
"#define rmmAssertFalse(value) \\\n"
"rmAssert((value), #value,  false, __LINE__, __FILE__, \"rmmAssertFalse\")\n\n"
)
   fh.write(
"void rmAssert(bool value, const char * valueText, bool expected, \n"
"              int line, const char * filename, const char * functionName);\n\n"
)

   PrintDoxygenComment(fh, "Verify the pointer @p actual is NULL.", "",
                       [ DoxygenParam("actual", "The pointer to check.") ],
                       "")
   fh.write(
"#define rmmAssertPtrNULL(actual) \\\n"
"rmAssertPtrNULL(actual, #actual, true, __LINE__, __FILE__, \"rmmAssertPtrNULL\")\n\n"
)
   PrintDoxygenComment(fh, "Verify the pointer @p actual is not NULL.", "",
                       [ DoxygenParam("actual", "The pointer to check.") ],
                       "")

   fh.write(
"#define rmmAssertPtrNotNULL(actual) \\\n"
"rmAssertPtrNULL(actual, #actual, false, __LINE__, __FILE__, \"rmmAssertPtrNULL\")\n\n"
)
   fh.write(
"void rmAssertPtrNULL(const void * value, const char * valueText, \n"
"                     bool shouldBeNULL, int line, const char * filename, \n"
"                     const char * functionName);\n\n"
)
   PrintDoxygenComment(fh, "Verify the User Type @p actual is equal to the User Type @p expected.",
                       "",
                       [ DoxygenParam("op",       "The test operation."),
                         DoxygenParam("type",     "The type of the @p actual and @p expected."),
                         DoxygenParam("actual",   "Pointer to the data representing the actual value."),
                         DoxygenParam("expected", "Pointer to the data representing the expected value.") ],
                       "")
   fh.write(
"#define rmmAssertType2(op, type, actual, expected) \\\n"
"rmAssertType2(op, type, actual,   #actual,         \\\n"
"                        expected, #expected,       \\\n"
"                        __LINE__, __FILE__,        \\\n"
"                        \"rmmAssertType2\")\n\n"
)
   fh.write(
"void rmAssertType2(enum ryanmock_comparison_operator2 op,\n"
"                   struct ryanmock_type * type,\n"
"                   void * actual,   const char * actualText,\n"
"                   void * expected, const char * expectedText,\n"
"                   int line, const char * filename,\n"
"                   const char * functionName);\n\n"
)

def PrintManualExpectMacrosAndPrototypes(fh):
   PrintDoxygenComment(fh, "Ignore the value passed into @p parameter.", "",
                       [ DoxygenParam("parameter", "The parameter name to ignore.") ],
                       "")
   fh.write(
"#define rmmExpectAny(parameter) \\\n"
"rmExpectAny(#parameter, __LINE__, __FILE__, \"rmmExpectAny\")\n"
)
   fh.write(
"void rmExpectAny(const char * parameter,\n"
"                 int line, const char * filename, const char * functionName);\n\n\n"
)
   PrintDoxygenComment(fh, "Verify that the value passed into @p parameter is true.", "",
                       [ DoxygenParam("parameter", "The parameter name to check.") ],
                       "")
   fh.write(
"#define rmmExpectTrue(parameter) \\\n"
"rmExpectAssert(#parameter, true, __LINE__, __FILE__, \"rmmExpectTrue\")\n\n"
)
   PrintDoxygenComment(fh, "Verify that the value passed into @p parameter is false.", "",
                       [ DoxygenParam("parameter", "The parameter name to check.") ],
                       "")
   fh.write(
"#define rmmExpectFalse(parameter) \\\n"
"rmExpectAssert(#parameter, false, __LINE__, __FILE__, \"rmmExpectFalse\")\n\n"
)
   fh.write(
"void rmExpectAssert(const char * parameter, bool expected, \n"
"                    int line, const char * filename, const char * functionName);\n\n"
)

   PrintDoxygenComment(fh, "Call callback function when checking function parameters", 
                       "Feel free to use any of the assert functions in your callback function.",
                       [ DoxygenParam("parameter", "The parameter name to check."),
                         DoxygenParam("callbackFunction", "The callback function to call."),
                         DoxygenParam("userData", "A pointer to whatever you may need in the callback.") ],
                       "")
   fh.write(
"#define rmmExpectCallback(parameter, callbackFunction, userData) \\\n"
"rmExpectCallback(#parameter, callbackFunction, #callbackFunction, userData, \\\n"
"                 __LINE__, __FILE__, \"rmmExpectCallback\")\n\n"
)
   fh.write(
"void rmExpectCallback(const char * parameter,\n"
"                      rmcheck_function callbackFunction,\n"
"                      const char * callbackFunctionName,\n"
"                      void * userData,\n"
"                      int line, const char * filename,\n"
"                      const char * functionName);\n\n\n"
)



   PrintDoxygenComment(fh, "Verify that the pointer passed into @p parameter is NULL.", "",
                       [ DoxygenParam("parameter", "The parameter name to check.") ],
                       "")
   fh.write(
"#define rmmExpectPtrNULL(parameter) \\\n"
"rmExpectPtrNULL(#parameter, true, __LINE__, __FILE__, \"rmmExpectPtrNULL\")\n\n"
)

   PrintDoxygenComment(fh, "Verify that the pointer passed into @p parameter is not NULL.", "",
                       [ DoxygenParam("parameter", "The parameter name to check.") ],
                       "")
   fh.write(
"#define rmmExpectPtrNotNULL(parameter) \\\n"
"rmExpectPtrNULL(#parameter, false, __LINE__, __FILE__, \"rmmExpectPtrNotNULL\")\n\n"
)
   PrintFunction(fh, "void rmExpectPtrNULL",
                 "const char * parameter, bool shouldBeNULL,\n"
                 "int line, const char * filename, const char * functionName",
                 "", ";")
   fh.write("\n")


def PrintManualOutputMacrosAndPrototypes(fh):
   PrintDoxygenComment(fh, "Don't change the value in @p parameter.", "",
                       [ DoxygenParam("parameter", "The parameter name not change.") ],
                       "")
   fh.write(
"#define rmmWillOutputNothing(parameter) \\\n"
"rmMockOutputMemory(NULL, \"NULL\", #parameter, \\\n"
"                   1, 0, \\\n"
"                   __LINE__, __FILE__, \\\n"
"                   \"rmmWillOutputNothing\")\n\n"
)

def PrintPreprocessorCheckBegin(fh, t):
   if t.preProcessorCheck:
      fh.write(f'{t.preProcessorCheck}\n')

def PrintPreprocessorCheckEnd(fh, t):
   if t.preProcessorCheck:
      fh.write("#endif\n")


def PrintAllMacrosAndPrototype(fh):
   fh.write(
"/**\n"
" * @file\n"
" * \n"
" * This is an automaticaly generated file, do not edit!\n"
" * See: devtools/headergen.py\n"
" */\n\n"
)


   PrintGroupStart(fh, "Asserts", "Assert functions", "")

   PrintManualAssertMacrosAndPrototypes(fh)
   for t in TypeTable:
      PrintPreprocessorCheckBegin(fh, t)
      
      for action in Action2Table:
         PrintAssert2Macro(fh, t, action.name, action.text)
      PrintAssert2FunctionPrototype(fh, t)

      PrintAssert3Between(fh, t, False)
      PrintAssert3Between(fh, t, True)
      PrintAssert3Within(fh, t, False)
      PrintAssert3Within(fh, t, True)
      PrintAssert3FunctionPrototype(fh, t)
      
      PrintPreprocessorCheckEnd(fh, t)

   PrintGroupEnd(fh, "Asserts")

   PrintGroupStart(fh, "MockOutputs", "Mocking output functions", "")
   
   PrintManualOutputMacrosAndPrototypes(fh)
   for t in TypeTable:
      PrintPreprocessorCheckBegin(fh, t)
      PrintWillReturnMacrosAndPrototype(fh, t)
      PrintPreprocessorCheckEnd(fh, t)

   PrintGroupEnd(fh, "MockOutputs")

   PrintGroupStart(fh, "Expects", "Expect functions",
                   "These functions need to be between a mock begin and mock end function call."
                   "They also need an assocated check function call inside the mocked function."
                   "\n\n"
                   "@see rmmMockBegin\n"
                   "@see rmmMockEnd\n"
                   "@see rmmParamCheck")

   PrintManualExpectMacrosAndPrototypes(fh)
   for t in TypeTable:
      PrintPreprocessorCheckBegin(fh, t)
      for action in Action2Table:
         PrintExpect2Macro(fh, t, action.name, action.text)
      PrintExpect2FunctionPrototype(fh, t)

      PrintExpect3Between(fh, t, False)
      PrintExpect3Between(fh, t, True)
      PrintExpect3Within(fh, t, False)
      PrintExpect3Within(fh, t, True)

      PrintExpect3FunctionPrototype(fh, t)

      PrintPreprocessorCheckEnd(fh, t)

   PrintGroupEnd(fh, "Expects")



if __name__ == "__main__":
   if len(sys.argv) >= 2:
      with open(sys.argv[1], "w") as fh:
         PrintAllMacrosAndPrototype(fh)
   else:
      PrintAllMacrosAndPrototype(sys.stdout)


