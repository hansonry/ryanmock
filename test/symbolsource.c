// Symbol Source File for Symbol Table Test

#include <rmsymboltable.h>

static const int array[] = {1, 2, 3, 4, 5};

static int a;

static int doubler(int in)
{
   return 2 * in;
}

// Public Getter and Setter Functions for checking and Setting
int getA(void)
{
   return a;
}

void setA(int value)
{
   a = value;
}

// Usualy named the same as the current module, but for testing purposes
// we are changing it up
RMM_SYMBOLTABLE_BEGIN(symbolSource1)
   RMM_SYMBOLTABLE_SYMBOL(a)
   RMM_SYMBOLTABLE_FUNCTION(doubler)
   RMM_SYMBOLTABLE_ARRAY(array)
RMM_SYMBOLTABLE_END()

