#include <ryanmock.h>

#define MAKE_PTR(type, value) ((type *)(uintptr_t)(value))

enum weekdays
{
   SUNDAY = 0,
   MONDAY,
   TUESDAY,
   WEDNESDAY,
   THURSDAY,
   FRIDAY,
   SATURDAY
};

void futBool(bool value)
{
   rmmParamCheck(value);
}

void futInt(int value)
{
   rmmParamCheck(value);
}

void futInt2(int value2)
{
   rmmParamCheck(value2);
}

void futPtr(void * value)
{
   rmmParamCheck(value);
}

void futMemory(void * value, size_t size)
{
   rmmParamCheckVoidArray(value, size);
}

void futEnum(uint16_t before, enum weekdays weekday, uint16_t after)
{
   (void)before;
   (void)after;
   rmmParamCheck(weekday);
}

void callbackInt(const void * parameterData, void * userData)
{
   const int * actualData = parameterData;
   const int * expectedData = userData;
   rmmAssertIntEqual(*actualData, *expectedData);
}

void futString(const char * value)
{
   rmmParamCheck(value);
}

void futThreeInt(int a, int b, int c)
{
   rmmParamCheck(a);
   rmmParamCheck(b);
   rmmParamCheck(c);
}


void futULong(unsigned long value)
{
   rmmParamCheck(value);
}

void futUShort(unsigned short value)
{
   rmmParamCheck(value);
}

void futIntU8(uint8_t value)
{
   rmmParamCheck(value);
}

void futIntU16(uint16_t value)
{
   rmmParamCheck(value);
}

void futIntU32(uint32_t value)
{
   rmmParamCheck(value);
}

void futIntPtrU(uintptr_t value)
{
   rmmParamCheck(value);
}

void futIntPtr(intptr_t value)
{
   rmmParamCheck(value);
}


void passing_rmmExpectTrue(void)
{
   rmmMockBegin(futBool);
      rmmExpectTrue(value);
   rmmMockEnd();
   rmmMockBegin(futInt);
      rmmExpectTrue(value);
   rmmMockEnd();
   
   // FUT
   futBool(true);
   futInt(1);
}

void failing_rmmExpectTrue(void)
{
   rmmMockBegin(futBool);
      rmmExpectTrue(value);
   rmmMockEnd();
   
   // FUT
   futBool(false);
}

void passing_rmmExpectFalse(void)
{
   rmmMockBegin(futBool);
      rmmExpectFalse(value);
   rmmMockEnd();
   rmmMockBegin(futInt);
      rmmExpectFalse(value);
   rmmMockEnd();
   
   // FUT
   futBool(false);
   futInt(0);
}

void failing_rmmExpectFalse(void)
{
   rmmMockBegin(futBool);
      rmmExpectFalse(value);
   rmmMockEnd();
   
   // FUT
   futBool(true);
}


void passing_rmmExpectIntEqual(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, -1);
   rmmMockEnd();
   
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 4);
   rmmMockEnd();

   // FUT
   futInt(-1);
   futInt(4);
}


void failing_rmmExpectIntEqual(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 6);
   rmmMockEnd();

   // FUT
   futInt(7);
}

void passing_rmmExpectIntNotEqual(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntNotEqual(value, -1);
   rmmMockEnd();
   
   rmmMockBegin(futInt);
      rmmExpectIntNotEqual(value, 4);
   rmmMockEnd();

   // FUT
   futInt(5);
   futInt(-3);
}


void failing_rmmExpectIntNotEqual(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntNotEqual(value, 33);
   rmmMockEnd();

   // FUT
   futInt(33);
}

void passing_rmmExpectIntGreaterThan(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntGreaterThan(value, 4);
   rmmMockEnd();

   // FUT
   futInt(7);
}

void failing_rmmExpectIntGreaterThan(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntGreaterThan(value, 33);
   rmmMockEnd();

   // FUT
   futInt(10);
}

void passing_rmmExpectIntGreaterThanOrEqualTo(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntGreaterThanOrEqualTo(value, 5);
   rmmMockEnd();
   
   rmmMockBegin(futInt);
      rmmExpectIntGreaterThanOrEqualTo(value, -44);
   rmmMockEnd();

   // FUT
   futInt(6);
   futInt(-44);
}

void failing_rmmExpectIntGreaterThanOrEqualTo(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntGreaterThanOrEqualTo(value, 10);
   rmmMockEnd();

   // FUT
   futInt(1);
}

void passing_rmmExpectIntLessThan(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntLessThan(value, 6);
   rmmMockEnd();

   // FUT
   futInt(-5);
}

void failing_rmmExpectIntLessThan(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntLessThan(value, 10);
   rmmMockEnd();

   // FUT
   futInt(11);
}

void passing_rmmExpectIntLessThanOrEqualTo(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntLessThanOrEqualTo(value, 6);
   rmmMockEnd();
   
   rmmMockBegin(futInt);
      rmmExpectIntLessThanOrEqualTo(value, 6);
   rmmMockEnd();

   // FUT
   futInt(-5);
   futInt(6);
}

void failing_rmmExpectIntLessThanOrEqualTo(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntLessThanOrEqualTo(value, 33);
   rmmMockEnd();

   // FUT
   futInt(34);
}

void passing_rmmExpectIntBetween(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntBetween(value, 10, 20);
   rmmMockEnd();

   // FUT
   futInt(15);
}

void failing_rmmExpectIntBetween_OORHigh(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntBetween(value, -10, -5);
   rmmMockEnd();

   // FUT
   futInt(0);
}

void failing_rmmExpectIntBetween_OORLow(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntBetween(value, -10, -5);
   rmmMockEnd();

   // FUT
   futInt(-15);
}


void passing_rmmExpectIntNotBetween(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntNotBetween(value, 0, 100);
   rmmMockEnd();
   rmmMockBegin(futInt);
      rmmExpectIntNotBetween(value, 0, 100);
   rmmMockEnd();
   

   // FUT
   futInt(-5);
   futInt(105);
}

void failing_rmmExpectIntNotBetween(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntNotBetween(value, 0, 100);
   rmmMockEnd();

   // FUT
   futInt(10);
}

void passing_rmmExpectIntWithin(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntWithin(value, 10, 5);
   rmmMockEnd();
   rmmMockBegin(futInt);
      rmmExpectIntWithin(value, 10, 5);
   rmmMockEnd();

   // FUT
   futInt(5);
   futInt(15);
}

void failing_rmmExpectIntWithin_OORHigh(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntWithin(value, 10, 5);
   rmmMockEnd();

   // FUT
   futInt(16);
}

void failing_rmmExpectIntWithin_OORLow(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntWithin(value, 10, 5);
   rmmMockEnd();

   // FUT
   futInt(4);
}

void passing_rmmExpectIntNotWithin(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntNotWithin(value, -10, 5);
   rmmMockEnd();
   rmmMockBegin(futInt);
      rmmExpectIntNotWithin(value, -10, 5);
   rmmMockEnd();

   // FUT
   futInt(-50);
   futInt(15);
}

void failing_rmmExpectIntNotWithin(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntNotWithin(value, -10, 5);
   rmmMockEnd();

   // FUT
   futInt(-11);
}

void passing_rmmExpectPtrNULL(void)
{
   rmmMockBegin(futPtr);
      rmmExpectPtrNULL(value);
   rmmMockEnd();
   
   // FUT
   futPtr(NULL);
}

void failing_rmmExpectPtrNULL(void)
{
   void *a = MAKE_PTR(void, 2340);
   rmmMockBegin(futPtr);
      rmmExpectPtrNULL(value);
   rmmMockEnd();
   
   // FUT
   futPtr(a);
}

void passing_rmmExpectPtrNotNULL(void)
{
   int a = 5;
   rmmMockBegin(futPtr);
      rmmExpectPtrNotNULL(value);
   rmmMockEnd();
   
   // FUT
   futPtr(&a);
}

void failing_rmmExpectPtrNotNULL(void)
{
   rmmMockBegin(futPtr);
      rmmExpectPtrNotNULL(value);
   rmmMockEnd();
   
   // FUT
   futPtr(NULL);
}

void passing_rmmExpectPtrEqual(void)
{
   void *a = MAKE_PTR(void, 0x5555);
   void *b = MAKE_PTR(void, 0xFFCC);
   int stuff[] = {1, 2, 3, 4, 5};
   rmmMockBegin(futPtr);
      rmmExpectPtrEqual(value, a);
   rmmMockEnd();
   rmmMockBegin(futPtr);
      rmmExpectPtrEqual(value, b);
   rmmMockEnd();
   rmmMockBegin(futMemory);
      rmmExpectPtrEqual(value, stuff);
   rmmMockEnd();
   
   // FUT
   futPtr(a);
   futPtr(b);
   futMemory(stuff, 5 * sizeof(int));
}

void failing_rmmExpectPtrEqual(void)
{
   void *a = MAKE_PTR(void, 0x2340);
   void *b = MAKE_PTR(void, 0xaabb);
   rmmMockBegin(futPtr);
      rmmExpectPtrEqual(value, a);
   rmmMockEnd();
   
   // FUT
   futPtr(b);
}

void passing_rmmExpectPtrNotEqual(void)
{
   void *a = MAKE_PTR(void, 0x5555);
   void *b = MAKE_PTR(void, 0xFFCC);
   void *c = NULL;
   void *d = MAKE_PTR(void, 0x33);
   rmmMockBegin(futPtr);
      rmmExpectPtrNotEqual(value, a);
   rmmMockEnd();
   rmmMockBegin(futPtr);
      rmmExpectPtrNotEqual(value, b);
   rmmMockEnd();
   
   // FUT
   futPtr(c);
   futPtr(d);
}

void failing_rmmExpectPtrNotEqual(void)
{
   void *a = MAKE_PTR(void, 0xeffe);
   void *b = MAKE_PTR(void, 0xeffe);
   rmmMockBegin(futPtr);
      rmmExpectPtrNotEqual(value, a);
   rmmMockEnd();
   
   // FUT
   futPtr(b);
}

void passing_rmmExpectMemoryEqual(void)
{
   int a[] = {10, 20, 30, 40, 50};
   int b[] = {10, 20, 30, 40, 60};
   rmmMockBegin(futMemory);
      rmmExpectMemoryEqual(value, a, 4);
   rmmMockEnd();
   
   rmmMockBegin(futMemory);
      rmmExpectMemoryEqual(value, a, 5);
   rmmMockEnd();
   
   // FUT
   futMemory(b, 4 * sizeof(int));
   futMemory(a, 5 * sizeof(int));
}

void failing_rmmExpectMemoryEqual(void)
{
   int a[] = {10, 20, 54, 32, 234};
   int b[] = {10, 20, 30, 40, 604};
   rmmMockBegin(futMemory);
      rmmExpectMemoryEqual(value, a, 5);
   rmmMockEnd();
   
   // FUT
   futMemory(b, 5 * sizeof(int));
}

void failing_rmmExpectMemoryEqualOverSize(void)
{
   int a[] = {10, 20, 30, 40, 604};
   int b[] = {10, 20, 30, 40, 604};
   rmmMockBegin(futMemory);
      rmmExpectMemoryEqual(value, a, 5);
   rmmMockEnd();
   
   // FUT
   futMemory(b, 1 * sizeof(int));
}

void passing_twoDiffrentIntChecksInOrder(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 6);
   rmmMockEnd();
   
   rmmMockBegin(futInt2);
      rmmExpectIntEqual(value2, 8);
   rmmMockEnd();

   // FUT
   futInt(6);
   futInt2(8);
}

void passing_twoDiffrentIntChecksOutOfOrder(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 6);
   rmmMockEnd();
   
   rmmMockBegin(futInt2);
      rmmExpectIntEqual(value2, 8);
   rmmMockEnd();

   // FUT
   futInt2(8);
   futInt(6);
}

void passing_twoSameIntChecksInOrder(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 1);
   rmmMockEnd();
   
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 2);
   rmmMockEnd();

   // FUT
   futInt(1);
   futInt(2);
}

void failing_twoSameIntChecksOutOfOrder(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 1);
   rmmMockEnd();
   
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 2);
   rmmMockEnd();

   // FUT
   futInt(2);
   futInt(1);
}

void passing_rmmMockBeginCount(void)
{
   rmmMockBeginCount(futInt, 3);
      rmmExpectIntEqual(value, 5);
   rmmMockEnd();
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 3);
   rmmMockEnd();
   
   // FUT
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(3);
}

void failing_rmmMockBeginCount(void)
{
   rmmMockBeginCount(futInt, 3);
      rmmExpectIntEqual(value, 5);
   rmmMockEnd();
   
   // FUT
   futInt(5);
   futInt(3);
   futInt(5);
}

void passing_rmmExpectAny(void)
{
   rmmMockBegin(futThreeInt);
      rmmExpectIntEqual(a, 1);
      rmmExpectAny(b);
      rmmExpectIntEqual(c, 3);
   rmmMockEnd();
   rmmMockBegin(futThreeInt);
      rmmExpectAny(a);
      rmmExpectIntEqual(b, 5);
      rmmExpectIntEqual(c, 6);
   rmmMockEnd();
   rmmMockBegin(futThreeInt);
      rmmExpectIntEqual(a, 7);
      rmmExpectIntEqual(b, 8);
      rmmExpectAny(c);
   rmmMockEnd();

   // FUT
   futThreeInt(1, 2, 3);
   futThreeInt(4, 5, 6);
   futThreeInt(7, 8, 9);
}

void failing_rmmExpectAnyFunctionNotCalled(void)
{
   rmmMockBegin(futThreeInt);
      rmmExpectAny(a);
      rmmExpectAny(b);
      rmmExpectAny(c);
   rmmMockEnd();
}

void passing_rmmMockBeginAll(void)
{
   rmmMockBeginAll(futInt);
      rmmExpectIntEqual(value, 5);
   rmmMockEnd();
   
   // FUT
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
}

void failing_rmmMockBeginAll(void)
{
   rmmMockBeginAll(futInt);
      rmmExpectIntEqual(value, 5);
   rmmMockEnd();
   
   // FUT
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(0);
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
   futInt(5);
}

void passing_rmmMockBeginAllAny(void)
{
   rmmMockBeginAll(futInt);
      rmmExpectAny(value);
   rmmMockEnd();
   rmmMockBeginAll(futBool);
      rmmExpectTrue(value);
   rmmMockEnd();
   
   // FUT
   futInt(1);
   futInt(2);
   futInt(3);
   futInt(4);
   futBool(true);
   futInt(5);
   futInt(6);
   futInt(7);
   futInt(8);
   futInt(9);
}

void passing_rmmExpectCallback(void)
{
   int expected = 5;
   rmmMockBegin(futInt);
      rmmExpectCallback(value, callbackInt, &expected);
   rmmMockEnd();
   
   // FUT
   futInt(5);
}

void failing_rmmExpectCallbackNotEqual(void)
{
   int expected = 5;
   rmmMockBegin(futInt);
      rmmExpectCallback(value, callbackInt, &expected);
   rmmMockEnd();
   
   // FUT
   futInt(7);
}

void failing_rmmExpectCallbackNotCalled(void)
{
   int expected = 5;
   rmmMockBegin(futInt);
      rmmExpectCallback(value, callbackInt, &expected);
   rmmMockEnd();
   
   // FUT
   // Intentionaly Empty
}

void passing_rmmExpectULong(void)
{
   rmmMockBegin(futULong);
      rmmExpectULongEqual(value, -1);
   rmmMockEnd();
   
   futULong(-1);
   
   rmmMockBegin(futULong);
      rmmExpectULongGreaterThan(value, 4);
   rmmMockEnd();

   futULong(10);

   rmmMockBegin(futULong);
      rmmExpectULongGreaterThanOrEqualTo(value, 4);
   rmmMockEnd();

   futULong(5);

   rmmMockBegin(futULong);
      rmmExpectULongGreaterThanOrEqualTo(value, 5);
   rmmMockEnd();

   futULong(5);

}


void failing_rmmExpectULongEqual(void)
{
   rmmMockBegin(futULong);
      rmmExpectULongEqual(value, 6);
   rmmMockEnd();

   // FUT
   futULong(7);
}

void passing_rmmExpectUShort(void)
{
   rmmMockBegin(futUShort);
      rmmExpectUShortEqual(value, -1);
   rmmMockEnd();
   
   futUShort(-1);
   
   rmmMockBegin(futUShort);
      rmmExpectUShortGreaterThan(value, 4);
   rmmMockEnd();

   futUShort(10);

   rmmMockBegin(futUShort);
      rmmExpectUShortGreaterThanOrEqualTo(value, 4);
   rmmMockEnd();

   futUShort(5);

   rmmMockBegin(futUShort);
      rmmExpectUShortGreaterThanOrEqualTo(value, 5);
   rmmMockEnd();

   futUShort(5);

}


void failing_rmmExpectUShortEqual(void)
{
   rmmMockBegin(futUShort);
      rmmExpectUShortEqual(value, 6);
   rmmMockEnd();

   // FUT
   futUShort(7);
}

void passing_rmmExpectIntU8(void)
{
   rmmMockBegin(futIntU8);
      rmmExpectIntU8Equal(value, -1);
   rmmMockEnd();
   
   futIntU8(-1);
   
   rmmMockBegin(futIntU8);
      rmmExpectIntU8GreaterThan(value, 4);
   rmmMockEnd();

   futIntU8(10);

   rmmMockBegin(futIntU8);
      rmmExpectIntU8GreaterThanOrEqualTo(value, 4);
   rmmMockEnd();

   futIntU8(5);

   rmmMockBegin(futIntU8);
      rmmExpectIntU8GreaterThanOrEqualTo(value, 5);
   rmmMockEnd();

   futIntU8(5);

}


void failing_rmmExpectIntU8Equal(void)
{
   rmmMockBegin(futIntU8);
      rmmExpectIntU8Equal(value, 6);
   rmmMockEnd();

   // FUT
   futIntU8(7);
}

void passing_rmmExpectBool(void)
{
   rmmMockBegin(futBool);
      rmmExpectBoolEqual(value, true);
   rmmMockEnd();
   
   futBool(true);
   
   rmmMockBegin(futBool);
      rmmExpectBoolNotEqual(value, true);
   rmmMockEnd();

   futBool(false);
}


void failing_rmmExpectBoolEqual(void)
{
   rmmMockBegin(futBool);
      rmmExpectBoolEqual(value, false);
   rmmMockEnd();

   // FUT
   futBool(true);
}

void passing_rmmExpectEnum(void)
{
   rmmMockBegin(futEnum);
      rmmExpectEnumEqual(weekday, MONDAY);
   rmmMockEnd();
   
   futEnum(0xFFFF, MONDAY, 0xFFFF);
   
   rmmMockBegin(futEnum);
      rmmExpectEnumGreaterThan(weekday, MONDAY);
   rmmMockEnd();

   futEnum(0xFFFF, TUESDAY, 0xFFFF);

   rmmMockBegin(futEnum);
      rmmExpectEnumGreaterThanOrEqualTo(weekday, TUESDAY);
   rmmMockEnd();

   futEnum(0xFFFF, WEDNESDAY, 0xFFFF);

   rmmMockBegin(futEnum);
      rmmExpectEnumGreaterThanOrEqualTo(weekday, WEDNESDAY);
   rmmMockEnd();

   futEnum(0xFFFF, WEDNESDAY, 0xFFFF);
}


void failing_rmmExpectEnumEqual(void)
{
   rmmMockBegin(futEnum);
      rmmExpectEnumEqual(weekday, MONDAY);
   rmmMockEnd();

   // FUT
   futEnum(0xFFFF, SATURDAY, 0xFFFF);
}

void passing_rmmExpectIntU16(void)
{
   rmmMockBegin(futIntU16);
      rmmExpectIntU16Equal(value, -1);
   rmmMockEnd();
   
   futIntU16(-1);
   
   rmmMockBegin(futIntU16);
      rmmExpectIntU16GreaterThan(value, 4);
   rmmMockEnd();

   futIntU16(10);

   rmmMockBegin(futIntU16);
      rmmExpectIntU16GreaterThanOrEqualTo(value, 4);
   rmmMockEnd();

   futIntU16(5);

   rmmMockBegin(futIntU16);
      rmmExpectIntU16GreaterThanOrEqualTo(value, 5);
   rmmMockEnd();

   futIntU16(5);

}


void failing_rmmExpectIntU16Equal(void)
{
   rmmMockBegin(futIntU16);
      rmmExpectIntU16Equal(value, 6);
   rmmMockEnd();

   // FUT
   futIntU16(7);
}

void passing_rmmExpectString(void)
{
   rmmMockBegin(futString);
      rmmExpectStringEqual(value, "A small String");
   rmmMockEnd();
   
   futString("A small String");
}

void failing_rmmExpectStringEqual(void)
{
   rmmMockBegin(futString);
      rmmExpectStringEqual(value, "A small String");
   rmmMockEnd();
   
   futString("An incorrect String");
   
}
void passing_rmmExpectIntU32(void)
{
   rmmMockBegin(futIntU32);
      rmmExpectIntU32Equal(value, -1);
   rmmMockEnd();
   
   futIntU32(-1);
   
   rmmMockBegin(futIntU32);
      rmmExpectIntU32GreaterThan(value, 4);
   rmmMockEnd();

   futIntU32(10);

   rmmMockBegin(futIntU32);
      rmmExpectIntU32GreaterThanOrEqualTo(value, 4);
   rmmMockEnd();

   futIntU32(5);

   rmmMockBegin(futIntU32);
      rmmExpectIntU32GreaterThanOrEqualTo(value, 5);
   rmmMockEnd();

   futIntU32(5);

}


void failing_rmmExpectIntU32Equal(void)
{
   rmmMockBegin(futIntU32);
      rmmExpectIntU32Equal(value, 6);
   rmmMockEnd();

   // FUT
   futIntU32(7);
}

void passing_rmmExpectIntPtrU(void)
{
   rmmMockBegin(futIntPtrU);
      rmmExpectIntPtrUEqual(value, -1);
   rmmMockEnd();
   
   futIntPtrU(-1);
   
   rmmMockBegin(futIntPtrU);
      rmmExpectIntPtrUGreaterThan(value, 4);
   rmmMockEnd();

   futIntPtrU(10);

   rmmMockBegin(futIntPtrU);
      rmmExpectIntPtrUGreaterThanOrEqualTo(value, 4);
   rmmMockEnd();

   futIntPtrU(5);

   rmmMockBegin(futIntPtrU);
      rmmExpectIntPtrUGreaterThanOrEqualTo(value, 5);
   rmmMockEnd();

   futIntPtrU(5);

}


void failing_rmmExpectIntPtrUEqual(void)
{
   rmmMockBegin(futIntPtrU);
      rmmExpectIntPtrUEqual(value, 6);
   rmmMockEnd();

   // FUT
   futIntPtrU(7);
}

void passing_rmmExpectIntPtr(void)
{
   rmmMockBegin(futIntPtr);
      rmmExpectIntPtrEqual(value, -1);
   rmmMockEnd();
   
   futIntPtr(-1);
   
   rmmMockBegin(futIntPtr);
      rmmExpectIntPtrGreaterThan(value, 4);
   rmmMockEnd();

   futIntPtr(10);

   rmmMockBegin(futIntPtr);
      rmmExpectIntPtrGreaterThanOrEqualTo(value, 4);
   rmmMockEnd();

   futIntPtr(5);

   rmmMockBegin(futIntPtr);
      rmmExpectIntPtrGreaterThanOrEqualTo(value, 5);
   rmmMockEnd();

   futIntPtr(5);

}


void failing_rmmExpectIntPtrEqual(void)
{
   rmmMockBegin(futIntPtr);
      rmmExpectIntPtrEqual(value, 6);
   rmmMockEnd();

   // FUT
   futIntPtr(7);
}
   
int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_rmmExpectTrue),
      rmmMakeTest(failing_rmmExpectTrue),
      rmmMakeTest(passing_rmmExpectFalse),
      rmmMakeTest(failing_rmmExpectFalse),
      rmmMakeTest(passing_rmmExpectIntEqual),
      rmmMakeTest(failing_rmmExpectIntEqual),
      rmmMakeTest(passing_rmmExpectIntNotEqual),
      rmmMakeTest(failing_rmmExpectIntNotEqual),
      rmmMakeTest(passing_rmmExpectIntGreaterThan),
      rmmMakeTest(failing_rmmExpectIntGreaterThan),
      rmmMakeTest(passing_rmmExpectIntGreaterThanOrEqualTo),
      rmmMakeTest(failing_rmmExpectIntGreaterThanOrEqualTo),
      rmmMakeTest(passing_rmmExpectIntLessThan),
      rmmMakeTest(failing_rmmExpectIntLessThan),
      rmmMakeTest(passing_rmmExpectIntLessThanOrEqualTo),
      rmmMakeTest(failing_rmmExpectIntLessThanOrEqualTo),
      rmmMakeTest(passing_rmmExpectIntBetween),
      rmmMakeTest(failing_rmmExpectIntBetween_OORHigh),
      rmmMakeTest(failing_rmmExpectIntBetween_OORLow),
      rmmMakeTest(passing_rmmExpectIntNotBetween),
      rmmMakeTest(failing_rmmExpectIntNotBetween),
      rmmMakeTest(passing_rmmExpectIntWithin),
      rmmMakeTest(failing_rmmExpectIntWithin_OORHigh),
      rmmMakeTest(failing_rmmExpectIntWithin_OORLow),
      rmmMakeTest(passing_rmmExpectIntNotWithin),
      rmmMakeTest(failing_rmmExpectIntNotWithin),
      rmmMakeTest(passing_rmmExpectPtrNULL),
      rmmMakeTest(failing_rmmExpectPtrNULL),
      rmmMakeTest(passing_rmmExpectPtrNotNULL),
      rmmMakeTest(failing_rmmExpectPtrNotNULL),
      rmmMakeTest(passing_rmmExpectPtrEqual),
      rmmMakeTest(failing_rmmExpectPtrEqual),
      rmmMakeTest(passing_rmmExpectPtrNotEqual),
      rmmMakeTest(failing_rmmExpectPtrNotEqual),
      rmmMakeTest(passing_rmmExpectMemoryEqual),
      rmmMakeTest(failing_rmmExpectMemoryEqual),
      rmmMakeTest(failing_rmmExpectMemoryEqualOverSize),
      rmmMakeTest(passing_twoDiffrentIntChecksInOrder),
      rmmMakeTest(passing_twoDiffrentIntChecksOutOfOrder),
      rmmMakeTest(passing_twoSameIntChecksInOrder),
      rmmMakeTest(failing_twoSameIntChecksOutOfOrder),
      rmmMakeTest(passing_rmmMockBeginCount),
      rmmMakeTest(failing_rmmMockBeginCount),
      rmmMakeTest(passing_rmmExpectAny),
      rmmMakeTest(failing_rmmExpectAnyFunctionNotCalled),
      rmmMakeTest(passing_rmmMockBeginAll),
      rmmMakeTest(failing_rmmMockBeginAll),
      rmmMakeTest(passing_rmmMockBeginAllAny),
      rmmMakeTest(passing_rmmExpectCallback),
      rmmMakeTest(failing_rmmExpectCallbackNotEqual),
      rmmMakeTest(failing_rmmExpectCallbackNotCalled),
      rmmMakeTest(passing_rmmExpectULong),
      rmmMakeTest(failing_rmmExpectULongEqual),
      rmmMakeTest(passing_rmmExpectUShort),
      rmmMakeTest(failing_rmmExpectUShortEqual),
      rmmMakeTest(passing_rmmExpectIntU8),
      rmmMakeTest(failing_rmmExpectIntU8Equal),
      rmmMakeTest(passing_rmmExpectBool),
      rmmMakeTest(failing_rmmExpectBoolEqual),
      rmmMakeTest(passing_rmmExpectEnum),
      rmmMakeTest(failing_rmmExpectEnumEqual),
      rmmMakeTest(passing_rmmExpectIntU16),
      rmmMakeTest(failing_rmmExpectIntU16Equal),
      rmmMakeTest(passing_rmmExpectString),
      rmmMakeTest(failing_rmmExpectStringEqual),
      rmmMakeTest(passing_rmmExpectIntU32),
      rmmMakeTest(failing_rmmExpectIntU32Equal),
      rmmMakeTest(passing_rmmExpectIntPtrU),
      rmmMakeTest(failing_rmmExpectIntPtrUEqual),
      rmmMakeTest(passing_rmmExpectIntPtr),
      rmmMakeTest(failing_rmmExpectIntPtrEqual),
   };
   return rmmRunTestsCmdLine(tests, "expects", argc, args);
}
