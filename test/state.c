#include <stdio.h>
#include <ryanmock.h>


static 
void suiteState(enum ryanmock_state state, const struct ryanmock_test * test)
{
   (void)test;
   switch(state)
   {
   case eRMS_SuiteSetup:
      printf("Suite Suite Setup\n");
      break;
   case eRMS_TestSetup:
      printf("Suite Test Setup\n");
      break;
   case eRMS_TestTeardown:
      printf("Suite Test Teardown\n");
      break;
   case eRMS_SuiteTeardown:
      printf("Suite Suite Teardown\n");
      break;
   }
}

static 
void testState(enum ryanmock_state state, const struct ryanmock_test * test)
{
   (void)test;
   switch(state)
   {
   case eRMS_SuiteSetup:
      printf("Test Suite Setup\n");
      break;
   case eRMS_TestSetup:
      printf("Test Test Setup\n");
      break;
   case eRMS_TestTeardown:
      printf("Test Test Teardown\n");
      break;
   case eRMS_SuiteTeardown:
      printf("Test Suite Teardown\n");
      break;
   }
}

void passing_test(void)
{
   rmmAssertIntEqual(7, 7);
   printf("Test\n");
}
   
int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest2(passing_test, testState),
   };
   return rmmRunTestsCmdLine2(tests, "state", suiteState, argc, args);
}
