#include <ryanmock.h>

void passing_A(void)
{
}

void passing_B(void)
{
}

void passing_C(void)
{
}

void passing_D(void)
{
}

   
int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_A),
      rmmMakeTest(passing_B),
      rmmMakeTest(passing_C),
      rmmMakeTest(passing_D),
   };
   return rmmRunTestsCmdLine(tests, "combination", argc, args);
}
