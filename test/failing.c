#include <ryanmock.h>


void empty_test(void)
{
}

void failing_test(void)
{
   rmmAssertTrue(false);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(empty_test),
      rmmMakeTest(failing_test)
   };
   return rmmRunTestsCmdLine(tests, "failing", argc, args);
}
