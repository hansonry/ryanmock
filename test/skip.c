#include <ryanmock.h>


void passing_someOtherPassingTest(void)
{
   rmmAssertIntEqual(1, 1);
}

void skipped_skip(void)
{
   rmmSkip();
}

void skipped_withPassingCheck(void)
{
   
   rmmAssertIntEqual(10, 10);
   rmmSkip();
   rmmAssertIntEqual(3, 4);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_someOtherPassingTest),
      rmmMakeTest(skipped_skip),
      rmmMakeTest(skipped_withPassingCheck),

   };
   return rmmRunTestsCmdLine(tests, "skip", argc, args);
}

