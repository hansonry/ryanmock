#include <ryanmock.h>


void empty_test(void)
{
}

void passing_test(void)
{
   rmmAssertTrue(1);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(empty_test),
      rmmMakeTest(passing_test)
   };
   return rmmRunTestsCmdLine(tests, "passing", argc, args);
}
