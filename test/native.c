#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define MAKE_PTR(type, value) ((type *)(uintptr_t)(value))

int main(int argc, char * args[])
{
   int * nullPtr = NULL;
   int * aPtr = MAKE_PTR(int, 0x1a);
   // Print System Bit Width (16, 32, 64, ....)
   printf("%d\n", (int)sizeof(void*) * 8);
   // Print a null pointer
   printf("%p\n", nullPtr);
   // Print a short pointer to check for padding and letter case
   printf("%p\n", aPtr);
   return 0;
}

