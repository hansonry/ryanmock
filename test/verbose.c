#include <ryanmock.h>


int mocked_function(int * a)
{
   rmmFunctionCalled();
   rmmParamCheck(a);
   rmmMockParam(a, 1);
   rmmMockReturn(int);
}

void failing_notMocked(void)
{
   int a = 5;
   mocked_function(&a);
}

void failing_missingReturn(void)
{
   int a = 5;
   rmmMockBeginOrdered(mocked_function);
   rmmExpectAny(a);
   rmmWillOutputNothing(a);
   rmmMockEnd();

   mocked_function(&a);
}


void failing_missingParameter(void)
{
   int a = 5;
   rmmMockBeginOrdered(mocked_function);
   rmmWillOutputNothing(a);
   rmmMockEndReturnInt(5);

   mocked_function(&a);
}

void failing_missingOutput(void)
{
   int a = 5;
   rmmMockBeginOrdered(mocked_function);
   rmmExpectAny(a);
   rmmMockEndReturnInt(5);

   mocked_function(&a);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(failing_notMocked),
      rmmMakeTest(failing_missingReturn),
      rmmMakeTest(failing_missingParameter),
      rmmMakeTest(failing_missingOutput),
   };
   return rmmRunTestsCmdLine(tests, "verbose", argc, args);
}
