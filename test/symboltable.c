#include <ryanmock.h>

// Pretend these are included via a header file of some sort
extern int getA(void);
extern void setA(int value);

// Symbols to link into
static const int * array;
static int * a;
static int (*doubler)(int in);
static int *thisSymbolDoesntExist;

RMM_SYMBOLTABLE_LOAD(symbolSource1)

void passing_loadAllSymbolsAndTest(void)
{
   const int expectArray[] = {1, 2, 3, 4, 5};
   rmmSymbolTableLink(symbolSource1, a);
   rmmSymbolTableLink(symbolSource1, array);
   rmmSymbolTableLink(symbolSource1, doubler);
   
   setA(5);
   rmmAssertIntEqual(*a, 5);
   
   setA(6);
   rmmAssertIntEqual(*a, 6);
   
   *a = 7;
   rmmAssertIntEqual(getA(), 7);

   *a = 10;
   rmmAssertIntEqual(getA(), 10);

   
   rmmAssertIntEqual(doubler(2), 4);
   rmmAssertIntEqual(doubler(4), 8);
   
   rmmAssertMemoryEqual(expectArray, array, 5);

}

void failing_LoadNonExistantSymbol(void)
{
   rmmSymbolTableLink(symbolSource1, thisSymbolDoesntExist);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_loadAllSymbolsAndTest),
      rmmMakeTest(failing_LoadNonExistantSymbol),

   };
   return rmmRunTestsCmdLine(tests, "symboltable", argc, args);
}
