#include <ryanmock.h>

static 
void futBool(bool value)
{
   rmmParamCheck(value);
}

int futReturnInt(void)
{
   rmmMockReturn(int);
}

void futInt(int value)
{
   rmmParamCheck(value);
}

void futOutInt(int *outValue)
{
   rmmMockParam(outValue, 1);
}

void futOutputMemory(void * memory, size_t size)
{
   rmmMockParamVoid(memory, size);
}

struct output_data
{
   int   a;
   char  b;
   float c;
};

struct output_data futReturnStruct(void)
{
   rmmMockReturn(struct output_data);
}

void futOutputStruct(struct output_data * data)
{
   rmmMockParam(data, 1);
}

const char * futReturnConstString(void)
{
   rmmMockReturn(const char *);
}

void * futReturnVoid(size_t size)
{
   rmmMockReturnVoidArray(size);
}

uint8_t * futReturnUInt8(size_t size)
{
   rmmMockReturnArray(uint8_t, size);
}

void futCheckAndOutput(void * data, size_t size)
{
   rmmParamCheck(data);
   rmmMockParamVoid(data, size);
}

void failing_ExpectedFunctionNotCalled(void)
{
   rmmMockBegin(futBool);
      rmmExpectTrue(value);
   rmmMockEnd();
}

void failing_ExpectedFunctionCalledTooManyTimes(void)
{
   rmmMockBegin(futBool);
      rmmExpectTrue(value);
   rmmMockEnd();

   // FUT
   futBool(true);
   // FUT
   futBool(true);
}

void passing_FUTReturnsInt(void)
{
   rmmMockBegin(futReturnInt);
   rmmMockEndReturnInt(-2);
   
   // FUT
   rmmAssertIntEqual(futReturnInt(), -2);
}

void failing_CheckCalledWithoutExpect(void)
{
   // FUT
   futBool(true);
}

void failing_MockFunctionNotCalled(void)
{
   rmmMockBegin(futReturnInt);
   rmmMockEndReturnInt(5);
}

void failing_MockFunctionCalledTooManyTimes(void)
{
   rmmMockBegin(futReturnInt);
   rmmMockEndReturnInt(5);

   // FUT
   futReturnInt();
   // FUT
   futReturnInt();
}

void failing_CheckCalledWithoutMock(void)
{
   // FUT
   futReturnInt();
}

void failing_MockBeginWithoutEnd(void)
{
   rmmMockBegin(futReturnInt);
   rmmMockBegin(futReturnInt);
}

void failing_MockEndWithoutBegin(void)
{
   rmmMockEnd();
}

void failing_EndTestWithoutMockEnd(void)
{
   rmmMockBegin(futReturnInt);
}

void passing_MockParamOutput(void)
{
   int output;
   rmmMockBegin(futOutInt);
      rmmWillOutputInt(outValue, 34);
   rmmMockEnd();

   // FUT
   futOutInt(&output);
   
   rmmAssertIntEqual(output, 34);
}

void failing_MockParamFunctionNotCalled(void)
{
   rmmMockBegin(futOutInt);
      rmmWillOutputInt(outValue, 6);
   rmmMockEnd();
}

void failing_MockParamFunctionCalledTooManyTimes(void)
{
   int output;
   rmmMockBegin(futOutInt);
      rmmWillOutputInt(outValue, -3);
   rmmMockEnd();

   // FUT
   futOutInt(&output);
   // FUT
   futOutInt(&output);
}

void failing_CheckCalledWithoutMockParam(void)
{
   int output;
   
   // FUT
   futOutInt(&output);
}

void passing_rmmExpectAssert(void)
{
   rmmMockBegin(futBool);
      rmmExpectTrue(value);
   rmmMockEnd();

   // FUT
   futBool(true);
}

void failing_rmmExpectAssert(void)
{
   rmmMockBegin(futBool);
      rmmExpectTrue(value);
   rmmMockEnd();

   // FUT
   futBool(false);
}

void passing_rmmExpectIntEqual(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 5);
   rmmMockEnd();

   // FUT
   futInt(5);
}

void failing_rmmExpectIntEqual(void)
{
   rmmMockBegin(futInt);
      rmmExpectIntEqual(value, 6);
   rmmMockEnd();

   // FUT
   futInt(7);
}

#define LENGTH 5
void passing_rmmWillOutputMemory(void)
{
   int memory[LENGTH]      = {1, 2, 3, 4, 5};
   int resetMemory[LENGTH] = {6, 7, 8, 9, 10};
   int outMemory[LENGTH];


   rmmMockBegin(futOutputMemory);
      rmmWillOutputMemory(memory, memory, LENGTH);
   rmmMockEnd();
   
   // FUT
   memcpy(outMemory, resetMemory, sizeof(int) * LENGTH);
   futOutputMemory(outMemory, LENGTH * sizeof(int));
   rmmAssertMemoryEqual(memory, outMemory, LENGTH);

   // Partail memory fill
   rmmMockBegin(futOutputMemory);
      rmmWillOutputMemory(memory, memory, LENGTH - 1);
   rmmMockEnd();

   
   // FUT
   memcpy(outMemory, resetMemory, sizeof(int) * LENGTH);
   futOutputMemory(outMemory, LENGTH * sizeof(int));
   rmmAssertMemoryEqual(memory, outMemory, LENGTH - 1);
   
   // No Memory Fill2
   rmmMockBegin(futOutputMemory);
      rmmWillOutputNothing(memory);
   rmmMockEnd();
   
   // FUT
   memcpy(outMemory, resetMemory, sizeof(int) * LENGTH);
   futOutputMemory(outMemory, LENGTH * sizeof(int));
   rmmAssertMemoryEqual(resetMemory, outMemory, LENGTH);
   
}

void failing_rmmWillOutputMemory(void)
{
   int badMemory[LENGTH] = {1, 2, 2, 4, 5};
   int memory[LENGTH] = {1, 2, 3, 4, 5};
   int outMemory[LENGTH];

   rmmMockBegin(futOutputMemory);
      rmmWillOutputMemory(memory, badMemory, LENGTH);
   rmmMockEnd();
   
   // FUT
   futOutputMemory(outMemory, LENGTH * sizeof(int));
   rmmAssertMemoryEqual(memory, outMemory, LENGTH);
}

void failing_rmmWillOutputMemoryWrongSize(void)
{
   int badMemory[LENGTH] = {1, 2, 2, 4, 5};
   int outMemory[LENGTH];

   rmmMockBegin(futOutputMemory);
      rmmWillOutputMemory(memory, badMemory, LENGTH);
   rmmMockEnd();
   
   // FUT
   futOutputMemory(outMemory, LENGTH);
}


void passing_rmmMockEndReturnStruct(void)
{
   struct output_data ret;
   struct output_data data = {
      .a = 5,
      .b = 3,
      .c = 6.34f
   };

   rmmMockBegin(futReturnStruct);
   rmmMockEndReturnMemory(&data, 1);

   // FUT
   ret = futReturnStruct();

   rmmAssertIntEqual(ret.a, 5);
   rmmAssertIntEqual(ret.b, 3);
   rmmAssertDoubleBetween(ret.c, 6, 7);
}

void failing_rmmMockEndReturnStruct(void)
{
   struct output_data ret;
   struct output_data data = {
      .a = 5,
      .b = 3,
      .c = 6.34f
   };

   rmmMockBegin(futReturnStruct);
   rmmMockEndReturnMemory(&data, 1);

   // FUT
   ret = futReturnStruct();

   rmmAssertIntEqual(ret.a, 6);
}

void passing_rmmWillOutputStruct(void)
{
   struct output_data ret;
   struct output_data data = {
      .a = 5,
      .b = 3,
      .c = 6.34f
   };

   rmmMockBegin(futOutputStruct);
      rmmWillOutputMemory(data, &data, 1);
   rmmMockEnd();

   // FUT
   futOutputStruct(&ret);

   rmmAssertIntEqual(ret.a, 5);
   rmmAssertIntEqual(ret.b, 3);
   rmmAssertDoubleBetween(ret.c, 6, 7);
}

void failing_rmmWillOutputStruct(void)
{
   struct output_data ret;
   struct output_data data = {
      .a = 5,
      .b = 4,
      .c = 7.34f
   };

   rmmMockBegin(futOutputStruct);
      rmmWillOutputMemory(data, &data, 1);
   rmmMockEnd();

   // FUT
   futOutputStruct(&ret);

   rmmAssertDoubleBetween(ret.c, 6, 7);
}

void passing_rmmMockEndReturnConstString(void)
{
   rmmMockBegin(futReturnConstString);
   rmmMockEndReturnPtr("The String");

   // FUT
   rmmAssertStringEqual("The String", futReturnConstString());
}

void failing_rmmMockEndReturnConstString(void)
{
   rmmMockBegin(futReturnConstString);
   rmmMockEndReturnPtr("Not The String");

   // FUT
   rmmAssertStringEqual("The String", futReturnConstString());
}

void passing_checkAndOutput(void)
{
   int mockedData[LENGTH] = {5, 4, 3, 2, 1}; 
   int output[LENGTH];
   rmmMockBegin(futCheckAndOutput);
      rmmExpectPtrEqual(data, &output);
      rmmWillOutputMemory(data, mockedData, LENGTH);
   rmmMockEnd();

   // FUT
   futCheckAndOutput(&output, LENGTH * sizeof(int));

   rmmAssertMemoryEqual(mockedData, output, LENGTH);
}

void failing_checkAndOutput(void)
{
   int mockedData[LENGTH]   = {5, 4, 3, 2, 1}; 
   int expectedData[LENGTH] = {1, 2, 3, 4, 5};
   int output[LENGTH];
   rmmMockBegin(futCheckAndOutput);
      rmmExpectPtrEqual(data, &output);
      rmmWillOutputMemory(data, mockedData, LENGTH);
   rmmMockEnd();

   // FUT
   futCheckAndOutput(&output, LENGTH * sizeof(int));

   rmmAssertMemoryEqual(expectedData, output, LENGTH);
}

void passing_rmmMockBeginCountReturnInt(void)
{
   rmmMockBeginCount(futReturnInt, 3);
   rmmMockEndReturnInt(1);
   
   // FUT
   rmmAssertIntEqual(futReturnInt(), 1);
   rmmAssertIntEqual(futReturnInt(), 1);
   rmmAssertIntEqual(futReturnInt(), 1);
}

void failing_rmmMockBeginCountReturnInt(void)
{
   rmmMockBeginCount(futReturnInt, 3);
   rmmMockEndReturnInt(1);
   
   // FUT
   rmmAssertIntEqual(futReturnInt(), 1);
   rmmAssertIntEqual(futReturnInt(), 7);
   rmmAssertIntEqual(futReturnInt(), 1);
}

void passing_ReturnMemory(void)
{
   uint8_t bytes[5] = {1, 2, 3, 4, 5 };
   uint8_t * output;
   rmmMockBegin(futReturnVoid);
   rmmMockEndReturnMemory(bytes, 5);
   
   output = futReturnVoid(5);
   
   rmmAssertMemoryEqual(output, bytes, 5);
   
   rmmMockBegin(futReturnUInt8);
   rmmMockEndReturnMemory(bytes, 5);
   
   output = futReturnUInt8(5);
   rmmAssertMemoryEqual(output, bytes, 5);
   
}

void failing_ReturnMemoryBadSize(void)
{
   uint8_t bytes[5] = {1, 2, 3, 4, 5 };
   uint8_t * output;
   
   rmmMockBegin(futReturnUInt8);
   rmmMockEndReturnMemory(bytes, 5);
   
   output = futReturnUInt8(4);
   rmmAssertMemoryEqual(output, bytes, 5);
   
}

void passing_rmmMockBeginAnyWithNoCalls(void)
{
   rmmMockBeginAny(futInt);
      rmmExpectAny(value);
   rmmMockEnd();
}

void passing_rmmMockBeginAnyWithSomeCalls(void)
{
   rmmMockBeginAny(futInt);
      rmmExpectAny(value);
   rmmMockEnd();
   
   futInt(1);
   futInt(2);
   futInt(3);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(failing_ExpectedFunctionNotCalled),
      rmmMakeTest(failing_ExpectedFunctionCalledTooManyTimes),
      rmmMakeTest(failing_CheckCalledWithoutExpect),
      rmmMakeTest(passing_FUTReturnsInt),
      rmmMakeTest(failing_MockFunctionNotCalled),
      rmmMakeTest(failing_MockFunctionCalledTooManyTimes),
      rmmMakeTest(failing_CheckCalledWithoutMock),
      rmmMakeTest(failing_MockBeginWithoutEnd),
      rmmMakeTest(failing_MockEndWithoutBegin),
      rmmMakeTest(failing_EndTestWithoutMockEnd),
      rmmMakeTest(passing_MockParamOutput),
      rmmMakeTest(failing_MockParamFunctionNotCalled),
      rmmMakeTest(failing_MockParamFunctionCalledTooManyTimes),
      rmmMakeTest(failing_CheckCalledWithoutMockParam),
      rmmMakeTest(passing_rmmExpectAssert),
      rmmMakeTest(failing_rmmExpectAssert),
      rmmMakeTest(passing_rmmExpectIntEqual),
      rmmMakeTest(failing_rmmExpectIntEqual),
      rmmMakeTest(passing_rmmWillOutputMemory),
      rmmMakeTest(failing_rmmWillOutputMemory),
      rmmMakeTest(failing_rmmWillOutputMemoryWrongSize),
      rmmMakeTest(passing_rmmMockEndReturnStruct),
      rmmMakeTest(failing_rmmMockEndReturnStruct),
      rmmMakeTest(passing_rmmWillOutputStruct),
      rmmMakeTest(failing_rmmWillOutputStruct),
      rmmMakeTest(passing_rmmMockEndReturnConstString),
      rmmMakeTest(failing_rmmMockEndReturnConstString),
      rmmMakeTest(passing_checkAndOutput),
      rmmMakeTest(failing_checkAndOutput),
      rmmMakeTest(passing_rmmMockBeginCountReturnInt),
      rmmMakeTest(failing_rmmMockBeginCountReturnInt),
      rmmMakeTest(passing_ReturnMemory),
      rmmMakeTest(failing_ReturnMemoryBadSize),
      rmmMakeTest(passing_rmmMockBeginAnyWithNoCalls),
      rmmMakeTest(passing_rmmMockBeginAnyWithSomeCalls),
   };
   return rmmRunTestsCmdLine(tests, "mocks", argc, args);
}
