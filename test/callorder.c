#include <ryanmock.h>

void futFunction1(void)
{
   rmmFunctionCalled();
}
void futFunction2(void)
{
   rmmFunctionCalled();
}
void futFunction3(void)
{
   rmmFunctionCalled();
}


void passing_rmmExpectOrdered(void)
{
   
   rmmMockBegin(futFunction1);
      rmmExpectOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();

   // FUT
   futFunction1();
   futFunction2();

   rmmMockBeginOrdered(futFunction1);
   rmmMockEnd();
   rmmMockBeginOrdered(futFunction2);
   rmmMockEnd();
   
   // FUT
   futFunction1();
   futFunction2();
}

void failing_rmmExpectOrdered(void)
{
   
   rmmMockBegin(futFunction1);
      rmmExpectOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();

   // FUT
   futFunction2();
   futFunction1();
}

void passing_rmmIgnoreOrdered1(void)
{
   
   rmmMockBegin(futFunction1);
      rmmIgnoreOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();

   // FUT
   futFunction1();
   futFunction2();
   
   rmmMockBeginIgnored(futFunction1);
   rmmMockEnd();
   rmmMockBeginOrdered(futFunction2);
   rmmMockEnd();

   // FUT
   futFunction1();
   futFunction2();
}

void failing_rmmIgnoreOrdered(void)
{
   rmmMockBegin(futFunction1);
      rmmIgnoreOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();

   // FUT
   futFunction1();
   futFunction2();
   futFunction1();
}

void passing_rmmIgnoreOrdered2(void)
{
   rmmMockBegin(futFunction1);
      rmmIgnoreOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();

   // FUT
   futFunction2();
   futFunction1();
   
   rmmMockBeginIgnored(futFunction1);
   rmmMockEnd();
   rmmMockBeginOrdered(futFunction2);
   rmmMockEnd();

   // FUT
   futFunction2();
   futFunction1();
}

void failing_rmmIgnoreOrderedBackwards(void)
{
   // For now at least, you have to have all your ignores up top, or in order.
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction1);
      rmmIgnoreOrdered();
   rmmMockEnd();

   // FUT
   futFunction1();
   futFunction2();
}

void passing_rmmMockBeginCount()
{
   rmmMockBeginCount(futFunction1, 2);
      rmmExpectOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();
   rmmMockBeginCount(futFunction3, 4);
      rmmExpectOrdered();
   rmmMockEnd();
   
   // FUT
   futFunction1();
   futFunction1();
   futFunction2();
   futFunction3();
   futFunction3();
   futFunction3();
   futFunction3();
   
   rmmMockBeginOrderedCount(futFunction1, 2);
   rmmMockEnd();
   rmmMockBeginOrdered(futFunction2);
   rmmMockEnd();
   rmmMockBeginOrderedCount(futFunction3, 4);
   rmmMockEnd();
   
   // FUT
   futFunction1();
   futFunction1();
   futFunction2();
   futFunction3();
   futFunction3();
   futFunction3();
   futFunction3();
}

void failing_rmmMockBeginCount()
{
   rmmMockBeginCount(futFunction1, 2);
      rmmExpectOrdered();
   rmmMockEnd();
   rmmMockBegin(futFunction2);
      rmmExpectOrdered();
   rmmMockEnd();
   rmmMockBeginCount(futFunction3, 4);
      rmmExpectOrdered();
   rmmMockEnd();
   
   // FUT
   futFunction1();
   futFunction1();
   futFunction3();
   futFunction2();
   futFunction3();
   futFunction3();
   futFunction3();
}

void passing_ignoringFunctionCall()
{
   rmmMockBeginAll(futFunction1);
      rmmIgnoreOrdered();
   rmmMockEnd();
   rmmMockBeginCount(futFunction2, 2);
      rmmExpectOrdered();
   rmmMockEnd();
   
   // FUT
   futFunction1();
   futFunction1();
   futFunction2();
   futFunction1();
   futFunction1();
   futFunction2();
   futFunction1();
   futFunction1();

   rmmFUTCalled();
   
   rmmMockBeginIgnoredAll(futFunction1);
   rmmMockEnd();
   rmmMockBeginOrderedCount(futFunction2, 2);
   rmmMockEnd();
   
   // FUT
   futFunction1();
   futFunction1();
   futFunction2();
   futFunction1();
   futFunction1();
   futFunction2();
   futFunction1();
   futFunction1();
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_rmmExpectOrdered),
      rmmMakeTest(failing_rmmExpectOrdered),
      rmmMakeTest(passing_rmmIgnoreOrdered1),
      rmmMakeTest(failing_rmmIgnoreOrdered),
      rmmMakeTest(passing_rmmIgnoreOrdered2),
      rmmMakeTest(failing_rmmIgnoreOrderedBackwards),
      rmmMakeTest(passing_rmmMockBeginCount),
      rmmMakeTest(failing_rmmMockBeginCount),
      rmmMakeTest(passing_ignoringFunctionCall),
   };
   return rmmRunTestsCmdLine(tests, "callorder", argc, args);
}

