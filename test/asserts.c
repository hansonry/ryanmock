#include <ryanmock.h>

#define MAKE_PTR(type, value) ((type *)(uintptr_t)(value))

void failing_rmmFail(void)
{
   rmmFail("You did an error :P");
}

void passing_rmmAssertTrue(void)
{
   rmmAssertTrue(1);
   rmmAssertTrue(4);
   rmmAssertTrue(true);
}

void failing_rmmAssertTrue(void)
{
   rmmAssertTrue(0);
}

void passing_rmmAssertFalse(void)
{
   rmmAssertFalse(0);
   rmmAssertFalse(false);
}

void failing_rmmAssertFalse(void)
{
   rmmAssertFalse(true);
}


void failing_rmmAssertWithValueText(void)
{
   int someMagicNumber = 43;
   rmmAssertTrue(someMagicNumber * 0);
}


void passing_rmmAssertIntEqual(void)
{
   rmmAssertIntEqual(5, 5);
   rmmAssertIntEqual(6, 6);
}

void failing_rmmAssertIntEqual(void)
{
   rmmAssertIntEqual(5, 4);
}

void passing_rmmAssertIntNotEqual(void)
{
   rmmAssertIntNotEqual(5, 7);
   rmmAssertIntNotEqual(-3, 6);
}

void failing_rmmAssertIntNotEqual(void)
{
   rmmAssertIntNotEqual(-1, -1);
}

void passing_rmmAssertIntGreaterThan(void)
{
   rmmAssertIntGreaterThan(6, 5);
}

void failing_rmmAssertIntGreaterThan(void)
{
   rmmAssertIntGreaterThan(-3, 5);
}

void passing_rmmAssertIntGreaterThanOrEqualTo(void)
{
   rmmAssertIntGreaterThanOrEqualTo(-3, -3);
   rmmAssertIntGreaterThanOrEqualTo(-2, -3);
}

void failing_rmmAssertIntGreaterThanOrEqualTo(void)
{
   rmmAssertIntGreaterThanOrEqualTo(0, 8);
}

void passing_rmmAssertIntLessThan(void)
{
   rmmAssertIntLessThan(10, 20);
}

void failing_rmmAssertIntLessThan(void)
{
   rmmAssertIntLessThan(44, 44);
}

void passing_rmmAssertIntLessThanOrEqualTo(void)
{
   rmmAssertIntLessThanOrEqualTo(30, 30);
   rmmAssertIntLessThanOrEqualTo(10, 30);
}

void failing_rmmAssertIntLessThanOrEqualTo(void)
{
   rmmAssertIntLessThanOrEqualTo(90, 44);
}

void passing_rmmAssertIntBetween(void)
{
   rmmAssertIntBetween(5, 0, 10);
}

void failing_rmmAssertIntBetween_OORHigh(void)
{
   rmmAssertIntBetween(11, 0, 10);
}

void failing_rmmAssertIntBetween_OORLow(void)
{
   rmmAssertIntBetween(-1, 0, 10);
}

void failing_rmmAssertIntNotBetween(void)
{
   rmmAssertIntNotBetween(6, 1, 11);
}

void passing_rmmAssertIntNotBetween_OORHigh(void)
{
   rmmAssertIntNotBetween(-1, -11, -2);
}

void passing_rmmAssertIntNotBetween_OORLow(void)
{
   rmmAssertIntNotBetween(4, 5, 20);
}

void passing_rmmAssertIntWithin(void)
{
   rmmAssertIntWithin(9,  5, 5);
   rmmAssertIntWithin(0,  5, 5);
   rmmAssertIntWithin(10, 5, 5);
}

void failing_rmmAssertIntWithin_OORHigh(void)
{
   rmmAssertIntWithin(11,  5, 5);
}

void failing_rmmAssertIntWithin_OORLow(void)
{
   rmmAssertIntWithin(-1,  5, 5);
}

void passing_rmmAssertIntNotWithin_OORHigh(void)
{
   rmmAssertIntNotWithin(35, 25, 5);
}

void passing_rmmAssertIntNotWithin_OORLow(void)
{
   rmmAssertIntNotWithin(10, 25, 5);
}

void failing_rmmAssertIntNotWithin(void)
{
   rmmAssertIntNotWithin(25, 25, 5);
}

void passing_rmmAssertPtrNULL(void)
{
   int * somthing = NULL;
   rmmAssertPtrNULL(NULL);
   rmmAssertPtrNULL(somthing);
}

void failing_rmmAssertPtrNULL(void)
{
   int * a = MAKE_PTR(int, 0x1534);
   rmmAssertPtrNULL(a);
}

void passing_rmmAssertPtrNotNULL(void)
{
   
   int * a = MAKE_PTR(int, 0x1534);
   int b = 5;
   rmmAssertPtrNotNULL(a);
   rmmAssertPtrNotNULL(&b);
}

void failing_rmmAssertPtrNotNULL(void)
{
   int * a = NULL;
   rmmAssertPtrNotNULL(a);
}

void passing_rmmAssertPtrEqual(void)
{
   
   int * a = MAKE_PTR(int, 0x1534);
   int * b = MAKE_PTR(int, 0x1534);
   rmmAssertPtrEqual(a, b);
   
   a = MAKE_PTR(int, 0xAAAA);
   b = MAKE_PTR(int, 0xAAAA);
   rmmAssertPtrEqual(a, b);
}

void failing_rmmAssertPtrEqual(void)
{
   int * a = MAKE_PTR(int, 0x25);
   int * b = MAKE_PTR(int, 0x0123);

   rmmAssertPtrEqual(a, b);
}

void passing_rmmAssertPtrNotEqual(void)
{
   
   int * a = MAKE_PTR(int, 0x34);
   int * b = MAKE_PTR(int, 0x1534);
   rmmAssertPtrNotEqual(a, b);
   
   a = MAKE_PTR(int, 0xBBBB);
   b = MAKE_PTR(int, 0xAAAA);
   rmmAssertPtrNotEqual(a, b);
}

void failing_rmmAssertPtrNotEqual(void)
{
   int * a = MAKE_PTR(int, 0xABCD);
   int * b = MAKE_PTR(int, 0xABCD);

   rmmAssertPtrNotEqual(a, b);
}


void passing_rmmAssertMemoryEqual(void)
{
   int actual1[]   = {1, 2, 3, 4, 5};
   int expected1[] = {1, 2, 3, 4, 5};
   rmmAssertMemoryEqual(actual1, expected1, 
                        sizeof(expected1) / sizeof(*expected1));
}

void failing_rmmAssertMemoryEqual(void)
{
   int actual1[]   = {1, 2, 49, 4, 5};
   int expected1[] = {1, 2, 3,  4, 5};
   rmmAssertMemoryEqual(actual1, expected1, 
                        sizeof(expected1) / sizeof(*expected1));
}

void passing_rmmAssertStringEqual(void)
{
   const char * actual   = "She sells seashells by the seashore";
   const char * expected = "She sells seashells by the seashore";
   rmmAssertStringEqual(actual, expected);
}

void failing_rmmAssertStringEqual(void)
{
   const char * actual   = "The quick brown fox jumps over the lazy dog";
   const char * expected = "The quick brown fox Jumps over the lazy dog";
   rmmAssertStringEqual(actual, expected);
}

void failing_rmmAssertStringEqualEmptyString(void)
{
   const char * actual   = "The quick brown fox jumps over the lazy dog";
   const char * expected = "";
   rmmAssertStringEqual(actual, expected);
}

void failing_rmmAssertStringEqualNotSameSize(void)
{
   const char * actual   = "The quick brown fox jumps over the lazy dog";
   const char * expected = "The quick brown fox jumps ";
   rmmAssertStringEqual(actual, expected);
}

void passing_rmmAssertStringNotEqual(void)
{
   const char * actual   = "12345";
   const char * expected = "12346";
   rmmAssertStringNotEqual(actual, expected);
}

void passing_rmmAssertStringNotEqualDifferentSize(void)
{
   const char * actual   = "12345";
   const char * expected = "123456";
   rmmAssertStringNotEqual(actual, expected);
}

void failing_rmmAssertStringNotEqual(void)
{
   const char * actual   = "12345";
   const char * expected = "12345";
   rmmAssertStringNotEqual(actual, expected);
}

void passing_rmmAssertDoubleEqual(void)
{
   rmmAssertDoubleEqual(8, 8);
}

void failing_rmmAssertDoubleEqual(void)
{
   rmmAssertDoubleEqual(8.214545, 8.53343);
}

void passing_rmmAssertULong(void)
{
   rmmAssertULongEqual(7, 7);
   rmmAssertULongGreaterThan(9, 8);
   rmmAssertULongGreaterThanOrEqualTo(9, 8);
   rmmAssertULongGreaterThanOrEqualTo(9, 9);
   rmmAssertULongLessThan(3, 10);
   rmmAssertULongLessThanOrEqualTo(9, 10);
   rmmAssertULongLessThanOrEqualTo(10, 10);
}

void failing_rmmAssertULongEqual(void)
{
   rmmAssertULongEqual(4, 0xFFFF);
}

void passing_rmmAssertUShort(void)
{
   rmmAssertUShortEqual(7, 7);
   rmmAssertUShortGreaterThan(9, 8);
   rmmAssertUShortGreaterThanOrEqualTo(9, 8);
   rmmAssertUShortGreaterThanOrEqualTo(9, 9);
   rmmAssertUShortLessThan(3, 10);
   rmmAssertUShortLessThanOrEqualTo(9, 10);
   rmmAssertUShortLessThanOrEqualTo(10, 10);
}

void failing_rmmAssertUShortEqual(void)
{
   rmmAssertUShortEqual(4, 0xFFFF);
}

void passing_rmmAssertIntU8(void)
{
   rmmAssertIntU8Equal(7, 7);
   rmmAssertIntU8GreaterThan(9, 8);
   rmmAssertIntU8GreaterThanOrEqualTo(9, 8);
   rmmAssertIntU8GreaterThanOrEqualTo(9, 9);
   rmmAssertIntU8LessThan(3, 10);
   rmmAssertIntU8LessThanOrEqualTo(9, 10);
   rmmAssertIntU8LessThanOrEqualTo(10, 10);
}

void failing_rmmAssertIntU8Equal(void)
{
   rmmAssertIntU8Equal(4, 20);
}


void passing_rmmAssertBool(void)
{
   rmmAssertBoolEqual(true, true);
   rmmAssertBoolNotEqual(true, false);
}

void failing_rmmAssertBoolEqual(void)
{
   rmmAssertBoolEqual(true, false);
}

void passing_rmmAssertEnum(void)
{
   rmmAssertEnumEqual(7, 7);
   rmmAssertEnumGreaterThan(9, 8);
   rmmAssertEnumGreaterThanOrEqualTo(9, 8);
   rmmAssertEnumGreaterThanOrEqualTo(9, 9);
   rmmAssertEnumLessThan(3, 10);
   rmmAssertEnumLessThanOrEqualTo(9, 10);
   rmmAssertEnumLessThanOrEqualTo(10, 10);
}

void failing_rmmAssertEnumEqual(void)
{
   rmmAssertEnumEqual(4, 20);
}

void passing_rmmAssertIntU16(void)
{
   rmmAssertIntU16Equal(7, 7);
   rmmAssertIntU16GreaterThan(9, 8);
   rmmAssertIntU16GreaterThanOrEqualTo(9, 8);
   rmmAssertIntU16GreaterThanOrEqualTo(9, 9);
   rmmAssertIntU16LessThan(3, 10);
   rmmAssertIntU16LessThanOrEqualTo(9, 10);
   rmmAssertIntU16LessThanOrEqualTo(10, 10);
}

void failing_rmmAssertIntU32Equal(void)
{
   rmmAssertIntU32Equal(4, 20);
}

void passing_rmmAssertIntU32(void)
{
   rmmAssertIntU32Equal(7, 7);
   rmmAssertIntU32GreaterThan(9, 8);
   rmmAssertIntU32GreaterThanOrEqualTo(9, 8);
   rmmAssertIntU32GreaterThanOrEqualTo(9, 9);
   rmmAssertIntU32LessThan(3, 10);
   rmmAssertIntU32LessThanOrEqualTo(9, 10);
   rmmAssertIntU32LessThanOrEqualTo(10, 10);
}

void failing_rmmAssertIntU16Equal(void)
{
   rmmAssertIntU16Equal(4, 20);
}

void passing_rmmAssertInt32(void)
{
   rmmAssertInt32Equal(7, 7);
   rmmAssertInt32GreaterThan(9, 8);
   rmmAssertInt32GreaterThanOrEqualTo(9, 8);
   rmmAssertInt32GreaterThanOrEqualTo(9, 9);
   rmmAssertInt32LessThan(3, 10);
   rmmAssertInt32LessThanOrEqualTo(9, 10);
   rmmAssertInt32LessThanOrEqualTo(10, 10);
}

void failing_rmmAssertInt32Equal(void)
{
   rmmAssertInt32Equal(4, 20);
}


#ifdef UINT64_MAX
void passing_rmmAssertIntU64(void)
{
   rmmAssertIntU64Equal(7, 7);
   rmmAssertIntU64GreaterThan(9, 8);
   rmmAssertIntU64GreaterThanOrEqualTo(9, 8);
   rmmAssertIntU64GreaterThanOrEqualTo(9, 9);
   rmmAssertIntU64LessThan(3, 10);
   rmmAssertIntU64LessThanOrEqualTo(9, 10);
   rmmAssertIntU64LessThanOrEqualTo(10, 10);
}

void failing_rmmAssertIntU64Equal(void)
{
   rmmAssertIntU64Equal(4, 20);
}
#endif // UINT64_MAX

void passing_rmmAssertIntPtrU(void)
{
   rmmAssertIntPtrUEqual(7, 7);
   rmmAssertIntPtrUGreaterThan(9, 8);
   rmmAssertIntPtrUGreaterThanOrEqualTo(9, 8);
   rmmAssertIntPtrUGreaterThanOrEqualTo(9, 9);
   rmmAssertIntPtrULessThan(3, 10);
   rmmAssertIntPtrULessThanOrEqualTo(9, 10);
   rmmAssertIntPtrULessThanOrEqualTo(10, 10);
}

void failing_rmmAssertIntPtrUEqual(void)
{
   rmmAssertIntPtrUEqual(4, 20);
}

void passing_rmmAssertIntPtr(void)
{
   rmmAssertIntPtrEqual(7, 7);
   rmmAssertIntPtrGreaterThan(9, 8);
   rmmAssertIntPtrGreaterThanOrEqualTo(9, 8);
   rmmAssertIntPtrGreaterThanOrEqualTo(9, 9);
   rmmAssertIntPtrLessThan(3, 10);
   rmmAssertIntPtrLessThanOrEqualTo(9, 10);
   rmmAssertIntPtrLessThanOrEqualTo(10, 10);
}

void failing_rmmAssertIntPtrEqual(void)
{
   rmmAssertIntPtrEqual(4, 20);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(failing_rmmFail),
      rmmMakeTest(passing_rmmAssertTrue),
      rmmMakeTest(failing_rmmAssertTrue),
      rmmMakeTest(passing_rmmAssertFalse),
      rmmMakeTest(failing_rmmAssertFalse),
      rmmMakeTest(failing_rmmAssertWithValueText),
      rmmMakeTest(passing_rmmAssertIntEqual),
      rmmMakeTest(failing_rmmAssertIntEqual),
      rmmMakeTest(passing_rmmAssertIntNotEqual),
      rmmMakeTest(failing_rmmAssertIntNotEqual),
      rmmMakeTest(passing_rmmAssertIntGreaterThan),
      rmmMakeTest(failing_rmmAssertIntGreaterThan),
      rmmMakeTest(passing_rmmAssertIntGreaterThanOrEqualTo),
      rmmMakeTest(failing_rmmAssertIntGreaterThanOrEqualTo),
      rmmMakeTest(passing_rmmAssertIntLessThan),
      rmmMakeTest(failing_rmmAssertIntLessThan),
      rmmMakeTest(passing_rmmAssertIntLessThanOrEqualTo),
      rmmMakeTest(failing_rmmAssertIntLessThanOrEqualTo),
      rmmMakeTest(passing_rmmAssertIntBetween),
      rmmMakeTest(failing_rmmAssertIntBetween_OORHigh),
      rmmMakeTest(failing_rmmAssertIntBetween_OORLow),
      rmmMakeTest(failing_rmmAssertIntNotBetween),
      rmmMakeTest(passing_rmmAssertIntNotBetween_OORHigh),
      rmmMakeTest(passing_rmmAssertIntNotBetween_OORLow),
      rmmMakeTest(passing_rmmAssertIntWithin),
      rmmMakeTest(failing_rmmAssertIntWithin_OORHigh),
      rmmMakeTest(failing_rmmAssertIntWithin_OORLow),
      rmmMakeTest(passing_rmmAssertIntNotWithin_OORHigh),
      rmmMakeTest(passing_rmmAssertIntNotWithin_OORLow),
      rmmMakeTest(failing_rmmAssertIntNotWithin),
      rmmMakeTest(passing_rmmAssertPtrNULL),
      rmmMakeTest(failing_rmmAssertPtrNULL),
      rmmMakeTest(passing_rmmAssertPtrNotNULL),
      rmmMakeTest(failing_rmmAssertPtrNotNULL),
      rmmMakeTest(passing_rmmAssertPtrEqual),
      rmmMakeTest(failing_rmmAssertPtrEqual),
      rmmMakeTest(passing_rmmAssertPtrNotEqual),
      rmmMakeTest(failing_rmmAssertPtrNotEqual),
      rmmMakeTest(passing_rmmAssertMemoryEqual),
      rmmMakeTest(failing_rmmAssertMemoryEqual),
      rmmMakeTest(passing_rmmAssertStringEqual),
      rmmMakeTest(failing_rmmAssertStringEqual),
      rmmMakeTest(failing_rmmAssertStringEqualEmptyString),
      rmmMakeTest(failing_rmmAssertStringEqualNotSameSize),
      rmmMakeTest(passing_rmmAssertStringNotEqual),
      rmmMakeTest(passing_rmmAssertStringNotEqualDifferentSize),
      rmmMakeTest(failing_rmmAssertStringNotEqual),
      rmmMakeTest(passing_rmmAssertDoubleEqual),
      rmmMakeTest(failing_rmmAssertDoubleEqual),
      rmmMakeTest(passing_rmmAssertULong),
      rmmMakeTest(failing_rmmAssertULongEqual),
      rmmMakeTest(passing_rmmAssertUShort),
      rmmMakeTest(failing_rmmAssertUShortEqual),
      rmmMakeTest(passing_rmmAssertIntU8),
      rmmMakeTest(failing_rmmAssertIntU8Equal),
      rmmMakeTest(passing_rmmAssertBool),
      rmmMakeTest(failing_rmmAssertBoolEqual),
      rmmMakeTest(passing_rmmAssertEnum),
      rmmMakeTest(failing_rmmAssertEnumEqual),
      rmmMakeTest(passing_rmmAssertIntU16),
      rmmMakeTest(failing_rmmAssertIntU16Equal),
      rmmMakeTest(passing_rmmAssertIntU32),
      rmmMakeTest(failing_rmmAssertIntU32Equal),
      rmmMakeTest(passing_rmmAssertInt32),
      rmmMakeTest(failing_rmmAssertInt32Equal),
#ifdef UINT64_MAX
      rmmMakeTest(passing_rmmAssertIntU64),
      rmmMakeTest(failing_rmmAssertIntU64Equal),
#endif // UINT64_MAX
      rmmMakeTest(passing_rmmAssertIntPtrU),
      rmmMakeTest(failing_rmmAssertIntPtrUEqual),
      rmmMakeTest(passing_rmmAssertIntPtr),
      rmmMakeTest(failing_rmmAssertIntPtrEqual),
   };
   return rmmRunTestsCmdLine(tests, "asserts", argc, args);
}
