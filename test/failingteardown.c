#include <ryanmock.h>

static 
void suiteState(enum ryanmock_state state, const struct ryanmock_test * test)
{
   (void)test;
   if(state == eRMS_SuiteSetup)
   {
      rmmAssertIntEqual(5, 5);
   }
   else if(state == eRMS_SuiteTeardown)
   {
      rmmAssertIntEqual(7, 0);
   }
}

void passing_test(void)
{
   rmmAssertIntEqual(6, 6);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_test),
   };
   return rmmRunTestsCmdLine2(tests, "failingteardown", 
                              suiteState, 
                              argc, args);
}
