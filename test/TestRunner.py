#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import os
import subprocess
import sys
import math

def GetNativeResults():
   result = subprocess.run(args=["./native"], capture_output=True, universal_newlines=True)
   lines = result.stdout.splitlines()
   bitWidth  = int(lines[0], 0)
   null      = lines[1]
   pointer   = lines[2]
   prefix0x  = pointer.startswith("0x")
   hexDigits = int(math.floor((bitWidth + 3) / 4))
   expectedPaddedSize = hexDigits + (2 if prefix0x else 0)
   padded = (len(pointer) == expectedPaddedSize)
   upperCase = pointer.endswith("A")
 
   return { 
      'bitWidth':  bitWidth, 
      'null':      null, 
      'prefix0x':  prefix0x,
      'hexDigits': hexDigits,
      'padded':    padded,
      'upperCase': upperCase
   }

# Caching NativeResults because it may take some time and it (hopefully) 
# never changes
NativeResults = GetNativeResults()

def path(path):
    pathStr = GetStringFromNode(path)
    return str(os.path.normpath(pathStr))

def PrefixEachLineInNodeWith(node, testStats, prefixText):
   if node.text == None:
      return ""
   else:
      lines = GetStringFromNode(node, testStats).splitlines()
      prefixedLines = []
      for l in lines:
         prefixedLines.append(prefixText + l)
      return "\n".join(prefixedLines) + "\n"


def FormatAddress(addressValue):
   addressValue = int(addressValue)
   if addressValue == 0:
      return NativeResults['null']
   addressString = ""
   if NativeResults['upperCase']:
      addressString = addressString + "{:X}".format(addressValue)
   else:
      addressString = addressString + "{:x}".format(addressValue)
   if NativeResults['padded']:
      addressString = addressString.zfill(NativeResults['hexDigits'])
   if NativeResults['prefix0x']:
      addressString = "0x" + addressString
   return addressString
    

def CreateNewTestStats():
    return {
        'pass':  0,
        'fail':  0,
        'error': 0
    }

def GetStringFromNode(node, testStats = None):
    if testStats == None:
        testStats = CreateNewTestStats()
    if node.text != None:
        nodeStr = str(node.text)
    else:
        nodeStr = ''
    for element in node:
        if element.tag == 'path':
            nodeStr = nodeStr + path(element)
        elif element.tag == 'cwd':
            nodeStr = nodeStr + str(os.getcwd()) + '/'
        elif element.tag == 'address':
            nodeStr = nodeStr + FormatAddress(int(element.text, 0))
        elif element.tag == 'null':
            nodeStr = nodeStr + FormatAddress(0)
        elif element.tag == 'testHelper':
            testName = element.attrib['name']
            thisTestStats = CreateNewTestStats()
            testBody = GetStringFromNode(element, thisTestStats)
            testCount = (thisTestStats['pass'] + thisTestStats['fail'] + 
                         thisTestStats['error'])
            nodeStr = nodeStr + f"[==========] Running {testCount} test(s) from {testName}."
            nodeStr = nodeStr + testBody
            nodeStr = nodeStr + f"[==========] {testCount} test(s) run."
            if thisTestStats['pass'] > 0:
                nodeStr = nodeStr + f"\n[  PASSED  ] {thisTestStats['pass']} test(s)."
            if thisTestStats['error'] > 0:
                nodeStr = nodeStr + f"\n[  ERROR   ] {thisTestStats['error']} test(s)."
            if thisTestStats['fail'] > 0:
                nodeStr = nodeStr + f"\n[  FAILED  ] {thisTestStats['fail']} test(s)."
            
        elif element.tag == 'pass':
            testName = element.attrib['name']
            nodeStr = nodeStr + f"[ RUN      ] {testName}\n"
            nodeStr = nodeStr + f"[       OK ] {testName}"
            testStats['pass'] = testStats['pass'] + 1
        elif element.tag == 'fail':
            testName = element.attrib['name']
            nodeStr = nodeStr + f"[ RUN      ] {testName}\n"
            nodeStr = nodeStr + PrefixEachLineInNodeWith(element, testStats,
                                                         "[     FAIL ] ")
            nodeStr = nodeStr + f"[  FAILURE ] {testName}"
            testStats['fail'] = testStats['fail'] + 1
        elif element.tag == 'error':
            testName = element.attrib['name']
            nodeStr = nodeStr + f"[ RUN      ] {testName}\n"
            nodeStr = nodeStr + PrefixEachLineInNodeWith(element, testStats,
                                                         "[  ERROR   ] ")
            nodeStr = nodeStr + f"[  ERROR   ] {testName}"
            testStats['error'] = testStats['error'] + 1
        else:
            print("Unknown tag: " + element.tag)
        if element.tail != None:
            nodeStr = nodeStr + element.tail
    return nodeStr

def getXMLBooleanAttribute(attribList, attributeName, defaultValue = False):
    if not attributeName in attribList:
        return defaultValue
    attribText = attribList[attributeName]
    if attribText == 'true':
        return True
    if attribText == 'false':
        return False
    if not attribText.isnumeric():
        return defaultValue
    return int(attribText) != 0

def parseExpectedXML(xmlFilename):
    
    tree = ET.parse(xmlFilename)
    root = tree.getroot()
   
    data = {}  
  
    expectedStr = GetStringFromNode(root)

    data['returnCode'] = int(root.attrib['exitCode'])
    
    if 'params' in root.attrib:
       data['params'] = root.attrib['params'].split(' ')
    else:
       data['params'] = []

    data['ignoreInfoLines'] = getXMLBooleanAttribute(root.attrib, 'ignoreInfoLines')
    
    data['lines'] = expectedStr.splitlines()[1:]

    return data


def runProgramUnderTest(programName, configArgs): 
    #commandStr = './' + programName
    commandStr = programName 
    #print("Running: " + commandStr)
    args = [commandStr, "--color", "false"] + configArgs

    result = subprocess.run(args=args, capture_output=True, universal_newlines=True)

    resultLines = result.stdout.splitlines()
    return {'lines': resultLines, 'returnCode': result.returncode}

def compareExpectedToActual(expected, actual, ignoreInfoLines, testName):
    resultIndex   = 0
    expectedIndex = 0
    reportedTest  = False
    while (resultIndex < len(actual['lines']) and
           expectedIndex < len(expected['lines'])):
           
        # Ignore info lines if we are ignoring info lines
        while (ignoreInfoLines and resultIndex < len(actual['lines']) and
               actual['lines'][resultIndex].startswith("[     INFO ]")):
               resultIndex = resultIndex + 1
        resultLine = actual['lines'][resultIndex]
        expectedLine = expected['lines'][expectedIndex]
        
        if resultLine != expectedLine:
            if not reportedTest:
                reportedTest = True
                print("Test: {}".format(testName))
            print("Line Diffrence actual {}, expected {}:".format(resultIndex + 1, expectedIndex + 1))
            print('   actual: ' + resultLine)
            print(' expected: ' + expectedLine)
            sys.exit(1)
        resultIndex   = resultIndex   + 1
        expectedIndex = expectedIndex + 1

    if resultIndex < len(actual['lines']):
        print("Actual Results longer than Expected Results line: {}".format(resultIndex + 1))
        print('   actual: ' + actual['lines'][resultIndex])
        print(' expected: ')
        sys.exit(1)
        
    elif expectedIndex < len(expected['lines']):
        index = len(actual['lines'])
        print("Expected Results longer than Actual Results line: {}".format(expectedIndex + 1))
        print('   actual: ')
        print(' expected: ' + expected['lines'][expectedIndex])
        sys.exit(1)
        

    if actual['returnCode'] != expected['returnCode']:
        print("Expected Return Code: {} Does not match return code: {}".format(expected['returnCode'], actual['returnCode']))
        sys.exit(1)

def RunTest(xmlExpectedFilename, programUnderTestName):
    expected = parseExpectedXML(xmlExpectedFilename)
    actual = runProgramUnderTest(programUnderTestName, expected['params'])
    compareExpectedToActual(expected, actual, expected['ignoreInfoLines'], programUnderTestName)
    
    
if __name__ == "__main__":
    if len(sys.argv) == 3:
        programUnderTestName = sys.argv[1]
        xmlExpectedFilename  = sys.argv[2]
        RunTest(xmlExpectedFilename, programUnderTestName)
    else:
        print('')
        print('{} [programToTest] [expectedXML]'.format(sys.argv[0]))
        print('')
 
