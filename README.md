# ryanmock

![pipeline status](https://gitlab.com/hansonry/ryanmock/badges/master/pipeline.svg)

An Open Source C Unit Testing and Mocking Library

## Example

Source:

```c
#include <ryanmock.h>

static void passing_test(void)
{
   rmmAssertIntEqual(5, 5);
}

static void failing_test(void)
{
   rmmAssertTrue(3 == 9);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(passing_test),
      rmmMakeTest(failing_test),
   };
   return rmmRunTestsCmdLine(tests, NULL, argc, args);
}
```

Result:

```
[==========] Running 2 test(s) from example_simple.
[ RUN      ] passing_test
[       OK ] passing_test
[ RUN      ] failing_test
[     FAIL ] Actual(false {3 == 9}) is not True!
[     INFO ] Actual(false {3 == 9} <Boolean>) Stack Trace:
[     INFO ]    Frame:   0, Line:    15, File: /home/ryan/projects/ryanmock/example/simple.c, Call: rmmAssertTrue
[     INFO ] Expected Stack Trace:
[     INFO ]    Frame:   0, Line:    15, File: /home/ryan/projects/ryanmock/example/simple.c, Call: rmmAssertTrue
[     INFO ] Test Stack Trace:
[     INFO ]    Frame:   0, Line:    15, File: /home/ryan/projects/ryanmock/example/simple.c, Call: rmmAssertTrue
[  FAILURE ] failing_test
[==========] 2 test(s) run.
[  PASSED  ] 1 test(s).
[  FAILED  ] 1 test(s).
```

For more assert examples see the 
[Assert Example Source](https://gitlab.com/hansonry/ryanmock/-/blob/master/example/asserts.c) 
file.

For a full mocking example see the 
[Calculator Example](https://gitlab.com/hansonry/ryanmock/-/tree/master/example/calculator).

Command line help dump:

```
Usage test_asserts [OPTION]...
Runs this test based on options.

None of these OPTIONs are required:
  -r, --random SEED      Randomizes test order. If SEED is not specified a
                         random one will be chosen.
  -s, --solo TESTNAME    Runs only the test named TESTNAME.
  -p, --combination      Order tests such that each test is run after
                         every other test. Make it easy to spot setup issues.
  -x, --xml-output FILE  Writes XML results to file FILE.
  -q, --quiet            Suppresses Test output to Standard Output.
  -f, --fill SIZE VALUE  Fill the stack with SIZE bytes of VALUE.
  -b, --break            Triggers a breakpoint if supported on error or 
                         failure.
  -c, --color ENABLED    Set color output to be enabled or disabled.
  -l, --list             List tests.
  -h, --help, -?         Shows this message.

Exit status:
  0 if all tests pass, no errors or failures,
  1 if at least one test fails and no test errors,
  2 if at least one test has an error,
  3 if configuration is incorrect (bad command line arguments),
  4 if configuraiton is not supported (system limitations),
  5 if information was supplied (listing tests, or requesting help)

This test is using the ryanmock library:
  <https://gitlab.com/hansonry/ryanmock>
```

## Features

* Assert System
* Function Mocking System (You will need to find a way to inject your mocks)
    * Mocking Return Types
    * Mocking Output Parameters
    * Checking Parameters
    * Function Call Ordering
* Test Stack Tracing (Manual)
* Data Types:
    * Integers
        * Signed and Unsigned
        * 8, 16, 32, 64 bit
        * native types like int, short, long
        * pointer sized integers (uintptr_t, intptr_t)
    * Pointers
    * Floating Point - (Doubles and Floats)
    * Memory
    * Strings
    * Enumerations
* Comparisons
    * Equal
    * NotEqual
    * GreaterThan
    * GreaterThanOrEqualTo
    * LessThan
    * LessThanOrEqualTo
    * Between
    * Within (Epsilon)
* Results
    * Standard Output
    * XML Output
* Random Test Ordering
    * Using Time Based Seed
    * Using Specified Seed
    * Running every test after every other test
* Static Local Symbol Export
* Stack Memory Fill (to detect unitialized values)
* Documentation

## Project Links

* [ryanmock on gitlab](https://gitlab.com/hansonry/ryanmock).
* [ryanmock Mocking Guide](https://gitlab.com/hansonry/ryanmock/-/blob/master/doc/MockingGuide.md)
* [ryanmock Functions Cheat Sheet](https://gitlab.com/hansonry/ryanmock/-/blob/master/doc/FunctionsCheatSheet.md)
* [ryanmock Doxygen Documentation](https://hansonry.gitlab.io/ryanmock/)
* [Release Notes](https://gitlab.com/hansonry/ryanmock/-/blob/master/doc/ReleaseNotes.md).
* [Contributors](https://gitlab.com/hansonry/ryanmock/-/blob/master/Contributors.md).

## Architecture

ryanmock has two main objects. Types and Checks. Types generically encapsulates
data that can be compared. Checks can then compare two values using a Type
without needing to understand the detail behind the data. This allows 
for code reduction.





